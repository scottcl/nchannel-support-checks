﻿namespace Support_Daily_Check
{
    partial class ShopifyCheckErrorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.regularStatusGroupBox = new System.Windows.Forms.GroupBox();
            this.checkBox05 = new System.Windows.Forms.CheckBox();
            this.checkBox07 = new System.Windows.Forms.CheckBox();
            this.checkBox03 = new System.Windows.Forms.CheckBox();
            this.showMoreButton = new System.Windows.Forms.Button();
            this.moreGroupBox = new System.Windows.Forms.GroupBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox04 = new System.Windows.Forms.CheckBox();
            this.acceptButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.helpButton = new System.Windows.Forms.Button();
            this.regularStatusGroupBox.SuspendLayout();
            this.moreGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // regularStatusGroupBox
            // 
            this.regularStatusGroupBox.Controls.Add(this.checkBox05);
            this.regularStatusGroupBox.Controls.Add(this.checkBox07);
            this.regularStatusGroupBox.Controls.Add(this.checkBox03);
            this.regularStatusGroupBox.Location = new System.Drawing.Point(10, 11);
            this.regularStatusGroupBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.regularStatusGroupBox.Name = "regularStatusGroupBox";
            this.regularStatusGroupBox.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.regularStatusGroupBox.Size = new System.Drawing.Size(401, 43);
            this.regularStatusGroupBox.TabIndex = 0;
            this.regularStatusGroupBox.TabStop = false;
            this.regularStatusGroupBox.Text = "Statuses";
            // 
            // checkBox05
            // 
            this.checkBox05.AutoSize = true;
            this.checkBox05.Checked = true;
            this.checkBox05.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox05.Location = new System.Drawing.Point(281, 17);
            this.checkBox05.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox05.Name = "checkBox05";
            this.checkBox05.Size = new System.Drawing.Size(91, 17);
            this.checkBox05.TabIndex = 3;
            this.checkBox05.Text = "Completed (5)";
            this.checkBox05.UseVisualStyleBackColor = true;
            // 
            // checkBox07
            // 
            this.checkBox07.AutoSize = true;
            this.checkBox07.Checked = true;
            this.checkBox07.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox07.Location = new System.Drawing.Point(158, 17);
            this.checkBox07.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox07.Name = "checkBox07";
            this.checkBox07.Size = new System.Drawing.Size(80, 17);
            this.checkBox07.TabIndex = 2;
            this.checkBox07.Text = "Shipped (7)";
            this.checkBox07.UseVisualStyleBackColor = true;
            // 
            // checkBox03
            // 
            this.checkBox03.AutoSize = true;
            this.checkBox03.Checked = true;
            this.checkBox03.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox03.Location = new System.Drawing.Point(4, 17);
            this.checkBox03.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox03.Name = "checkBox03";
            this.checkBox03.Size = new System.Drawing.Size(112, 17);
            this.checkBox03.TabIndex = 1;
            this.checkBox03.Text = "Acknowledged (3)";
            this.checkBox03.UseVisualStyleBackColor = true;
            // 
            // showMoreButton
            // 
            this.showMoreButton.Location = new System.Drawing.Point(10, 59);
            this.showMoreButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.showMoreButton.Name = "showMoreButton";
            this.showMoreButton.Size = new System.Drawing.Size(134, 27);
            this.showMoreButton.TabIndex = 1;
            this.showMoreButton.Text = "Show More...";
            this.showMoreButton.UseVisualStyleBackColor = true;
            this.showMoreButton.Click += new System.EventHandler(this.showMoreButton_Click);
            // 
            // moreGroupBox
            // 
            this.moreGroupBox.Controls.Add(this.checkBox15);
            this.moreGroupBox.Controls.Add(this.checkBox13);
            this.moreGroupBox.Controls.Add(this.checkBox04);
            this.moreGroupBox.Location = new System.Drawing.Point(10, 91);
            this.moreGroupBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.moreGroupBox.Name = "moreGroupBox";
            this.moreGroupBox.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.moreGroupBox.Size = new System.Drawing.Size(401, 106);
            this.moreGroupBox.TabIndex = 2;
            this.moreGroupBox.TabStop = false;
            this.moreGroupBox.Text = "More Statuses";
            this.moreGroupBox.Visible = false;
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(281, 17);
            this.checkBox15.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(97, 17);
            this.checkBox15.TabIndex = 11;
            this.checkBox15.Text = "InProgress (14)";
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(158, 18);
            this.checkBox13.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(83, 17);
            this.checkBox13.TabIndex = 4;
            this.checkBox13.Text = "OnHold (13)";
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // checkBox04
            // 
            this.checkBox04.AutoSize = true;
            this.checkBox04.Location = new System.Drawing.Point(5, 18);
            this.checkBox04.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.checkBox04.Name = "checkBox04";
            this.checkBox04.Size = new System.Drawing.Size(83, 17);
            this.checkBox04.TabIndex = 0;
            this.checkBox04.Text = "Declined (4)";
            this.checkBox04.UseVisualStyleBackColor = true;
            // 
            // acceptButton
            // 
            this.acceptButton.Location = new System.Drawing.Point(317, 212);
            this.acceptButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(94, 28);
            this.acceptButton.TabIndex = 3;
            this.acceptButton.Text = "OK";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.acceptButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(10, 215);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(95, 24);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // helpButton
            // 
            this.helpButton.Location = new System.Drawing.Point(142, 215);
            this.helpButton.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(128, 29);
            this.helpButton.TabIndex = 5;
            this.helpButton.Text = "Help";
            this.helpButton.UseVisualStyleBackColor = true;
            this.helpButton.Click += new System.EventHandler(this.helpButton_Click);
            // 
            // ShopifyCheckErrorForm
            // 
            this.AcceptButton = this.acceptButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(420, 249);
            this.Controls.Add(this.helpButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.acceptButton);
            this.Controls.Add(this.moreGroupBox);
            this.Controls.Add(this.showMoreButton);
            this.Controls.Add(this.regularStatusGroupBox);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "ShopifyCheckErrorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Select Status To Remove";
            this.regularStatusGroupBox.ResumeLayout(false);
            this.regularStatusGroupBox.PerformLayout();
            this.moreGroupBox.ResumeLayout(false);
            this.moreGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox regularStatusGroupBox;
        private System.Windows.Forms.CheckBox checkBox05;
        private System.Windows.Forms.CheckBox checkBox07;
        private System.Windows.Forms.CheckBox checkBox03;
        private System.Windows.Forms.Button showMoreButton;
        private System.Windows.Forms.GroupBox moreGroupBox;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox04;
        private System.Windows.Forms.Button acceptButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button helpButton;
    }
}