﻿using nChannel_Daily_Program;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Support_Daily_Check
{
    public partial class ShopifyCheckErrorForm : Form
    {
        private bool showMore = false;
        private Main_Form mainForm;

        public ShopifyCheckErrorForm(Main_Form form)
        {
            mainForm = form;
            InitializeComponent();
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            List<int> statusList = new List<int>();
            if (checkBox03.Checked)
            {
                statusList.Add(3);
            }
            if (checkBox04.Checked)
            {
                statusList.Add(4);
            }
            if (checkBox05.Checked)
            {
                statusList.Add(5);
            }
            if (checkBox07.Checked)
            {
                statusList.Add(7);
            }
            if (checkBox13.Checked)
            {
                statusList.Add(13);
            }
            if (checkBox15.Checked)
            {
                statusList.Add(15);
            }
           // mainForm.checkAllErrors(statusList);
            this.Close();
        }

        private void showMoreButton_Click(object sender, EventArgs e)
        {
            if (showMore)
            {
                moreGroupBox.Hide();
                showMoreButton.Text = "Show more...";
                showMore = false;
            }
            else
            {
                moreGroupBox.Show();
                showMoreButton.Text = "Hide...";
                showMore = true;
            }
        }

        private void helpButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Check each status you want to remove as an error.\nThe program will then do a query for each reference number in the errors section of the selected account.\n"+
                "If a reference number is in nChannel and has the status of any selected checkbox, it will be removed from the errors list and placed into possible errors section.", "Help", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
