﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;
using Console = System.Console;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using System.Diagnostics;
using nChannel_Daily_Program;
using nChannel.Framework.v1_0.Collections;
using Newtonsoft.Json;
using nChannel.Framework.v1_0;
using System.Net;
using System.IO;
using System.Windows.Forms;
using nChannel.Framework.v1_0.Custom;

/*
* Created by: Scott Lewis
* Date: July 1, 2016

*/
public class NRAManager
{
    private string saveFileName = "VDOUnresolvedOrders_01_Modified.xls";//the name of the new excel saved file with the correct flatten information
    private string saveFileLocation = "C:\\Users\\Scott.Lewis\\Desktop\\";//the location to save the new correct file. Don't forget the ending "\\"
    private string openFileName = "VDOUnresolvedOrders_01.xls";//the name of the excel file you want to be edited and read
    private string openFileLocation = "C:\\Users\\Scott.Lewis\\Desktop\\";//the location of the excel file you want to be read. Don't forget the ending "\\"

    private static Regex digitsOnly = new Regex(@"[^\d]");//used to get rid of any non-numeric in a string
    private static Regex charsOnly = new Regex("[0-9]{2,}");//used to get rid of any numbers in a string

    private static int totalErrors = 0;//total number of errors found after all queries, excludes possible errors

    //list of acknowledge variables
    public List<string> acknowledge_orderStatusList;
    public List<string> acknowledge_orderIDList;
    public List<string> acknowledge_comitteeNumberList;
    public List<string> acknowledge_lineItemIDList;
    public List<string> acknowledge_dateRequiredList;
    public List<string> acknowledge_descriptionList;
    public List<string> acknowledge_distributorNameList;
    public List<string> acknowledge_eventDateList;
    public List<string> acknowledge_orderDateList;
    public List<string> acknowledge_given_OrdersList;
    public List<string> acknowledge_given_ProductCodeList;
    public List<string> acknowledge_referenceNumberList = new List<string>();
    public List<string> acknowledge_skuList = new List<string>();

    //list of pending variables
    public List<string> pending_orderStatusList;
    public List<string> pending_orderIDList;
    public List<string> pending_comitteeNumberList;
    public List<string> pending_lineItemIDList;
    public List<string> pending_dateRequiredList;
    public List<string> pending_descriptionList;
    public List<string> pending_distributorNameList;
    public List<string> pending_eventDateList;
    public List<string> pending_orderDateList;
    public List<string> pending_given_OrdersList;
    public List<string> pending_given_ProductCodeList;
    public List<string> pending_referenceNumberList = new List<string>();
    public List<string> pending_skuList = new List<string>();
    public List<string> concat_Order_ProductCodeList = new List<string>();
    public List<string> concat_Reference_SKUList = new List<string>();

    public const int SUCCESS = 1;
    public const int ERROR = 2;
    public Dictionary<string, List<string>> pendingErrorDictionary = new Dictionary<string, List<string>>();
    /// <summary>
    /// order * product code that are not found in the excel sheet but in the system
    /// </summary>
    public List<string> nraPendingErrorList = new List<string>();
    public List<string> nraAcknowledgeErrorList = new List<string>();

    protected Main_Form mainForm;
    public NRAManager(Main_Form form)
    {
        mainForm = form;
    }

    public string setFileName(string name)
    {
        int slashIndex = name.LastIndexOf('\\');
        openFileName = name.Substring(slashIndex + 1);
        // mainForm.addOutput("Name: " + openFileName);
        return openFileName;
    }
    public string setFileLocation(string name)
    {
        int slashIndex = name.LastIndexOf('\\');
        openFileLocation = name.Substring(0, slashIndex + 1);
        return openFileLocation;
        //  mainForm.addOutput("Location: " + openFileLocation);
    }

    /// <summary>
    /// Will process whichever is true or all if all are true
    /// </summary>
    /// <param name="processPending"></param>
    /// <param name="processAckowledge"></param>
    public void processNRA(bool processPending, bool processAcknowledge)
    {
        if (processPending)
        {
            getAllColumns(1);
            mainForm.Invoke(new MethodInvoker(delegate
            {
                mainForm.setNRAStatusText("Finding Pending Errors...");
            }));
            concat_Order_ProductCodeList = new List<string>();
            concat_Reference_SKUList = new List<string>();

            //creates the string of orders used for query
            string orders = "";
            foreach (string x in pending_given_OrdersList)
            {
                orders += "'" + x + "',";
            }
            orders = orders.TrimEnd(',');
            string queryStr = "select ReferenceNumber, e.SKU from [Order].[Order] o join [Order].Entry e on e.OrderID=o.ID join [Order].Status os on os.ID=o.Status left join [Order].EntryStatus es on es.ID=e.Status where AccountID=767 and o.Deleted=0 and e.Deleted=0 and e.Status in (1) and ReferenceNumber in ";
            queryStr += "(" + orders + ") order by o.ReferenceNumber";
            mainForm.Invoke(new MethodInvoker(delegate
            {
                mainForm.setNRAFirstCheckQuery(queryStr);
            }));
            //get the reference and sku list set from nChannel
            queryOrders(queryStr, false, true);
            for (int i = 0; i < pending_referenceNumberList.Count; i++)
            {
                concat_Reference_SKUList.Add(pending_referenceNumberList[i] + "*" + pending_skuList[i]);
            }
            for (int i = 0; i < pending_given_OrdersList.Count; i++)
            {
                concat_Order_ProductCodeList.Add(pending_given_OrdersList[i] + "*" + pending_given_ProductCodeList[i]);
            }

            Console.WriteLine("Displaying Errors: ");

            foreach (string x in concat_Order_ProductCodeList)
            {
                foreach(string y in concat_Reference_SKUList)
                {
                    if(!x.Equals(y))
                    {
                        nraPendingErrorList.Add(x);
                    }
                }
               /* if (!concat_Reference_SKUList.Contains(x))
                {
                    nraPendingErrorList.Add(x);
                    Console.WriteLine(x);
                }*/
            }
            //  referenceNumberList = getColumnList(10);
        }
        if (processAcknowledge)
        {
            getAllColumns(2);
            mainForm.Invoke(new MethodInvoker(delegate
            {
                mainForm.setNRAStatusText("Finding Ackowledged Errors...");
            }));
            concat_Order_ProductCodeList = new List<string>();
            concat_Reference_SKUList = new List<string>();

            string orders = "";
            foreach (string x in acknowledge_given_OrdersList)
            {
                orders += "'" + x + "',";
            }
            orders = orders.TrimEnd(',');
            string queryStr = "select ReferenceNumber,os.Name as OrderStatus,e.SKU,e.status,es.Name as linestatus from[Order].[Order] o join[Order].Entry e on e.OrderID = o.ID join[Order].Status os on os.ID = o.Status left join[Order].EntryStatus es on es.ID = e.Status where AccountID = 767 and o.Deleted = 0 and e.Deleted = 0 and e.Status in (10) and ReferenceNumber in ";
            queryStr += "(" + orders + ") order by o.ReferenceNumber";
            //get the reference and sku list set from nChannel
            queryOrders(queryStr, false, true);
            for (int i = 0; i < acknowledge_referenceNumberList.Count; i++)
            {
                concat_Reference_SKUList.Add(acknowledge_referenceNumberList[i] + "*" + acknowledge_skuList[i]);
            }
            for (int i = 0; i < acknowledge_given_OrdersList.Count; i++)
            {
                concat_Order_ProductCodeList.Add(acknowledge_given_OrdersList[i] + "*" + acknowledge_given_ProductCodeList[i]);
            }

            foreach (string x in concat_Order_ProductCodeList)
            {
                if (!concat_Reference_SKUList.Contains(x))
                {
                    nraAcknowledgeErrorList.Add(x);
                }
            }
            // referenceNumberList = getColumnList(10);
        }

        if (processPending)
        {
            mainForm.Invoke(new MethodInvoker(delegate
            {
                mainForm.setNRAStatusText("Finished Pending.");
            }));
        }
        else if (processAcknowledge)
        {
            mainForm.Invoke(new MethodInvoker(delegate
            {
                mainForm.setNRAStatusText("Finished Acknowledge");
            }));
        }
    }

    public void getOrderInfo(int orderID)
    {
        //used to create a list of strings that will be added to the new excel worksheet
        List<string> list = new List<string>();

        Excel.Application xlApp;
        Excel.Workbook xlWorkBook;
        Excel.Worksheet xlWorkSheet;
        Microsoft.Office.Interop.Excel.Range range;
        xlApp = new Microsoft.Office.Interop.Excel.Application();

        string str = "";
        int rCnt = 0;
        Console.WriteLine("Opening workbook and first worksheet");
        xlWorkBook = xlApp.Workbooks.Open(openFileLocation + openFileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
        xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        Console.WriteLine("Worksheet and workbook opened.\nReading cells...");
        range = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.UsedRange;

        string row = "";


        Microsoft.Office.Interop.Excel.Range range2 = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[rCnt, 5];
        range = range.Find(orderID);

        string cellValue = range2.Value.ToString();
        str = cellValue;
        row = str;
        list.Add(row);
        //Console.WriteLine("\t"+row);
        row = "";

        Console.WriteLine("Finished processing cells.");
        /*  foreach (string x in list) //Used to print all list results that will be saved to new workbook
          {
              Console.WriteLine(x);
          }
          */

        //close any excel objects for clean finish
        xlApp.Quit();
        releaseObject(xlWorkSheet);
        releaseObject(xlWorkBook);
        releaseObject(xlApp);

        // return list;
    }
    /*
    public int firstCheck(DateTime dateTime)
    {
        //start stopwatch for time elapsed
        Stopwatch sw = new Stopwatch();
        sw.Start();

        Console.WriteLine("\n---------------\nStarting NRA First Check Query ");
        mainForm.addOutput("Starting NRA First Check Query");
        string connectionStr = "Data Source=db-01;" +
        "Initial Catalog=nChannel;" +
        "User id=Sa-nchannelreader;" +
        "Password=Fluftuo2!;";
        SqlConnection sqlConnection1 = null;
        SqlCommand cmd;
        SqlDataReader reader = null;

        mainForm.addOutput(dateTime.ToShortDateString());

        string query = " select o1.AccountID,o1.id as destinationorderid " +
            "from [Order].[Order] o join [Order].Entry e on e.OrderID=o.ID join [Order].[Order] o1 on o1.OriginatingOrderID=o.ID " +
            "join [order].[Entry] e1 on e1.OrderID=o1.ID and e1.SKU=e.sku left join [Order].EntryDetail ed1 on ed1.OrderEntryID=e1.ID left " +
            "join [Order].EntryDetailShipping eds1 on eds1.OrderEntryDetailID=ed1.ID left join [Order].EntryDetail ed on ed.OrderEntryID=e.ID left " +
            "join [Order].EntryDetailShipping eds on eds.OrderEntryDetailID=ed.ID " +
            "where o.AccountID=767 and o.Deleted=0 and e.Deleted=0 and o1.Deleted=0 and (ed1.Deleted=0 or ed.Deleted is null) and (eds.Deleted=0 or eds.Deleted is null) " +
            "and (eds1.Deleted=0 or eds1.Deleted is null) and ((e.Status != e1.Status) or (e1.status not in (5) and eds1.trackingnumber !='')) and o1.DateModified>'" + dateTime.ToShortDateString() + "' order by o1.accountid,o1.DateModified desc ";
        mainForm.setNRAFirstCheckQuery(query);
        try
        {
            sqlConnection1 = new SqlConnection(connectionStr);
            cmd = new SqlCommand();

            cmd.CommandText = query;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;

            sqlConnection1.Open();

            reader = cmd.ExecuteReader();
        }
        catch (Exception ex)
        {
            mainForm.setNRAStatusText("SQL Connection Failed");
            mainForm.addOutput("SQL Failed. NRA could not query orders.");
            mainForm.addOutput(ex.Message);
            return 0;
        }

        if (reader.HasRows)
        {
            while (reader.Read())
            {
                string accountStr = reader.GetValue(0).ToString();
                string idStr = reader.GetValue(1).ToString();
                List<string> idList = new List<string>();
                if (firstCheckDictionary.ContainsKey(accountStr))
                {
                    idList = firstCheckDictionary[accountStr];
                    idList.Add(idStr);
                    firstCheckDictionary[accountStr] = idList;
                }
                else
                {
                    idList.Add(idStr);
                    firstCheckDictionary.Add(accountStr, idList);
                }
            }
            // Console.WriteLine("Rows Found: " + refNumEdit.Count);
        }
        else
        {
            Console.WriteLine("No rows found.");
            mainForm.addOutput("No rows found.");
        }

        sqlConnection1.Close();
        sw.Stop();
        Console.WriteLine("Connection Closed. Time Elapsed: " + sw.Elapsed + "\n------------");
        mainForm.addOutput("Connection Closed. Time Elapsed: " + sw.Elapsed + "\n------------");
        return SUCCESS;
    }
    */

    public void queryOrders(string query, bool pending, bool acknowledge)
    {
        //start stopwatch for time elapsed
        Stopwatch sw = new Stopwatch();
        sw.Start();

        Console.WriteLine("\n---------------\nStarting NRA Query for Pending ");
        mainForm.addOutput("Starting NRA Query for Orders");
        string connectionStr = "Data Source=db-01;" +
        "Initial Catalog=nChannel;" +
        "User id=Sa-nchannelreader;" +
        "Password=Fluftuo2!;";
        SqlConnection sqlConnection1 = null;
        SqlCommand cmd;
        SqlDataReader reader = null;
        try
        {
            sqlConnection1 = new SqlConnection(connectionStr);
            cmd = new SqlCommand();

            cmd.CommandText = query;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;

            sqlConnection1.Open();

            reader = cmd.ExecuteReader();
        }
        catch (Exception ex)
        {
            mainForm.setNRAStatusText("SQL Connection Failed");
            mainForm.addOutput("SQL Failed. NRA could not query orders.");
            mainForm.addOutput(ex.Message);
        }

        if (reader.HasRows)
        {
            while (reader.Read())
            {
                // Console.WriteLine("{0}\t{1}", reader.GetValue(0), reader.GetValue(1));
                if (pending)
                {
                    pending_referenceNumberList.Add(reader.GetValue(0).ToString());
                    pending_skuList.Add(reader.GetValue(1).ToString());
                }
                else if (acknowledge)
                {
                    acknowledge_referenceNumberList.Add(reader.GetValue(0).ToString());
                    acknowledge_skuList.Add(reader.GetValue(1).ToString());
                }

            }
            // Console.WriteLine("Rows Found: " + refNumEdit.Count);
        }
        else
        {
            Console.WriteLine("No rows found.");
            mainForm.addOutput("No rows found.");
        }

        sqlConnection1.Close();
        sw.Stop();
        Console.WriteLine("Connection Closed. Time Elapsed: " + sw.Elapsed + "\n------------");
        mainForm.addOutput("Connection Closed. Time Elapsed: " + sw.Elapsed + "\n------------");
    }

    public List<string> getColumnList(int column)
    {
        #region Read Column
        //used to create a list of strings that will be added to the new excel worksheet
        List<string> list = new List<string>();

        Excel.Application xlApp;
        Excel.Workbook xlWorkBook;
        Excel.Worksheet xlWorkSheet;
        Microsoft.Office.Interop.Excel.Range range;
        xlApp = new Microsoft.Office.Interop.Excel.Application();

        string str = "";
        int rCnt = 0;
        Console.WriteLine("Opening workbook and first worksheet");
        xlWorkBook = xlApp.Workbooks.Open(openFileLocation + openFileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
        xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        Console.WriteLine("Worksheet and workbook opened.\nReading cells...");
        range = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.UsedRange;

        string row = "";

        //in this case we need to check the columns before proceeding, so for each columns then rows
        for (rCnt = 2; rCnt < range.Rows.Count; rCnt++)
        {
            Microsoft.Office.Interop.Excel.Range range2 = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[rCnt, column];
            if (range2.Value == null)//If true, cell must have been empty. Should only do this if there is a column gap
            {
                continue;
            }
            string cellValue = range2.Value.ToString();
            str = cellValue;
            row = str;
            list.Add(row);
            //Console.WriteLine("\t"+row);
            row = "";
        }

        Console.WriteLine("Finished processing cells.");
        /*  foreach (string x in list) //Used to print all list results that will be saved to new workbook
          {
              Console.WriteLine(x);
          }
          */

        //close any excel objects for clean finish
        xlApp.Quit();
        releaseObject(xlWorkSheet);
        releaseObject(xlWorkBook);
        releaseObject(xlApp);

        return list;
        #endregion
    }

    /// <summary>
    /// Gets all columns 1 through 12 except 5 and 10 and sets the respective list
    /// </summary>
    /// <returns></returns>
    public List<string> getAllColumns(int sheetNumber)
    {
        //used to create a list of strings that will be added to the new excel worksheet
        List<string> list = new List<string>();

        Excel.Application xlApp;
        Excel.Workbook xlWorkBook;
        Excel.Worksheet xlWorkSheet;
        Microsoft.Office.Interop.Excel.Range range;
        xlApp = new Microsoft.Office.Interop.Excel.Application();

        string str = "";
        int rCnt = 0;
        Console.WriteLine("Opening workbook and first worksheet");
        xlWorkBook = xlApp.Workbooks.Open(openFileLocation + openFileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
        xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(sheetNumber);
        Console.WriteLine("Worksheet and workbook opened.\nReading cells...");
        range = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.UsedRange;

        string row = "";

        //in this case we need to check the columns before proceeding, so for each columns then rows
        for (int cCnt = 1; cCnt <= 9; cCnt++)
        {
            mainForm.Invoke(new MethodInvoker(delegate
            {
                mainForm.setNRAStatusText("Processing column " + cCnt + " of 9...");
            }));
            list = new List<string>();
            for (rCnt = 2; rCnt < range.Rows.Count; rCnt++)
            {
                Microsoft.Office.Interop.Excel.Range range2 = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[rCnt, cCnt];
                if (range2.Value == null)//If true, cell must have been empty. Should only do this if there is a column gap
                {
                    continue;
                }
                string cellValue = range2.Value.ToString();
                str = cellValue;
                row = str;
                list.Add(row);
                //Console.WriteLine("\t"+row);
                row = "";
            }
            #region Set list of current column
            switch (cCnt)
            {
                case 1:
                    {
                        if (sheetNumber == 1)
                        {
                            pending_comitteeNumberList = list;
                        }
                        else
                        {
                            acknowledge_comitteeNumberList = list;
                        }
                        break;
                    }
                case 2:
                    {
                        if (sheetNumber == 1)
                        {
                            pending_given_OrdersList = list;
                        }
                        else
                        {
                            acknowledge_given_OrdersList = list;
                        }
                        break;
                    }
                case 3:
                    {
                        if (sheetNumber == 1)
                        {
                            pending_lineItemIDList = list;
                        }
                        else
                        {
                            acknowledge_lineItemIDList = list;
                        }
                        break;
                    }
                case 4:
                    {
                        if (sheetNumber == 1)
                        {
                            pending_dateRequiredList = list;
                        }
                        else
                        {
                            acknowledge_dateRequiredList = list;
                        }
                        break;
                    }
                case 5:
                    {
                        if (sheetNumber == 1)
                        {
                            pending_given_ProductCodeList = list;
                        }
                        else
                        {
                            acknowledge_given_ProductCodeList = list;
                        }
                        break;
                    }
                case 6:
                    {
                        if (sheetNumber == 1)
                        {
                            pending_descriptionList = list;
                        }
                        else
                        {
                            acknowledge_descriptionList = list;
                        }
                        break;
                    }
                case 7:
                    {
                        if (sheetNumber == 1)
                        {
                            pending_distributorNameList = list;
                        }
                        else
                        {
                            acknowledge_distributorNameList = list;
                        }
                        break;
                    }
                case 8:
                    {
                        if (sheetNumber == 1)
                        {
                            pending_eventDateList = list;
                        }
                        else
                        {
                            acknowledge_eventDateList = list;
                        }
                        break;
                    }
                case 9:
                    {
                        if (sheetNumber == 1)
                        {
                            pending_orderDateList = list;
                        }
                        else
                        {
                            acknowledge_orderDateList = list;
                        }
                        break;
                    }
            }
            #endregion

        }
        Console.WriteLine("Finished processing cells.");
        //close any excel objects for clean finish
        xlApp.Quit();
        releaseObject(xlWorkSheet);
        releaseObject(xlWorkBook);
        releaseObject(xlApp);

        return list;
    }

    public List<string> getProductCodes()
    {
        #region Reads and returns column 5 for products
        //used to create a list of strings that will be added to the new excel worksheet
        List<string> list = new List<string>();

        Excel.Application xlApp;
        Excel.Workbook xlWorkBook;
        Excel.Worksheet xlWorkSheet;
        Microsoft.Office.Interop.Excel.Range range;
        xlApp = new Microsoft.Office.Interop.Excel.Application();

        string str = "";
        int rCnt = 0;
        Console.WriteLine("Opening workbook and first worksheet");
        xlWorkBook = xlApp.Workbooks.Open(openFileLocation + openFileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
        xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        Console.WriteLine("Worksheet and workbook opened.\nReading cells...");
        range = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.UsedRange;

        string row = "";

        //in this case we need to check the columns before proceeding, so for each columns then rows
        for (rCnt = 2; rCnt < range.Rows.Count; rCnt++)
        {
            Microsoft.Office.Interop.Excel.Range range2 = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[rCnt, 5];
            if (range2.Value == null)//If true, cell must have been empty. Should only do this if there is a column gap
            {
                continue;
            }
            string cellValue = range2.Value.ToString();
            str = cellValue;
            row = str;
            list.Add(row);
            //Console.WriteLine("\t"+row);
            row = "";
        }

        //Console.WriteLine("Finished processing cells.");
        /*  foreach (string x in list) //Used to print all list results that will be saved to new workbook
          {
              Console.WriteLine(x);
          }
          */

        //close any excel objects for clean finish
        xlApp.Quit();
        releaseObject(xlWorkSheet);
        releaseObject(xlWorkBook);
        releaseObject(xlApp);

        return list;
        #endregion
    }

    public List<string> getPendingOrders()
    {
        #region Returns and reads column 2 for Order ID
        //used to create a list of strings that will be added to the new excel worksheet
        List<string> list = new List<string>();

        Excel.Application xlApp;
        Excel.Workbook xlWorkBook;
        Excel.Worksheet xlWorkSheet;
        Microsoft.Office.Interop.Excel.Range range;
        xlApp = new Microsoft.Office.Interop.Excel.Application();

        string str = "";
        int rCnt = 0;
        Console.WriteLine("Opening workbook and first worksheet");
        xlWorkBook = xlApp.Workbooks.Open(openFileLocation + openFileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
        xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        Console.WriteLine("Worksheet and workbook opened.\nReading cells...");
        range = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.UsedRange;

        string row = "";

        //in this case we need to check the columns before proceeding, so for each columns then rows
        for (rCnt = 2; rCnt < range.Rows.Count; rCnt++)
        {
            Microsoft.Office.Interop.Excel.Range range2 = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[rCnt, 2];
            if (range2.Value == null)//If true, cell must have been empty. Should only do this if there is a column gap
            {
                continue;
            }
            string cellValue = range2.Value.ToString();
            str = cellValue;
            row = str;
            list.Add(row);
            //Console.WriteLine("\t"+row);
            row = "";
        }

        Console.WriteLine("Finished processing cells.");
        /*  foreach (string x in list) //Used to print all list results that will be saved to new workbook
          {
              Console.WriteLine(x);
          }
          */

        //close any excel objects for clean finish
        xlApp.Quit();
        releaseObject(xlWorkSheet);
        releaseObject(xlWorkBook);
        releaseObject(xlApp);

        return list;
        #endregion
    }

    private void releaseObject(object obj)
    {
        try
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }
        catch (Exception ex)
        {
            obj = null;
            Console.WriteLine("Unable to release the Object " + ex.ToString());
        }
        finally
        {
            GC.Collect();
        }
    }


    /* Below methods are unneccessary. Used as a fix for computers with inconsistent cultures. Sets to US culture then resets */
    /*
    //declare a variable to hold the CurrentCulture
    System.Globalization.CultureInfo oldCI;
    //get the old CurrenCulture and set the new, en-US
    void SetNewCurrentCulture()
    {
        oldCI = System.Threading.Thread.CurrentThread.CurrentCulture;
        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
    }
    //reset Current Culture back to the originale
    void ResetCurrentCulture()
    {
        System.Threading.Thread.CurrentThread.CurrentCulture = oldCI;
    }
    */
    private string ApiUrl = "https://api.nchannel.com";
    private string accountID = "1272";
    private string queryParams = "?dataformat=json&suppressevents=true&token=sharp";

    public string CallUrl(string url, string method, object objToSend = null)
    {
        Console.WriteLine(url);
        WebRequest req = WebRequest.Create(url);
        req.Method = method.ToString();
        if (objToSend != null)
        {
            Stream reqStream = req.GetRequestStream();
            string postData = JsonConvert.SerializeObject(objToSend);
            byte[] postArray = Encoding.ASCII.GetBytes(postData);
            reqStream.Write(postArray, 0, postArray.Length);
            reqStream.Close();
        }
        StreamReader sr = new StreamReader(req.GetResponse().GetResponseStream());
        return sr.ReadToEnd();
    }

    public void executeOrderProcessing(string wfID, List<string> orderIDList)
    {
        string url = string.Concat(new string[]
        {
                this.ApiUrl,
                "/Workflows/",
                this.accountID.ToString(),
                "/",
                wfID,
                "/Execute",
                this.queryParams });
        try
        {
            Work_WorkAttributes attributes = new Work_WorkAttributes();
            foreach (string id in orderIDList)
            {
                attributes.Add(new Work_WorkAttribute
                {
                    Name = "nChannel.OrderID",
                    Value = id
                });
            }
            WebRequest req = WebRequest.Create(url);
            req.Method = "POST";
            Stream reqStream = req.GetRequestStream();
            string postData = JsonConvert.SerializeObject(attributes);
            byte[] postArray = Encoding.ASCII.GetBytes(postData);
            reqStream.Write(postArray, 0, postArray.Length);
            reqStream.Close();
        //    StreamReader sr = new StreamReader(req.GetResponse().GetResponseStream());
         //   string result = sr.ReadToEnd();
           // MessageResult message = JsonConvert.DeserializeObject<MessageResult>(result);
            //MessageBox.Show(message.Message, message.Status.ToString());
        }
        catch (Exception ex)
        {
            Console.WriteLine(url);
        }
    }

    public void executeOrderShipping(string wfID, List<string> orderIDList)
    {
        string url = string.Concat(new string[]
        {
                this.ApiUrl,
                "/Workflows/",
                this.accountID.ToString(),
                "/",
                wfID,
                "/Execute",
                this.queryParams });
        try
        {

       
        Work_WorkAttributes attributes = new Work_WorkAttributes();
        foreach (string id in orderIDList)
        {
            attributes.Add(new Work_WorkAttribute
            {
                Name = "nChannel.OrderID",
                Value = id
            });
        }
        WebRequest req = WebRequest.Create(url);
        req.Method = "POST";
        Stream reqStream = req.GetRequestStream();
        string postData = JsonConvert.SerializeObject(attributes);
        byte[] postArray = Encoding.ASCII.GetBytes(postData);
        reqStream.Write(postArray, 0, postArray.Length);
        reqStream.Close();
      //  StreamReader sr = new StreamReader(req.GetResponse().GetResponseStream());
      //  string result = sr.ReadToEnd();
      //  MessageResult message = JsonConvert.DeserializeObject<MessageResult>(result);
      //  MessageBox.Show(message.Message, message.Status.ToString());
        }
        catch (Exception ex)
        {
            Console.WriteLine(url);
        }
    }

    public void loadChannelChannels()
    {
        string strChannels = this.CallUrl(string.Concat(new object[]
        {
                this.ApiUrl,
                "/Accounts/",
                this.accountID,
                "/ChannelChannelsForAccount/",
                this.queryParams
        }), "GET", null) ?? "";
        if (strChannels != "")
        {
            Dictionary<string, string> items = new Dictionary<string, string>();
            items.Add("-1", "Select Channel..");
            Channel_Channels channels = JsonConvert.DeserializeObject<Channel_Channels>(strChannels);
            channels.ForEach(delegate (Channel_Channel x)
            {
                items.Add(x.ID.ToString(), x.Name);
            });
            mainForm.GetNRAFirstCheck_ChannelComboBox().DataSource = new BindingSource(items, null);
        }
    }


    public Account_Locations loadLocationsByChannel(string channelID)
    {
        string str = this.CallUrl(string.Concat(new object[]
        {
                this.ApiUrl,
                "/Accounts/",
                this.accountID,
                "/LocationsByChannelChannel/",
                channelID,
                this.queryParams
        }), "GET", null) ?? "";
        Account_Locations result;
        if (str != "")
        {
            Dictionary<string, string> items = new Dictionary<string, string>();
            items.Add("-1", "Select Locations..");
            Account_Locations entities = JsonConvert.DeserializeObject<Account_Locations>(str);
            entities.ForEach(delegate (Account_Location x)
            {
                items.Add(x.ID.ToString(), x.Name);
            });
            result = entities;
        }
        else
        {
            result = new Account_Locations();
        }
        return result;
    }

    public void populate_cbWorkFlow(string channelID, string locationID)
    {
        Account_ChannelLocations entities = this.loadChannelLocation(channelID, locationID);
        Dictionary<string, string> items = new Dictionary<string, string>();
        items.Add("-1", "Select WorkFlow..");
        entities.ForEach(delegate (Account_ChannelLocation x)
        {
            x.Account_ChannelTransactions.ForEach(delegate (Account_ChannelTransaction ct)
            {
                ct.Work_WorkFlows.ForEach(delegate (Work_WorkFlow wf)
                {
                    items.Add(wf.ID.ToString(), wf.Name);
                });
            });
        });
        
    }

    public Account_ChannelLocations loadChannelLocation(string channelID, string locationID)
    {
        string tmpQueryParams = "?dataformat=json&suppressevents=false&token=sharp";
        string str = this.CallUrl(string.Concat(new object[]
        {
                this.ApiUrl,
                "/Accounts/",
                this.accountID,
                "/ChannelLocationsByChannelIDAndLocationID/",
                channelID,
                "/",
                locationID,
                tmpQueryParams
        }), "GET", null) ?? "";
        Account_ChannelLocations result;
        if (str != "")
        {
            Account_ChannelLocations entities = JsonConvert.DeserializeObject<Account_ChannelLocations>(str);
            result = entities;
        }
        else
        {
            result = new Account_ChannelLocations();
        }
        return result;
    }

}


