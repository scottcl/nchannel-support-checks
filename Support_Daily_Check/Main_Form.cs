﻿using nChannel.Framework.v1_0;
using nChannel.Framework.v1_0.Collections;
using Support_Daily_Check;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace nChannel_Daily_Program
{
    public partial class Main_Form : Form
    {
        private ShopifyManager shopifyManager;
        private NRAManager nraManager;
        private NRAFirstCheck nraFirstCheck;
        private ShopifyCheckErrorForm shopifyCheckErrorFrm;
        private bool ignoreSelectedIndexChanged;//true will prevent event handler for selected index change. Used to set datasource
        private bool deleted = false;
        private int days = 7;
        private int orderTypeID = 1;
        private List<string> checkedErrors;//a list of saved/checked errors to force to possible list
        private Dictionary<int, List<string>> recurringErrors;
        private string saveFile = @"\\192.168.40.41\storage\Support\dailyCheck_Text.txt";
        private BackgroundWorker idleWorker = new BackgroundWorker();//for checking when to close the application
        private BackgroundWorker fileWorker = new BackgroundWorker();//for saving and loading the save/settings file
        private BackgroundWorker bgWorker = new BackgroundWorker();//standard/default/testing
        private BackgroundWorker shopifyWorker = new BackgroundWorker();//for querying shopify errors
        private BackgroundWorker shopifyQueryOrderWorker = new BackgroundWorker();//for querying single shopify error and more info
        private BackgroundWorker nraFirstCheckQueryWorker = new BackgroundWorker();//for querying first check in nra and more info
        private BackgroundWorker nraFirstCheckOrderProcessWorker = new BackgroundWorker();//for shipping and order processing
        private BackgroundWorker nraExcelWorker = new BackgroundWorker();//for nra excel processing
        private BackgroundWorker shopifyCheckErrorWorker = new BackgroundWorker();//for querying orders in the error list
        private bool loadingFile = false, retryLoadFile = true;
        private const string checkedMark = " - Checked";
        List<string> shopifyQueryOrderResultList = new List<string>();
        private string accountNum = "", errorStr = "";//for temporary purposes
        List<string> shopifyCheckOrderResultList = new List<string>();
       
        private bool processAcknowledge, processPending;
        private bool shopifyDoCheckErrors = true;
        /// <summary>
        /// Sets the level to add to output.
        /// 1=All [Default], 2=Min, 3=None
        /// </summary>
        private int showOutputLevel = 1;
        public Main_Form()
        {
            int errorStatus = 0;//to remove duplicate message boxes popping up
            //errorStatus = checkInternet();

            InitializeComponent();
            /* Assign background workers */
            this.idleWorker.DoWork += new DoWorkEventHandler(this.idleWorker_DoWork);
            this.idleWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.idleWorker_RunWorkerCompleted);
            

            this.bgWorker.WorkerReportsProgress = true;
            this.bgWorker.WorkerSupportsCancellation = true;
            this.bgWorker.DoWork += new DoWorkEventHandler(this.bgWorker_DoWork);
            this.bgWorker.ProgressChanged += new ProgressChangedEventHandler(this.bw_ProgressChanged);
            this.bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);

            this.nraExcelWorker.WorkerReportsProgress = true;
            this.nraExcelWorker.WorkerSupportsCancellation = true;
            this.nraExcelWorker.DoWork += new DoWorkEventHandler(this.nraExcelWorker_DoWork);
            this.nraExcelWorker.ProgressChanged += new ProgressChangedEventHandler(this.nraExcelWorker_ProgressChanged);
            this.nraExcelWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(this.nraExcelWorker_RunWorkerCompleted);

            this.nraFirstCheckQueryWorker.WorkerReportsProgress = true;
            this.nraFirstCheckQueryWorker.WorkerSupportsCancellation = true;
            this.nraFirstCheckQueryWorker.DoWork += new DoWorkEventHandler(nraFirstCheckQueryWorker_DoWork);
            this.nraFirstCheckQueryWorker.ProgressChanged += new ProgressChangedEventHandler(nraFirstCheckQueryWorker_ProgressChanged);
            this.nraFirstCheckQueryWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(nraFirstCheckQueryWorker_RunWorkerCompleted);

            this.nraFirstCheckOrderProcessWorker.WorkerReportsProgress = true;
            this.nraFirstCheckOrderProcessWorker.WorkerSupportsCancellation = true;
            this.nraFirstCheckOrderProcessWorker.DoWork += new DoWorkEventHandler(nraFirstCheckOrderProcessWorker_DoWork);
            this.nraFirstCheckOrderProcessWorker.ProgressChanged += new ProgressChangedEventHandler(nraFirstCheckOrderProcessWorker_ProgressChanged);
            this.nraFirstCheckOrderProcessWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(nraFirstCheckOrderProcessWorker_RunWorkerCompleted);

            this.shopifyWorker.WorkerReportsProgress = true;
            this.shopifyWorker.WorkerSupportsCancellation = true;
            this.shopifyWorker.DoWork += new DoWorkEventHandler(this.shopify_DoWork);
            this.shopifyWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(shopify_RunWorkerCompleted);

            this.shopifyQueryOrderWorker.WorkerReportsProgress = true;
            this.shopifyQueryOrderWorker.WorkerSupportsCancellation = true;
            this.shopifyQueryOrderWorker.DoWork += new DoWorkEventHandler(this.shopifyQueryOrderWorker_DoWork);
            this.shopifyQueryOrderWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(shopifyQueryOrderWorker_RunWorkerCompleted);
            
            this.fileWorker.WorkerReportsProgress = true;
            this.fileWorker.WorkerSupportsCancellation = true;
            this.fileWorker.DoWork += new DoWorkEventHandler(this.fileWorker_DoWork);
            this.fileWorker.ProgressChanged += new ProgressChangedEventHandler(this.fileWorker_ProgressChanged);
            this.fileWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(fileWorker_RunWorkerCompleted);

            this.shopifyCheckErrorWorker.WorkerReportsProgress = true;
            this.shopifyCheckErrorWorker.WorkerSupportsCancellation = true;
            this.shopifyCheckErrorWorker.DoWork += new DoWorkEventHandler(this.shopifyCheckErrorWorker_DoWork);
            this.shopifyCheckErrorWorker.ProgressChanged += new ProgressChangedEventHandler(this.shopifyCheckErrorWorker_ProgressChanged);
            this.shopifyCheckErrorWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(shopifyCheckErrorWorker_RunWorkerCompleted);

            shopifyManager = new ShopifyManager(this);
            nraManager = new NRAManager(this);
            nraFirstCheck = new NRAFirstCheck(this);
            recurringErrors = new Dictionary<int, List<string>>();
            shopifyCheckErrorFrm = new ShopifyCheckErrorForm(this);

            outputToolStripComboBox.SelectedIndex = 0;

            if (errorStatus == 0)
            {
                if (!fileWorker.IsBusy)
                {
                    loadingFile = true;
                    fileWorker.RunWorkerAsync();
                }
            }
        }

        private DateTime lastSystemTime;
        bool inactive = false;
        private void idleWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            lastSystemTime = DateTime.Now;
            bool closeApplication = false;
            int minutes = 10;
            int sleepTime = (60 * 1000) * minutes;
            minutes = 60;
            long maxIdleTime = (60 * 1000) * minutes;
            Stopwatch sw = new Stopwatch();            
            do
            {
                if (!inactive)
                {
                    sw.Stop();
                    Console.WriteLine("SW Stopped. Sleeping...");
                    System.Threading.Thread.Sleep(sleepTime);
                }
                else
                {
                    if(!sw.IsRunning)
                    {
                        sw.Restart();
                     //   Console.WriteLine("SW Restarted.");
                    }
                  //  Console.WriteLine("Checking time( "+sw.ElapsedMilliseconds+" > "+maxIdleTime+" )");
                    if (sw.ElapsedMilliseconds > maxIdleTime)
                    {
                        closeApplication = true;
                        sw.Stop();
                    }
                }          
            } while (!closeApplication);
        }
        private bool DoesApplicationHaveFocus()
        {
            bool hasFocus = false;
            if (ContainsFocus)
            {
                hasFocus = true;
            }
            else {
                FormCollection forms = Application.OpenForms;
                foreach (Form f in forms)
                {
                    if (f != null && f.ContainsFocus)
                    {
                        hasFocus = true;
                        break;
                    }
                }
            }
            return hasFocus;
        }
        private void Main_Form_Leave(object sender, EventArgs e)
        {
           // Console.WriteLine("Left Form");
        }
        private void Main_Form_Deactivate(object sender, EventArgs e)
        {
            Console.WriteLine("inactive true");
            inactive = true;
        }
        private void Main_Form_Activated(object sender, EventArgs e)
        {
            Console.WriteLine("inactive false");
            inactive = false;
        }
        private void Main_Form_MouseMove(object sender, MouseEventArgs e)
        {
           // Console.WriteLine("Mouse Move Form");
        }
        private void idleWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.Invoke(new MethodInvoker(delegate
            {
                if (!(e.Error == null))
                {
                    MessageBox.Show("Could not close application due to idle.\n" + e.Error.Message);
                }
            }));

            Application.Exit();
        }

        #region Check Internet for VPN and connection
        private int checkInternet()
        {
            int errorStatus = 0;
            //0=Success, 1=Incorrect VPN, 2=VPN connection lost, 3=internet connection is down, 4=No interent
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
                foreach (NetworkInterface Interface in interfaces)
                {
                    //MessageBox.Show(Interface.Name + " " + Interface.NetworkInterfaceType.ToString() + " " + Interface.Description);
                    if (Interface.OperationalStatus == OperationalStatus.Up)
                    {
                        if ((Interface.NetworkInterfaceType == NetworkInterfaceType.Ppp) && (Interface.NetworkInterfaceType != NetworkInterfaceType.Loopback))
                        {
                            IPv4InterfaceStatistics statistics = Interface.GetIPv4Statistics();
                            if (!Interface.Name.Contains("nChannel"))//check if VPN is with nChannel (such as home) if not error out
                            {
                                errorStatus = 1;
                            }
                            else
                            {
                                errorStatus = 0;
                                break;
                            }
                            // MessageBox.Show(Interface.Name + " " + Interface.NetworkInterfaceType.ToString() + " " + Interface.Description);
                        }
                        else
                        {
                            errorStatus = 2;
                        }
                    }
                    else
                    {
                        errorStatus = 3;
                    }
                }
            }
            else
            {
                errorStatus = 4;
            }

            switch (errorStatus)
            {
                case 1:
                    {
                        MessageBox.Show("Warning. You may be using a VPN that cannot connect to nChannel systems.", "VPN Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }
                case 2:
                    {
                        MessageBox.Show("Warning. VPN Connnection is lost!", "VPN Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }
                case 3:
                    {
                        MessageBox.Show("Warning. Network connection is not working properly.", "Internet Connection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }
                case 4:
                    {
                        MessageBox.Show("Error! No internet connection found", "No Internet", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }
            }
            return errorStatus;
        }
        #endregion
        private void shopifyQueryOrderWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            if (worker.CancellationPending == true)
            {
                e.Cancel = true;
                return;
            }
            else
            {
                worker.ReportProgress(0);
                shopifyQueryOrderResultList = shopifyManager.queryMoreInfo(accountNum, errorStr, deleted, orderTypeID);
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    return;
                }
                worker.ReportProgress(50);
                base.Invoke(new MethodInvoker(delegate
                {
                    processShopifyQueryOrderResults();
                }));
                worker.ReportProgress(100);
            }
        }

        private void shopifyQueryOrderWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            base.Invoke(new MethodInvoker(delegate
            {
                switch (e.ProgressPercentage)
                {
                    case 0:
                        {
                            addOutput("Starting Shopify Query Selected Error for " + errorStr + " in " + accountNum);
                            setShopifyQueryStatus("Starting...");
                            break;
                        }
                    case 50:
                        {
                            addOutput("Shopify Query Selected Error: Query done, updating form...");
                            setShopifyQueryStatus("Query done, updating form...");
                            break;
                        }
                    case 100:
                        {
                            addOutput("Successfully finished Shopify Query Selected Error.");
                            setShopifyQueryStatus("Complete");
                            break;
                        }
                }
            }));

        }

        private void shopifyQueryOrderWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.Invoke(new MethodInvoker(delegate
            {
                if ((e.Cancelled == true))
                {
                    addOutput("Shopify Query Selected Error was Canceled");
                    setShopifyQueryStatus("Canceled");
                }
                else if (!(e.Error == null))
                {
                    addOutput("Shopify Query Selected Failed: " + e.Error.Message);
                    setShopifyQueryStatus("Error");
                }

            }));
        }
        private void shopifyCheckErrorWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            if (worker.CancellationPending == true)
            {
                e.Cancel = true;
                return;
            }
            else
            {
                Console.WriteLine("account num equals: "+accountNum);
                if (accountNum.Equals("All"))//execute all accounts
                {
                    List<string> acconts_ErrorsList = new List<string>();
                    List<int> accountsList = new List<int>();
                    foreach(int x in ShopifyManager.errors_Account_List.Keys)
                    {
                        acconts_ErrorsList.Add(x.ToString());
                    }
                   // acconts_ErrorsList = ShopifyManager.errors_Account_List.Keys.ToList();
                    List<int> addToPossibleList = new List<int>();

                    foreach (string account in accountListBox.Items)
                    {
                        if (account.Equals("All"))
                        {
                            continue;
                        }
                        accountNum = account;
                        List<string> errorList = ShopifyManager.errors_Account_List[Convert.ToInt32(accountNum)].Cast<string>().ToList();
                        List<string> resultList = new List<string>();
                        if (worker.CancellationPending == true)
                        {
                            e.Cancel = true;
                            return;
                        }
                        resultList = shopifyManager.checkAllErrors(accountNum, errorList, deleted, orderTypeID);
                        Console.WriteLine("Number of reference numbers in nChannel: " + (resultList.Count - 1) + " of " + errorList.Count + " in " + accountNum);
                        if (resultList.Count - 1 == errorList.Count)//remove all the errors in the selected account
                        {
                            ShopifyManager.errors_Account_List.Remove(Convert.ToInt32(accountNum));
                        }
                        else
                        {
                            ShopifyManager.errors_Account_List[Convert.ToInt32(accountNum)] = errorList.Except(resultList).ToList();
                        }
                        Console.WriteLine("ERROR LIST from query: ");
                        foreach (string x in errorList)
                        {
                            Console.WriteLine(x);
                        }
                    }
                  
                    base.Invoke(new MethodInvoker(delegate
                    {
                        ignoreSelectedIndexChanged = true;
                        errorListBox.DataSource = null;
                        errorsLabel.Text = "Errors";
                        updateOrderInfo(Color.DarkGray);
                        //    acconts_ErrorsList = ShopifyManager.errors_Account_List.Keys.ToList();
                        acconts_ErrorsList = new List<string>();
                        foreach (int x in ShopifyManager.errors_Account_List.Keys)
                        {
                            acconts_ErrorsList.Add(x.ToString());
                        }
                        //   accountsList = ShopifyManager.errors_Account_List.Keys.ToList();
                        //   accountsList.Add("All");
                        //   foreach (int x in acconts_ErrorsList)
                        //   {
                        //       accountsList.Add(x + "");
                        //  }
                        accountListBox.DataSource = null;
                        accountListBox.DataSource = acconts_ErrorsList;
                        Console.WriteLine("ACCOUNTS IN LISTBOX 1: ");
                        foreach (string x in acconts_ErrorsList)
                              {
                                   Console.WriteLine(x + "");
                             }
                            List<string> accountListItems = accountListBox.Items.Cast<string>().ToList() ;
                        accountListItems.Insert(0, "All");
                        accountListBox.DataSource = null;
                         Console.WriteLine("ACCOUNTS IN LISTBOX 2: ");
                         foreach (string x in accountListItems)
                         {
                             Console.WriteLine(x + "");
                         }
                        accountListBox.DataSource = accountListItems;
                         accountListBox.Refresh();
                         ignoreSelectedIndexChanged = false;
                     }));
                 }
                 else//execute for the selected account
                 {
                     List<string> errorList = new List<string>();
                     base.Invoke(new MethodInvoker(delegate
                     {
                         errorList = errorListBox.Items.Cast<string>().ToList();
                     }));
                     List<string> resultList = new List<string>();
                     if (worker.CancellationPending == true)
                     {
                         e.Cancel = true;
                         return;
                     }
                     resultList = shopifyManager.checkAllErrors(accountNum, errorList, deleted, orderTypeID);
                     //Console.WriteLine("Number of reference numbers in nChannel: " + resultList.Count + " of " + errorList.Count + " in " + accountNum);
                     if (resultList.Count - 1 == errorList.Count)//remove all the errors in the selected account
                     {
                         base.Invoke(new MethodInvoker(delegate
                         {
                             ignoreSelectedIndexChanged = true;
                             errorListBox.DataSource = null;
                             setErrorListBox(0);
                             errorListBox.Refresh();
                             ShopifyManager.errors_Account_List.Remove(Convert.ToInt32(accountNum));
                             accountListBox.DataSource = null;

                             List<int> acconts_ErrorsList = new List<int>();
                             List<string> accountsList = new List<string>();

                             acconts_ErrorsList = ShopifyManager.errors_Account_List.Keys.ToList();

                             List<int> addToPossibleList = new List<int>();

                             accountsList.Add("All");
                             foreach (int x in acconts_ErrorsList)
                             {
                                 accountsList.Add(x + "");
                             }
                             accountListBox.DataSource = accountsList;
                             accountListBox.Refresh();
                             ignoreSelectedIndexChanged = false;
                         }));
                     }
                     else
                     {
                         base.Invoke(new MethodInvoker(delegate
                         {
                             ignoreSelectedIndexChanged = true;
                             errorList = errorList.Except(resultList).ToList();
                             errorListBox.DataSource = null;
                             ShopifyManager.errors_Account_List[Convert.ToInt32(accountNum)] = errorList;
                             setErrorListBox(0);
                             ignoreSelectedIndexChanged = false;
                         }));
                     }
                 }

                 worker.ReportProgress(100);
             }
         }

         private void shopifyCheckErrorWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
         {
             base.Invoke(new MethodInvoker(delegate
             {
                 switch (e.ProgressPercentage)
                 {
                     case 0:
                         {
                             addOutput("Starting to Check Shopify Errors...");
                             setShopifyQueryStatus("Starting Check...");
                             //nraFirstCheck_StatusLabel.Text = "Starting NRA First Check Query...";
                             break;
                         }
                     case 50:
                         {
                             //addOutput("Adding Accounts To Query from file...");
                             // nraFirstCheck_StatusLabel.Text = "Query Successful. Adding results to lists...";
                             break;
                         }
                     case 100:
                         {
                             addOutput("Successfully Checked Shopify Errors.");
                             setShopifyQueryStatus("Complete");
                             // nraFirstCheck_StatusLabel.Text = "Comlpeted NRA First Check Query";
                             break;
                         }
                 }
             }));

         }

         private void shopifyCheckErrorWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
         {
             base.Invoke(new MethodInvoker(delegate
             {
                 if ((e.Cancelled == true))
                 {
                     addOutput("Shopify Error Check Canceled");
                     setShopifyQueryStatus("Canceled");
                 }

                 else if (!(e.Error == null))
                 {
                     addOutput("Shopify Error Check Failed: " + e.Error.Message+ " Details: "+e.Error.StackTrace);
                     setShopifyQueryStatus("Error");
                 }

             }));
         }

         private void nraExcelWorker_DoWork(object sender, DoWorkEventArgs e)
         {
             BackgroundWorker worker = sender as BackgroundWorker;

             if (worker.CancellationPending == true)
             {
                 e.Cancel = true;
                 return;
             }
             else
             {
                 worker.ReportProgress(0);
                 if (processPending)
                 {
                     nraManager.processNRA(processPending, false);
                     base.Invoke(new MethodInvoker(delegate
                     {
                         nraAddPendingLists();
                     }));
                 }
                 if (processAcknowledge)
                 {
                     nraManager.processNRA(false, processAcknowledge);
                     base.Invoke(new MethodInvoker(delegate
                     {
                         nraAddAcknowledgeLists();
                     }));
                 }
                 worker.ReportProgress(50);
                 //fillAccountsToQueryList();
                 base.Invoke(new MethodInvoker(delegate
                 {
                     //selectedDate = nraFirstDateTimePicker.Value;
                 }));
                 worker.ReportProgress(100);
             }
         }

         private void nraExcelWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
         {
             base.Invoke(new MethodInvoker(delegate
             {
                 switch (e.ProgressPercentage)
                 {
                     case 0:
                         {
                             addOutput("NRA Excel Processes Starting...");
                             //nraFirstCheck_StatusLabel.Text = "Starting NRA First Check Query...";
                             break;
                         }
                     case 50:
                         {
                             addOutput("Adding Accounts To Query from file...");
                             // nraFirstCheck_StatusLabel.Text = "Query Successful. Adding results to lists...";
                             break;
                         }
                     case 100:
                         {
                             addOutput("Successfully finished loading text file.");
                             // nraFirstCheck_StatusLabel.Text = "Comlpeted NRA First Check Query";
                             break;
                         }
                 }
             }));

         }

         private void nraExcelWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
         {
             base.Invoke(new MethodInvoker(delegate
             {
                 if ((e.Cancelled == true))
                 {
                     addOutput("NRA Excel Processes Canceled");
                     setNRAStatusText("Canceled");
                 }
                 else if (!(e.Error == null))
                 {
                     addOutput("NRA Excel Process Error: " + e.Error.Message);
                     setNRAStatusText("Error");
                 }
             }));
         }

         private void fileWorker_DoWork(object sender, DoWorkEventArgs e)
         {
             BackgroundWorker worker = sender as BackgroundWorker;

             if (worker.CancellationPending == true)
             {
                 e.Cancel = true;
                 return;
             }
             else
             {
                //wait for a 800 milliseconds ( >second) as the form loads\
                Stopwatch delay = new Stopwatch();
                delay.Start();
                do{                } while (delay.ElapsedMilliseconds < 800);
                delay.Stop();
                 worker.ReportProgress(0);
                 loadTextFile();
                 if (worker.CancellationPending == true)
                 {
                     e.Cancel = true;
                     return;
                 }
                 worker.ReportProgress(50);
                 fillAccountsToQueryList();
                 base.Invoke(new MethodInvoker(delegate
                 {
                     //selectedDate = nraFirstDateTimePicker.Value;
                 }));
                 worker.ReportProgress(100);
             }
         }

         private void fileWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
         {
             base.Invoke(new MethodInvoker(delegate
             {
                 switch (e.ProgressPercentage)
                 {
                     case 0:
                         {
                             addOutput("Loading text file...");
                             setShopifyQueryStatus("Loading text file");
                             //nraFirstCheck_StatusLabel.Text = "Starting NRA First Check Query...";
                             break;
                         }
                     case 50:
                         {
                             addOutput("Adding Accounts To Query from file...");
                             setShopifyQueryStatus("Adding Accounts...");
                             // nraFirstCheck_StatusLabel.Text = "Query Successful. Adding results to lists...";
                             break;
                         }
                     case 100:
                         {
                             addOutput("Successfully finished loading text file.");
                             setShopifyQueryStatus("Ready");
                             // nraFirstCheck_StatusLabel.Text = "Comlpeted NRA First Check Query";
                             break;
                         }
                 }
             }));

         }

         private void fileWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
         {
             base.Invoke(new MethodInvoker(delegate
             {
                 if ((e.Cancelled == true))
                 {
                     addOutput("Loading/Adding File was Canceled");
                     setShopifyQueryStatus("Loading File was Canceled");
                 }

                 else if (!(e.Error == null))
                 {
                     addOutput("Loading/Adding File Failed: " + e.Error.Message);
                     setShopifyQueryStatus("Loading File Failed");
                     if (retryLoadFile)
                     {
                         //fileWorker.RunWorkerAsync();
                         retryLoadFile = false;
                     }
                 }
                 else
                 {
                     setShopifyQueryStatus("Ready");
                     loadingFile = false;
                 }

             }));
         }


         private void shopify_DoWork(object sender, DoWorkEventArgs e)
         {
             #region Shopify Background Work
             BackgroundWorker worker = sender as BackgroundWorker;

             if ((worker.CancellationPending == true))
             {
                 e.Cancel = true;
                 return;
             }
             else
             {
                 bool accountSelectAll = false;
                 List<string> accountQueryItems;
                 base.Invoke(new MethodInvoker(delegate
                 {
                     addOutput("==========================");
                     addOutput("Start Query Button Pressed");
                     addOutput("==========================");
                     setShopifyQueryStatus("Starting...");

                     //Makes the info text boxes grey to make clear they are not neccessarily the current info
                     //change the text and color for each info textbox
                     //must change backcolor first to change forecolor on readonly textbox
                     Color foreColor = Color.Black;
                     foreColor = Color.Gray;
                     dateCreatedTextBox.BackColor = dateCreatedTextBox.BackColor;
                     dateCreatedTextBox.ForeColor = foreColor;
                     dateCreatedTextBox.Refresh();
                     dateModifiedTextBox.BackColor = dateModifiedTextBox.BackColor;
                     dateModifiedTextBox.ForeColor = foreColor;
                     dateModifiedTextBox.Refresh();
                     referenceNumberTextBox.BackColor = referenceNumberTextBox.BackColor;
                     referenceNumberTextBox.ForeColor = foreColor;
                     referenceNumberTextBox.Refresh();
                     idTextBox.BackColor = idTextBox.BackColor;
                     idTextBox.ForeColor = foreColor;
                     idTextBox.Refresh();

                     ignoreSelectedIndexChanged = true;
                     ShopifyManager.errors_Account_List.Clear();
                     ShopifyManager.possible_Errors_Account_List.Clear();
                     ShopifyManager.accountNum_Errors.Clear();
                     possibleAccountListBox.DataSource = null;
                     possibleAccountListBox.Items.Clear();
                     possibleAccountListBox.Refresh();
                     possibleErrorListBox.DataSource = null;
                     possibleErrorListBox.Items.Clear();
                     possibleErrorListBox.Refresh();
                     errorListBox.ClearSelected();
                     errorListBox.DataSource = null;
                     errorListBox.Items.Clear();
                     errorListBox.Refresh();
                     accountListBox.DataSource = null;
                     accountListBox.Items.Clear();
                     accountListBox.Refresh();
                     shopify_CheckedCheckBox.Checked = false;
                     shopify_OrderInfoLabel.Visible = false;
                     days = Convert.ToInt32(daysNumericUpDown.Value);
                     deleted = deletedCheckBox.Checked;
                     orderTypeID = Convert.ToInt32(orderTypeIDNumericUpDown.Value);

                     accountSelectAll = accountsQueryListBox.SelectedIndex == 0;
                 }));
                 if (accountSelectAll)
                 {
                     foreach (string x in accountsQueryListBox.Items)
                     {
                         if (x == "All")
                         {
                             continue;
                         }
                         base.Invoke(new MethodInvoker(delegate
                         {
                             setShopifyQueryStatus("Working on " + x + "...");
                         }));
                         if ((worker.CancellationPending == true))
                         {
                             e.Cancel = true;
                             return;
                         }
                         shopifyManager.newShopifyQuery("", Convert.ToInt32(x), days, deleted, orderTypeID);
                     }
                 }
                 else
                 {
                     foreach (string x in accountsQueryListBox.SelectedItems)
                     {
                         base.Invoke(new MethodInvoker(delegate
                         {
                             setShopifyQueryStatus("Working on " + x + "...");
                         }));
                         if ((worker.CancellationPending == true))
                         {
                             e.Cancel = true;
                             return;
                         }
                         shopifyManager.newShopifyQuery("", Convert.ToInt32(x), days, deleted, orderTypeID);
                     }
                 }
                 if ((worker.CancellationPending == true))
                 {
                     e.Cancel = true;
                     return;
                 }
                 base.Invoke(new MethodInvoker(delegate
                 {

                     if (shopifyManager.getTotalErrors() == 0)
                     {
                         addOutput("------------------------");
                         addOutput("No errors were found.");
                     }
                     else
                     {
                         addOutput("------------------------");
                         addOutput("Total Errors Found: " + shopifyManager.getTotalErrors() + ". In Accounts: ");
                         foreach (int x in ShopifyManager.accountNum_Errors)
                         {
                             // Console.WriteLine(x);
                             addOutput(x + "");
                         }
                     }

                     addOutput("------------------------");

                     List<int> acconts_ErrorsList = new List<int>();
                     List<string> accounts_PossibleList = new List<string>();

                     acconts_ErrorsList = ShopifyManager.errors_Account_List.Keys.ToList();

                     List<int> addToPossibleList = new List<int>();

                     accounts_PossibleList.Add("All");
                     foreach (int x in acconts_ErrorsList)
                     {
                         accounts_PossibleList.Add(x + "");
                     }

                     ignoreSelectedIndexChanged = true;                     
                         accountListBox.DataSource = accounts_PossibleList;
                         accountListBox.ClearSelected();
                     ignoreSelectedIndexChanged = false;

                     //Add possible errors
                     acconts_ErrorsList = new List<int>();
                     accounts_PossibleList = new List<string>();

                     acconts_ErrorsList = ShopifyManager.possible_Errors_Account_List.Keys.ToList();
                     accounts_PossibleList.Add("All");
                     foreach (int x in acconts_ErrorsList)
                     {
                         accounts_PossibleList.Add(x + "");
                     }
                     foreach (int x in addToPossibleList)
                     {
                         if (!accounts_PossibleList.Contains(x + ""))
                         {
                             accounts_PossibleList.Add(x + "");
                         }
                     }

                     //Checks if possible list contains recurring errors, if not add it
                     foreach (KeyValuePair<int, List<string>> entry in recurringErrors)
                     {
                         if (!accounts_PossibleList.Contains(entry.Key.ToString()))
                         {
                             accounts_PossibleList.Add(entry.Key.ToString());
                         }
                     }
                     ignoreSelectedIndexChanged = true;
                     possibleAccountListBox.DataSource = accounts_PossibleList;
                     possibleAccountListBox.ClearSelected();
                     ignoreSelectedIndexChanged = false;
                     //remove any recurring errors
                 }));

             }
             #endregion
         }

         private void shopify_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
         {
             base.Invoke(new MethodInvoker(delegate
             {
                 if ((e.Cancelled == true))
                 {
                     Console.WriteLine("Shopify Query Canceled!");
                     addOutput("Shopify Query Canceled!");
                     setShopifyQueryStatus("Canceled");
                 }

                 else if (!(e.Error == null))
                 {
                     Console.WriteLine("Error: " + e.Error.Message);
                     addOutput("Shopify Query Error!");
                     addOutput(e.Error.Message);
                     setShopifyQueryStatus("Error");
                 }

                 else
                 {
                     if (shopifyDoCheckErrors)
                     {
                         accountNum = "All";
                         if (!shopifyCheckErrorWorker.IsBusy)
                         {
                             shopifyCheckErrorWorker.RunWorkerAsync();
                         }
                     }
                     Console.WriteLine("Done!");
                     addOutput("Shopify Query Completed!");
                     setShopifyQueryStatus("Complete");
                 }
             }));
         }

         private void nraFirstCheckQueryWorker_DoWork(object sender, DoWorkEventArgs e)
         {
             BackgroundWorker worker = sender as BackgroundWorker;

             if (worker.CancellationPending == true)
             {
                 e.Cancel = true;
                 return;
             }
             else
             {
                 worker.ReportProgress(0);
                 DateTime selectedDate = new DateTime();
                 base.Invoke(new MethodInvoker(delegate
                 {
                     selectedDate = nraFirstDateTimePicker.Value;
                 }));
                 if (nraFirstCheck.queryOrders(selectedDate) == NRAManager.SUCCESS)
                 {
                     worker.ReportProgress(50);
                     base.Invoke(new MethodInvoker(delegate
                     {
                         ignoreSelectedIndexChanged = true;


                         /*testing purposes
                         List<string> testList = new List<string>();
                         testList.Add("dc2fca43-5bc9-4376-9117-cf040c5b710f");
                         testList.Add("3502b8af-bf4d-42ea-87eb-97a4e1087a42");
                         nraFirstCheck.firstCheckDictionary.Add("1272", testList);
                         */
                        List<string> keys = nraFirstCheck.firstCheckDictionary.Keys.Cast<string>().ToList();
                        keys.Insert(0, "All");
                        nraFirst_AccountsListBox.DataSource = keys;
                        nraFirst_AccountsListBox.ClearSelected();
                        nraFirstCheck.loadChannelChannels();
                        ignoreSelectedIndexChanged = false;
                    }));
                }
                worker.ReportProgress(100);
            }
        }

        private void nraFirstCheckQueryWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            base.Invoke(new MethodInvoker(delegate
            {
                switch (e.ProgressPercentage)
                {
                    case 0:
                        {
                            addOutput("Starting NRA First Check Query...");
                            setNRAFirstCheckQueryStatus("Starting...");
                            break;
                        }
                    case 50:
                        {
                            addOutput("Query Successful. Adding results to lists...");
                            setNRAFirstCheckQueryStatus("Successful. Adding results to lists...");
                            break;
                        }
                    case 100:
                        {
                            addOutput("Comlpeted NRA First Check Query");
                            setNRAFirstCheckQueryStatus("Complete");
                            break;
                        }
                }
            }));

        }

        private void nraFirstCheckQueryWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.Invoke(new MethodInvoker(delegate
            {
                if ((e.Cancelled == true))
                {
                    addOutput("NRA Check Query Canceled");
                    setNRAFirstCheckQueryStatus("Canceled!");
                }

                else if (!(e.Error == null))
                {
                    addOutput("Error: " + e.Error.Message);
                    setNRAFirstCheckQueryStatus("Error:" + e.Error.Message);
                }

                else
                {
                    //addOutput("Executed both shipping and order workflows");
                    // nraFirstCheck_StatusLabel.Text = "Executed both shipping and order workflows";
                }

            }));
        }

        private void nraFirstCheckOrderProcessWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            if (worker.CancellationPending == true)
            {
                e.Cancel = true;
                return;
            }
            else
            {
                worker.ReportProgress(0);

                List<string> ordersList = new List<string>();
                // ordersList.Add("3502b8af-bf4d-42ea-87eb-97a4e1087a42");
                string workflowID = "";
                string accountNum = "";
                List<string> accountsList = new List<string>();
                int accountsIndex = 0;
                base.Invoke(new MethodInvoker(delegate
                {
                    accountsList = nraFirst_AccountsListBox.Items.Cast<string>().ToList();
                    accountsIndex = nraFirst_AccountsListBox.SelectedIndex;
                }));
                if (accountsIndex == 0)
                {
                    foreach (string account in accountsList)
                    {
                        if (account == "All")
                        {
                            continue;
                        }
                        accountNum = account;

                        //get status 5 orders
                        ordersList = nraFirstCheck.firstCheckDictionary_Status5[accountNum];
                        ordersList = ordersList.Distinct<string>().ToList<string>();
                        base.Invoke(new MethodInvoker(delegate
                        {
                            setNRAFirstCheckQueryStatus("Getting Shipping Wofklow for Account " + accountNum);
                        }));
                        //get shipping workflow
                        workflowID = nraFirstCheck.getShippingWorkflowID(account);
                        int count = 0;
                        //execute workflow
                        foreach (string id in ordersList)
                        {
                            if (String.IsNullOrEmpty(id) || String.IsNullOrWhiteSpace(id))
                                continue;
                            base.Invoke(new MethodInvoker(delegate
                            {
                                setNRAFirstCheckQueryStatus("Executing Order Shipping for Account " + accountNum + " (" + count + " of " + ordersList.Count + ")");
                            }));
                            if (worker.CancellationPending == true)
                            {
                                e.Cancel = true;
                                return;
                            }
                            nraFirstCheck.executeWorkflow(workflowID, id);
                            count++;
                        }
                        base.Invoke(new MethodInvoker(delegate
                        {
                            setNRAFirstCheckQueryStatus("Getting Order Wofklow for Account " + accountNum);
                        }));
                        //get order workflow
                        workflowID = nraFirstCheck.getOrderProcessingWorkflowID(account);
                        count = 0;
                        //execute workflow
                        foreach (string id in ordersList)
                        {
                            if(String.IsNullOrEmpty(id) || String.IsNullOrWhiteSpace(id))
                                continue;
                            base.Invoke(new MethodInvoker(delegate
                            {
                                setNRAFirstCheckQueryStatus("Executing Order Processor for Account " + accountNum + " (" + count + " of " + ordersList.Count + ")");
                            }));
                            if (worker.CancellationPending == true)
                            {
                                e.Cancel = true;
                                return;
                            }
                            nraFirstCheck.executeWorkflow(workflowID, id);
                            count++;
                        }

                        //Process orders status not 5
                        ordersList = nraFirstCheck.firstCheckDictionary[accountNum];
                        ordersList = ordersList.Distinct<string>().ToList<string>();                       
                        base.Invoke(new MethodInvoker(delegate
                        {
                            setNRAFirstCheckQueryStatus("Getting Order Wofklow for Account " + accountNum);
                        }));
                        //get order workflow
                        workflowID = nraFirstCheck.getOrderProcessingWorkflowID(account);
                        count = 0;
                        //execute workflow
                        foreach (string id in ordersList)
                        {
                            if (String.IsNullOrEmpty(id) || String.IsNullOrWhiteSpace(id))
                                continue;
                            base.Invoke(new MethodInvoker(delegate
                            {
                                setNRAFirstCheckQueryStatus("Executing Order Processor for Account " + accountNum + " (" + count + " of " + ordersList.Count + ")");
                            }));
                            if (worker.CancellationPending == true)
                            {
                                e.Cancel = true;
                                return;
                            }
                            nraFirstCheck.executeWorkflow(workflowID, id);
                            count++;
                        }
                    }
                }
                else
                {
                    base.Invoke(new MethodInvoker(delegate
                    {
                        accountNum = nraFirst_AccountsListBox.SelectedItem.ToString();
                    }));
                    //get orders
                    ordersList = nraFirstCheck.firstCheckDictionary[accountNum];
                    ordersList = ordersList.Distinct<string>().ToList<string>();
                    //get shipping workflow
                    workflowID = nraFirstCheck.getShippingWorkflowID(accountNum);
                    //execute workflow
                    nraFirstCheck.executeWorkflow(workflowID, ordersList);
                    //get order workflow
                    workflowID = nraFirstCheck.getOrderProcessingWorkflowID(accountNum);
                    //execute workflow
                    nraFirstCheck.executeWorkflow(workflowID, ordersList);
                }


                /*Order shipping process
                workflowID = nraFirstCheck.getShippingWorkflowID(1272+"");
                base.Invoke(new MethodInvoker(delegate
                {
                    // workflowID = nraFirstCheck_workflowsComboBox.SelectedValue.ToString();
                    Console.WriteLine("WorkflowID: " + workflowID);
                    addOutput("WorkflowID: " + workflowID);
                }));
                //int bracketStart = workflowID.IndexOf("[");
                // int commaIndex = workflowID.IndexOf(",");
                // workflowID = workflowID.Substring(bracketStart + 1, commaIndex - 1);
                nraFirstCheck.executeWorkflow(workflowID, ordersList);
                worker.ReportProgress(50);

                //Order processing
                ordersList = new List<string>();
                ordersList.Add("3502b8af-bf4d-42ea-87eb-97a4e1087a42");
                workflowID = "19882";
                //workflowID = nraFirstCheck.getOrderProcessingWorkflowID(1272);
                base.Invoke(new MethodInvoker(delegate
                {
                    // workflowID = nraFirstCheck_workflowsComboBox.SelectedValue.ToString();
                    Console.WriteLine("WorkflowID: " + workflowID);
                    addOutput("WorkflowID: " + workflowID);
                }));
                //int bracketStart = workflowID.IndexOf("[");
                // int commaIndex = workflowID.IndexOf(",");
                // workflowID = workflowID.Substring(bracketStart + 1, commaIndex - 1);
                //nraFirstCheck.executeWorkflow(workflowID, ordersList);
                //bracketStart = workflowID.IndexOf("[");
                //commaIndex = workflowID.IndexOf(",");
                //workflowID = workflowID.Substring(bracketStart + 1, commaIndex - 1);
               // nraManager.executeOrderShipping(workflowID, ordersList);
               */
            }
        }

        private void nraFirstCheckOrderProcessWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            base.Invoke(new MethodInvoker(delegate
            {
                switch (e.ProgressPercentage)
                {
                    case 50:
                        {
                            addOutput("Order Shipping Process Executed. Beginning Order Processing Execution...");
                            nraFirstCheck_StatusLabel.Text = "Order Shipping Process Executed. Beginning Order Processing Execution...";
                            break;
                        }
                    case 75:
                        {
                            addOutput("Waiting 3 seconds to start order processor...");
                            nraFirstCheck_StatusLabel.Text = "Waiting 3 seconds to start order processor...";
                            break;
                        }
                    case 100:
                        {
                            addOutput("Complete");
                            nraFirstCheck_StatusLabel.Text = "Executed both shipping and order workflows";
                            break;
                        }
                }
            }));

        }

        private void nraFirstCheckOrderProcessWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            base.Invoke(new MethodInvoker(delegate
            {
                if ((e.Cancelled == true))
                {
                    addOutput("Canceled");
                    nraFirstCheck_StatusLabel.Text = "Canceled!";
                }

                else if (!(e.Error == null))
                {
                    addOutput("Error!" + e.Error.Message);
                    nraFirstCheck_StatusLabel.Text = "Error!" + e.Error.Message;
                }

                else
                {
                    addOutput("Executed both shipping and order workflows");
                    nraFirstCheck_StatusLabel.Text = "Executed both shipping and order workflows";
                }

            }));
        }

        #region Backgroundworker Example - ToDo Delete
        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            for (int i = 1; (i <= 10); i++)
            {
                if ((worker.CancellationPending == true))
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    // Perform a time consuming operation and report progress.
                    System.Threading.Thread.Sleep(500);
                    worker.ReportProgress((i * 10));
                }
            }
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Console.WriteLine(e.ProgressPercentage.ToString() + "%");
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((e.Cancelled == true))
            {
                Console.WriteLine("Canceled!");
            }

            else if (!(e.Error == null))
            {
                Console.WriteLine("Error: " + e.Error.Message);
            }

            else
            {
                Console.WriteLine("Done!");
            }
        }
        #endregion

        [STAThread]
        static void Main(string[] args)
        {
            Main_Form mainForm;
            System.Windows.Forms.Application.Run(mainForm = new Main_Form());
            mainForm.FormBorderStyle = FormBorderStyle.FixedSingle;
            mainForm.MaximizeBox = false;
        }

        private void fillAccountsToQueryList()
        {
            List<string> list = new List<string>();

            try
            {
                list = System.IO.File.ReadAllLines(saveFile).ToList();
            }
            catch (Exception ex)
            {

                base.Invoke(new MethodInvoker(delegate
                {
                    // MessageBox.Show("Error opening and reading file.\nCould not fill accounts to query list.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    addOutput("Error opening files to fill query accounts.");
                    addOutput(ex.Message);
                }));
                return;
            }
            List<string> accountList = new List<string>();//list of accounts in text file
            checkedErrors = new List<string>();//list of errors already checked and forced to possible list

            int possibleErrors_Start_Index = list.IndexOf("#PossibleErrors_Start");
            for (int i = 0; i < possibleErrors_Start_Index; i++)
            {
                if (!list[i].StartsWith("#"))
                {
                    accountList.Add(list[i]);
                }
            }
            for (int i = possibleErrors_Start_Index; i < list.Count; i++)
            {
                if (!list[i].StartsWith("#"))
                {
                    checkedErrors.Add(list[i]);
                }
            }
            /*foreach (string line in list)
             {
                
              Console.WriteLine("\t" + line);
            }
                //Remove any lines with #, used for comments in text file
                for (int i = list.Count - 1; i >= 0; i--)
            {
                if (list[i].StartsWith("#"))
                {
                    if (readingAccounts && list[i].Contains("PossibleErrors_Start"))
                    {
                        readingAccounts = false;
                    }
                    list.RemoveAt(i);
                }
                if (readingAccounts)
                {
                    accountList.Add(list[i]);
                }
                else
                {

                }
            }*/

            base.Invoke(new MethodInvoker(delegate
            {
                ignoreSelectedIndexChanged = true;
                accountList.Insert(0, "All");
                accountsQueryListBox.DataSource = accountList;
                ignoreSelectedIndexChanged = false;
            }));
        }

        private void loadTextFile()
        {
            base.Invoke(new MethodInvoker(delegate
            {
                ignoreSelectedIndexChanged = true;
                accountsQueryListBox.DataSource = null;
                accountsQueryListBox.ClearSelected();
                ignoreSelectedIndexChanged = false;
            }));
            List<string> textFileList = new List<string>();
            try
            {
                textFileList = System.IO.File.ReadAllLines(saveFile).ToList();
            }
            catch (Exception ex)
            {
                base.Invoke(new MethodInvoker(delegate
                {
                    // MessageBox.Show("Error loading text file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    addOutput("Error loading text file.");
                    addOutput(ex.Message);
                }));
                return;
            }
            List<string> accountList = new List<string>();//list of accounts in text file
            checkedErrors = new List<string>();//list of errors already checked and forced to possible list

            int possibleErrors_Start_Index = textFileList.IndexOf("#PossibleErrors_Start");
            for (int i = 0; i < possibleErrors_Start_Index; i++)
            {
                if (!textFileList[i].StartsWith("#"))
                {
                    accountList.Add(textFileList[i]);
                }
            }

            List<string> errors_ToRecurringAccountList = new List<string>();
            for (int i = possibleErrors_Start_Index; i < textFileList.Count; i++)
            {
                if (!textFileList[i].StartsWith("#"))
                {
                    //split to comma to 2 items
                    string item = textFileList[i];
                    string accountStr = item.Substring(0, item.IndexOf(','));
                    string errorStr = item.Substring(item.IndexOf(',') + 1);
                    checkedErrors.Add(accountStr);
                    checkedErrors.Add(errorStr);
                    //check if account error already exists, if not add it to recurring errors list, otherwise just adds the error
                    errors_ToRecurringAccountList = new List<string>();
                    if (!recurringErrors.ContainsKey(Convert.ToInt32(accountStr)))
                    {
                        errors_ToRecurringAccountList.Add(errorStr);
                        recurringErrors.Add(Convert.ToInt32(accountStr), errors_ToRecurringAccountList);
                        addOutput(Convert.ToInt32(accountStr) + "  ,   " + errors_ToRecurringAccountList);
                    }
                    else
                    {
                        errors_ToRecurringAccountList = recurringErrors[Convert.ToInt32(accountStr)];
                        errors_ToRecurringAccountList.Add(errorStr);
                        recurringErrors[Convert.ToInt32(accountStr)] = errors_ToRecurringAccountList;
                    }
                }
            }

            /*  foreach (KeyValuePair<int, List<string>> entry in recurringErrors)
              {
                  foreach (string x in entry.Value)
                  {
                      addOutput(entry.Key + " = " + x);
                  }
              }
              */

            base.Invoke(new MethodInvoker(delegate
            {
                accountList.Insert(0, "All");
                accountsQueryListBox.DataSource = accountList;

                List<string> possibleAccountList = new List<string>();
                possibleAccountList = possibleAccountListBox.Items.Cast<String>().ToList();
                //adds every other number from checkedErrors to possible account list if distinct
                for (int i = 0; i < checkedErrors.Count; i += 2)
                {
                    possibleAccountList.Add(checkedErrors[i]);
                }
                ignoreSelectedIndexChanged = true;
                possibleAccountList.Insert(0, "All");
                possibleAccountList = possibleAccountList.Distinct<string>().ToList();
                possibleAccountListBox.DataSource = possibleAccountList;
                possibleAccountListBox.ClearSelected();
                ignoreSelectedIndexChanged = false;
            }));
        }

        /// <summary>
        /// Adds all possible errors for the account given. Will also change label "Errors (total)" to the total added
        /// </summary>
        /// <param name="accountNum">The account selected. 0 for all accounts.</param>
        private void setErrorListBox(int accountNum)
        {
            ignoreSelectedIndexChanged = true;
            List<string> list = new List<string>();
            for (int i = 0; i < ShopifyManager.errors_Account_List.Count; i++)
            {
                if (accountNum != 0)
                {
                    foreach (string x in ShopifyManager.errors_Account_List[accountNum])
                    {
                        list.Add(x);
                    }
                }
                else
                {
                    foreach (List<string> x in ShopifyManager.errors_Account_List.Values)
                    {
                        foreach (string y in x)
                        {
                            list.Add(y);
                        }
                    }
                }
            }

            if (accountListBox.SelectedIndex != -1 && accountListBox.SelectedIndex != 0)
            {
                //addOutput("Keys: ");
                //remove any saved/checked errors
                try
                {
                    foreach (int x in recurringErrors.Keys)
                    {
                        //addOutput("\t" + x);
                        int selectedAccount = Convert.ToInt32(accountListBox.SelectedItem);
                        if (x == selectedAccount)
                        {
                            for (int i = list.Count - 1; i >= 0; i--)
                            {
                                foreach (string y in recurringErrors[x].ToList())
                                {
                                    // addOutput("Testtt: "+y);
                                    if (y == list[i])
                                    {
                                        list.Remove(y);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    addOutput("Error on adding recurring errors to list.");
                    addOutput(ex.Message);
                }
            }
            else if (accountListBox.SelectedIndex == 0)
            {
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    foreach (KeyValuePair<int, List<string>> errorPair in recurringErrors)
                    {
                        // do something with entry.Value or entry.Key
                        for (int j = 0; j < errorPair.Value.Count; j++)
                        {
                            if (errorPair.Value[j].Equals(list[i]))
                            {
                                list.RemoveAt(i);
                            }
                        }
                    }
                }
            }

            /*else if(accountListBox.SelectedIndex == 0)//remove all recurring errors
            {
                foreach (int x in recurringErrors.Keys)
                {
                    foreach(string account in accountListBox.Items)
                    {
                        if(account == "All")
                        {
                            continue;
                        }
                        int selectedAccount = Convert.ToInt32(account);
                        if (x == selectedAccount)
                        {
                            for (int i = list.Count - 1; i >= 0; i--)
                            {
                                foreach (string y in recurringErrors[x].ToList())
                                {
                                    addOutput("Testtt: " + y);
                                    if (y == list[i])
                                    {
                                        list.Remove(y);
                                    }
                                }
                            }
                        }
                    }                    
                }
            }*/

            list = list.Distinct().ToList();
            errorsLabel.Text = "Errors (" + list.Count + ")";
            //  errorListBox.Items.Clear();
            errorListBox.DataSource = null;

            errorListBox.DataSource = list;
            errorListBox.ClearSelected();
            errorListBox.Refresh();

            ignoreSelectedIndexChanged = false;
        }

        /// <summary>
        /// Adds all possible errors for the account given.
        /// </summary>
        /// <param name="accountNum">The account selected. 0 for all accounts.</param>
        private void setPossibleErrorListBox(int accountNum)
        {
            List<string> errors_PossibleList = new List<string>();
            //check if possible account exists, if not add it
            /*  if (accountNum != 0 && !possibleAccountListBox.Items.Contains(accountNum))
              {
                  ignoreSelectedIndexChanged = true;
                  errors_PossibleList = possibleAccountListBox.Items.Cast<String>().ToList();
                  errors_PossibleList.Add(accountNum+"");
                  possibleAccountListBox.DataSource = errors_PossibleList;
                  possibleAccountListBox.ClearSelected();
                  ignoreSelectedIndexChanged = false;
              }*/
            try
            {
                for (int i = 0; i < ShopifyManager.possible_Errors_Account_List.Count; i++)
                {
                    if (accountNum != 0)
                    {
                        foreach (string x in ShopifyManager.possible_Errors_Account_List[accountNum])
                        {
                            errors_PossibleList.Add(x);
                        }
                    }
                    else
                    {
                        foreach (List<string> x in ShopifyManager.possible_Errors_Account_List.Values)
                        {
                            foreach (string y in x)
                            {
                                errors_PossibleList.Add(y);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception on getting possible errors");
                addOutput("Exception on getting possible errors.");
                addOutput(ex.Message);
                ignoreSelectedIndexChanged = false;
            }
            //add any recurring errors
            foreach (KeyValuePair<int, List<string>> entry in recurringErrors)
            {
                if (entry.Key == accountNum || accountNum == 0)
                {
                    foreach (string x in entry.Value)
                    {
                        if (!errors_PossibleList.Contains(x))
                        {
                            errors_PossibleList.Add(x);
                        }
                    }
                }
            }
            /* bool useThisAccount = false;//used to determine if the account number selected is the account number currently indexed in the for loop
             //add any saved/checked errors   skip first index for "All"
             for (int i = 1; i < checkedErrors.Count; i ++)
             {
                 if (useThisAccount)
                 {
                     useThisAccount = false;
                     list.Add(checkedErrors[i]);
                 }
                 if(Convert.ToInt32(checkedErrors[i]) == accountNum)
                 {
                     useThisAccount = true;
                 }
             }

               /*  foreach (string y in checkedErrors)
                 {
                     int commaIndex = y.IndexOf(",") - 1;
                     string num1 = y.Substring(0, commaIndex);
                     string num2 = y.Substring(commaIndex);
                     if (Convert.ToInt32(num1) == accountNum)
                     {
                         if (!list.Contains(num1))
                         {
                             list.Add(num1);
                         }
                     }
                 }*/
            ignoreSelectedIndexChanged = true;
            errors_PossibleList = errors_PossibleList.Distinct<string>().ToList();
            possibleErrorsLabel.Text = "Possible Errors (" + errors_PossibleList.Count + ")";
            possibleErrorListBox.DataSource = errors_PossibleList;
            possibleErrorListBox.ClearSelected();
            ignoreSelectedIndexChanged = false;
            /* }catch(Exception ex)
             {
                 Console.WriteLine("Exception on getting possible errors");
             }*/
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            if (loadingFile)
            {
                MessageBox.Show("Please wait, loading text file.", "Loading Text File", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (shopifyWorker.IsBusy != true)
            {
                shopifyWorker.RunWorkerAsync();
            }
            return;

            #region Old Code - Moved to Shopify DoWork
            addOutput("!! Start Query Button Pressed !!");
            queryStatusLabel.Text = "Query Status: Working...";
            queryStatusLabel.Refresh();
            //Makes the info text boxes grey to make clear they are not neccessarily the current info
            //change the text and color for each info textbox
            //must change backcolor first to change forecolor on readonly textbox
            Color foreColor = Color.Black;
            foreColor = Color.Gray;
            dateCreatedTextBox.BackColor = dateCreatedTextBox.BackColor;
            dateCreatedTextBox.ForeColor = foreColor;
            dateCreatedTextBox.Refresh();
            dateModifiedTextBox.BackColor = dateModifiedTextBox.BackColor;
            dateModifiedTextBox.ForeColor = foreColor;
            dateModifiedTextBox.Refresh();
            referenceNumberTextBox.BackColor = referenceNumberTextBox.BackColor;
            referenceNumberTextBox.ForeColor = foreColor;
            referenceNumberTextBox.Refresh();
            idTextBox.BackColor = idTextBox.BackColor;
            idTextBox.ForeColor = foreColor;
            idTextBox.Refresh();

            ignoreSelectedIndexChanged = true;
            ShopifyManager.errors_Account_List.Clear();
            ShopifyManager.possible_Errors_Account_List.Clear();
            ShopifyManager.accountNum_Errors.Clear();
            possibleAccountListBox.DataSource = null;
            possibleAccountListBox.Items.Clear();
            possibleAccountListBox.Refresh();
            possibleErrorListBox.DataSource = null;
            possibleErrorListBox.Items.Clear();
            possibleErrorListBox.Refresh();
            errorListBox.ClearSelected();
            errorListBox.DataSource = null;
            errorListBox.Items.Clear();
            errorListBox.Refresh();
            accountListBox.DataSource = null;
            accountListBox.Items.Clear();
            accountListBox.Refresh();
            shopify_CheckedCheckBox.Checked = false;
            shopify_OrderInfoLabel.Visible = false;
            days = Convert.ToInt32(daysNumericUpDown.Value);
            deleted = deletedCheckBox.Checked;
            orderTypeID = Convert.ToInt32(orderTypeIDNumericUpDown.Value);
            if (accountsQueryListBox.SelectedIndex == 0)
            {
                foreach (string x in accountsQueryListBox.Items)
                {
                    if (x == "All")
                    {
                        continue;
                    }
                    queryStatusLabel.Text = "Query Status: Working on " + x + "...";
                    queryStatusLabel.Refresh();

                    shopifyManager.newShopifyQuery("", Convert.ToInt32(x), days, deleted, orderTypeID);
                }
            }
            else
            {
                foreach (string x in accountsQueryListBox.SelectedItems)
                {
                    queryStatusLabel.Text = "Query Status: Working on " + x + "...";
                    queryStatusLabel.Refresh();
                    shopifyManager.newShopifyQuery("", Convert.ToInt32(x), days, deleted, orderTypeID);
                }
            }

            //  Console.WriteLine("Total Errors Found: " + manager.getTotalErrors() + ". In Accounts: ");
            if (shopifyManager.getTotalErrors() == 0)
            {
                addOutput("No errors were found.");
            }
            else
            {
                addOutput("Total Errors Found: " + shopifyManager.getTotalErrors() + ". In Accounts: ");
                foreach (int x in ShopifyManager.accountNum_Errors)
                {
                    // Console.WriteLine(x);
                    addOutput(x + "");
                }
            }

            List<int> acconts_ErrorsList = new List<int>();
            List<string> accounts_PossibleList = new List<string>();
            /*for (int i = 0; i < Manager.errors_Account_List.Count; i++)
            {
                list1 = Manager.errors_Account_List.Keys.ToList();
                foreach (List<string> x in Manager.errors_Account_List.Values)
                {
                    foreach (string y in x)
                    {
                        list2.Add(y);
                    }
                }
            }*/
            acconts_ErrorsList = ShopifyManager.errors_Account_List.Keys.ToList();

            List<int> addToPossibleList = new List<int>();


            //remove saved/checked errors
            /*for(int i = list1.Count - 1; i >= 0; i --)
            {
                foreach(string y in checkedErrors)
                {
                    int commaIndex = y.IndexOf(",");
                    string num1 = y.Substring(0, commaIndex);
                    if(Convert.ToInt32(num1) == list1[i])
                    {
                        addToPossibleList.Add(list1[i]);
                        list1.RemoveAt(i);
                    }
                }
            }
            for (int i = acconts_ErrorsList.Count - 1; i >= 0; i-=2)
            {
                foreach (string y in checkedErrors)
                {
                    
                    if (Convert.ToInt32(y) == acconts_ErrorsList[i])
                    {
                        addToPossibleList.Add(acconts_ErrorsList[i]);
                        acconts_ErrorsList.RemoveAt(i);
                    }
                }
            }*/

            accounts_PossibleList.Add("All");
            foreach (int x in acconts_ErrorsList)
            {
                //  if (!recurringErrors.ContainsKey(x))
                //  {
                accounts_PossibleList.Add(x + "");
                //  }
            }

            ignoreSelectedIndexChanged = true;
            accountListBox.DataSource = accounts_PossibleList;
            accountListBox.ClearSelected();
            ignoreSelectedIndexChanged = false;

            //Add possible errors
            acconts_ErrorsList = new List<int>();
            accounts_PossibleList = new List<string>();
            /*for (int i = 0; i < Manager.errors_Account_List.Count; i++)
            {
                list1 = Manager.errors_Account_List.Keys.ToList();
                foreach (List<string> x in Manager.errors_Account_List.Values)
                {
                    foreach (string y in x)
                    {
                        list2.Add(y);
                    }
                }
            }*/
            acconts_ErrorsList = ShopifyManager.possible_Errors_Account_List.Keys.ToList();
            accounts_PossibleList.Add("All");
            foreach (int x in acconts_ErrorsList)
            {
                accounts_PossibleList.Add(x + "");
            }
            foreach (int x in addToPossibleList)
            {
                if (!accounts_PossibleList.Contains(x + ""))
                {
                    accounts_PossibleList.Add(x + "");
                }
            }

            //Checks if possible list contains recurring errors, if not add it
            foreach (KeyValuePair<int, List<string>> entry in recurringErrors)
            {
                if (!accounts_PossibleList.Contains(entry.Key.ToString()))
                {
                    accounts_PossibleList.Add(entry.Key.ToString());
                }
            }
            ignoreSelectedIndexChanged = true;
            possibleAccountListBox.DataSource = accounts_PossibleList;
            possibleAccountListBox.ClearSelected();
            ignoreSelectedIndexChanged = false;
            //remove any recurring errors
            #endregion
        }

        private void errorListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged)
            {
                return;
            }
            ignoreSelectedIndexChanged = true;
            possibleErrorListBox.ClearSelected();
            possibleAccountListBox.ClearSelected();
            if (errorListBox.SelectedItems.Count == 1)
            {
                //System.Diagnostics.Process.Start("https://portal.nchannel.com/Home/BoxLogin");
            }
            else
            {
                // MessageBox.Show("You must select one order to view in portal.", "Too Many Orders Selected",MessageBoxButtons.OK);
            }
            //show checked box
            shopify_CheckedCheckBox.Visible = true;
            if (errorListBox.SelectedItem.ToString().Contains("Checked"))
            {
                shopify_CheckedCheckBox.Checked = true;
            }
            else
            {
                shopify_CheckedCheckBox.Checked = false;
            }

            ignoreSelectedIndexChanged = false;
            /*   List<string> list = new List<string>();
             *   
             *   if (accountListBox.SelectedIndex != 0 && accountListBox.SelectedIndex != -1)
             {
               //  addOutput("Keys: ");
                 //remove any saved/checked errors
                 foreach (int x in recurringErrors.Keys)
                 {
                    // addOutput("\t" + x);
                     int selectedAccount = Convert.ToInt32(accountListBox.SelectedItem);
                     if (x == selectedAccount)
                     {
                         foreach (string selected in errorListBox.SelectedItems)
                         {
                             foreach (string y in recurringErrors[x])
                             {
                                 if (y == selected)
                                 {
                                     list.Remove(y);
                                 }
                             }
                         }
                     }
                 }
             }
            list = list.Distinct().ToList();
            errorsLabel.Text = "Errors (" + list.Count + ")";
            errorListBox.DataSource = list;
            errorListBox.ClearSelected();
            ignoreSelectedIndexChanged = false;*/
            /*string error = errorListBox.SelectedItem.ToString();
            Console.WriteLine("error: " + error);
            int account = 0;
            foreach(List<string> x in Manager.errors_Account_List.Values)
            {
                foreach(string y in x)
                {
                    if(Manager.errors_Account_List.)
                }
            }
            account = Manager.errors_Account_List.FirstOrDefault(x => x.Value == error).Key;
            accountListBox.SelectedItem = account;*/
        }

        private void accountListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (shopifyCheckErrorWorker.IsBusy)
            {
                return;
            }
            if (ignoreSelectedIndexChanged)
            {
                return;
            }
            ignoreSelectedIndexChanged = true;
            errorListBox.ClearSelected();
            possibleAccountListBox.ClearSelected();
            possibleErrorListBox.ClearSelected();
            ignoreSelectedIndexChanged = false;
            int account = 0;
            if (accountListBox.SelectedIndex != 0)
            {
                account = Convert.ToInt32(accountListBox.SelectedItem);
            }
            setErrorListBox(account);

            ignoreSelectedIndexChanged = true;

            //hide checked box
            shopify_CheckedCheckBox.Visible = false;

            ignoreSelectedIndexChanged = false;
        }

        private void errorListBox_MouseClick(object sender, MouseEventArgs e)
        {
            Console.WriteLine("Clicked");
            if (e.Button == MouseButtons.Right)
            {
                Console.WriteLine("Right Clicked");
                try
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (object row in errorListBox.SelectedItems)
                    {
                        sb.Append(row.ToString());
                        sb.AppendLine();
                    }
                    Console.WriteLine(sb);
                    //sb.Remove(sb.Length - 1, 1); // Just to avoid copying last empty row
                    Clipboard.SetData(System.Windows.Forms.DataFormats.Text, sb.ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        /// <summary>
        /// Displays the given time and calculates if time falls within Normal, Slow, Fast, or Error based on average queries from previous sessions
        /// </summary>
        /// <param name="time">The time in miliseconds elapsed</param>
        public void setElapsedTime(long time)
        {
            base.Invoke(new MethodInvoker(delegate
            {
                TimeSpan span = new TimeSpan(time);
                elapsedTimeLabel.Text = "Elapsed Time: " + span.ToString(@"mm\:ss\.fffffff");
                string status = "Could not determine";
                if (span.TotalSeconds > 60)
                {
                    status = "Slow";
                }
                else if (span.TotalMilliseconds <= .002)
                {
                    status = "Fast " + span.TotalMilliseconds;
                }
                else if (time < 0)
                {
                    status = "Error Occured";
                }
                else
                {
                    status = "Normal";
                }
                queryStatusLabel.Text = "Time Based Status: " + status;
                queryStatusLabel.Refresh();
            }));
        }

        /// <summary>
        /// Adds a new line at bottom of Output tab. \n is added to message at the beginning.
        /// </summary>
        /// <param name="message">The message to add to the output tab</param>
        /// <param name="showTimeStamp">Adds message with ShortDate and ShortTime stamp if true (default)</param>
        public void addOutput(string message, bool showTimeStamp = true)
        {
            if(showOutputLevel == 3)
            {
                return;
            }
            if (showOutputLevel == 1 && showTimeStamp)
            {
                message = DateTime.Now.ToLongTimeString() + "  " + DateTime.Now.ToString("M/d/y") + " ::: " + message;
            }
            try
            {
                outputTextBox.Text = outputTextBox.Text + "\n" + message;
                //sets to auto scroll to bottom
                outputTextBox.SelectionStart = outputTextBox.Text.Length;
                outputTextBox.ScrollToCaret();
            }
            catch (InvalidOperationException ex)
            {
                base.Invoke(new MethodInvoker(delegate
                {
                    outputTextBox.Text = outputTextBox.Text + "\n" + message;
                    //sets to auto scroll to bottom
                    outputTextBox.SelectionStart = outputTextBox.Text.Length;
                    outputTextBox.ScrollToCaret();
                }));
            }

        }

        private void errorListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                e.SuppressKeyPress = true;
                Console.WriteLine("Key Up");
                try
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (object row in errorListBox.SelectedItems)
                    {
                        sb.Append(row.ToString());
                        sb.AppendLine();
                    }
                    Console.WriteLine(sb);
                    //sb.Remove(sb.Length - 1, 1); // Just to avoid copying last empty row
                    Clipboard.SetData(System.Windows.Forms.DataFormats.Text, sb.ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void possibleErrorListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged)
            {
                return;
            }
            ignoreSelectedIndexChanged = true;
            errorListBox.ClearSelected();
            ignoreSelectedIndexChanged = false;
        }

        private void possibleAccountListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged)
            {
                return;
            }
            ignoreSelectedIndexChanged = true;
            accountListBox.ClearSelected();
            errorListBox.ClearSelected();
            possibleErrorListBox.ClearSelected();
            ignoreSelectedIndexChanged = false;
            int account = 0;
            if (possibleAccountListBox.SelectedIndex != 0)
            {
                account = Convert.ToInt32(possibleAccountListBox.SelectedItem);
            }
            setPossibleErrorListBox(account);
        }

        /// <summary>
        /// Sets the NRA First Check query text box
        /// </summary>
        /// <param name="text">The message</param>
        public void setNRAFirstCheckQueryStatus(string text)
        {
            nraFirstCheck_StatusLabel.Text = "Query Status: " + text;
            nraFirstCheck_StatusLabel.Refresh();
        }

        /// <summary>
        /// Sets the shopify query status label
        /// </summary>
        /// <param name="text">The message</param>
        public void setShopifyQueryStatus(string text)
        {
            queryStatusLabel.Text = "Query Status: " + text;
            queryStatusLabel.Refresh();
        }

        /// <summary>
        /// Sets the shopify last query text box
        /// </summary>
        /// <param name="text">The query string</param>
        public void setLastQueryText(string text)
        {
            lastQueryRichTextBox.Text = text;
        }

        /// <summary>
        /// Sets the fore color of the given color and refreshes all textboxes in the order info section with shopify
        /// </summary>
        /// <param name="color"></param>
        private void updateOrderInfo(Color foreColor)
        {
            dateCreatedTextBox.BackColor = dateCreatedTextBox.BackColor;
            dateCreatedTextBox.ForeColor = foreColor;
            dateCreatedTextBox.Refresh();
            dateModifiedTextBox.BackColor = dateModifiedTextBox.BackColor;
            dateModifiedTextBox.ForeColor = foreColor;
            dateModifiedTextBox.Refresh();
            referenceNumberTextBox.BackColor = referenceNumberTextBox.BackColor;
            referenceNumberTextBox.ForeColor = foreColor;
            referenceNumberTextBox.Refresh();
            idTextBox.BackColor = idTextBox.BackColor;
            idTextBox.ForeColor = foreColor;
            idTextBox.Refresh();
            accountNameTextBox.BackColor = accountNameTextBox.BackColor;
            accountNameTextBox.ForeColor = foreColor;
            accountNameTextBox.Refresh();
            accountNumberTextBox.BackColor = accountNumberTextBox.BackColor;
            accountNumberTextBox.ForeColor = foreColor;
            accountNumberTextBox.Refresh();
            statusTextBox.BackColor = statusTextBox.BackColor;
            statusTextBox.ForeColor = foreColor;
            statusTextBox.Refresh();
            shopify_OrderInfoLabel.BackColor = shopify_OrderInfoLabel.BackColor;
            shopify_OrderInfoLabel.ForeColor = foreColor;
            shopify_OrderInfoLabel.Refresh();
        }

        private void queryMoreInfoButton_Click(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged)
            {
                MessageBox.Show("Something in the background might be changing the lists, please try again.", "Query More Info Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            bool error = false;
            if (errorListBox.SelectedIndex != -1)//trying to query a regular error
            {
                if (accountListBox.SelectedIndex == -1 || accountListBox.SelectedIndex == 0)
                {
                    error = true;
                }
            }
            else if (possibleErrorListBox.SelectedIndex != -1)//trying to query a possible error
            {
                if (possibleAccountListBox.SelectedIndex == -1 || possibleAccountListBox.SelectedIndex == 0)
                {
                    error = true;
                }
            }
            else if (errorListBox.SelectedIndex == -1 && possibleErrorListBox.SelectedIndex == -1)
            {
                error = true;
            }
            if (error)
            {
                MessageBox.Show("Must select an account with an error to find more info.", "Error");
                return;
            }
            addOutput("=====================================");
            addOutput("Start More Info Query Button Pressed");
            addOutput("=====================================");

            //Makes the info text boxes grey to make clear they are not neccessarily the current info
            //change the text and color for each info textbox
            //must change backcolor first to change forecolor on readonly textbox
            Color foreColor = Color.Black;
            foreColor = Color.Gray;
            updateOrderInfo(foreColor);
            foreColor = Color.Black;
            shopify_OrderInfoLabel.Visible = false;

            if (errorListBox.SelectedIndex != -1)
            {
                accountNum = accountListBox.SelectedItem.ToString();
                if (errorListBox.SelectedItem.ToString().Contains(checkedMark))//check if string contains "checked" mark and if so, remove the checked mark
                {
                    errorStr = errorListBox.SelectedItem.ToString().Replace(checkedMark, "");
                }
                else
                {
                    errorStr = errorListBox.SelectedItem.ToString();
                }
                setShopifyQueryStatus("Working on " + accountNum + "...");
                if (!shopifyQueryOrderWorker.IsBusy)
                {
                    shopifyQueryOrderWorker.RunWorkerAsync();
                }
                //shopifyQueryOrderResultList = shopifyManager.queryMoreInfo(accountNum, errorStr, deleted, orderTypeID);
            }
            else if (possibleErrorListBox.SelectedIndex != -1)
            {
                accountNum = possibleAccountListBox.SelectedItem.ToString();
                errorStr = possibleErrorListBox.SelectedItem.ToString();
                setShopifyQueryStatus("Working on " + accountNum + "...");
                if (!shopifyQueryOrderWorker.IsBusy)
                {
                    shopifyQueryOrderWorker.RunWorkerAsync();
                }
                // shopifyQueryOrderResultList = shopifyManager.queryMoreInfo(accountNum, errorStr, deleted, orderTypeID);
            }

        }

        private void processShopifyQueryOrderResults()
        {
            Color foreColor = Color.Black;
            if (shopifyQueryOrderResultList.Count == 0 || shopifyQueryOrderResultList.Count == 1)
            {
                setShopifyQueryStatus("Query Status: No results found.");

                //Makes the info text boxes grey to make clear they are not neccessarily the current info
                //change the text and color for each info textbox
                //must change backcolor first to change forecolor on readonly textbox
                foreColor = Color.Gray;
                updateOrderInfo(foreColor);
                return;
            }
            //change the text and color for each info textbox
            //must change backcolor first to change forecolor on readonly textbox

            idTextBox.Text = shopifyQueryOrderResultList[1];
            referenceNumberTextBox.Text = shopifyQueryOrderResultList[2];
            DateTime dt = DateTime.Parse(shopifyQueryOrderResultList[3]);
            dateModifiedTextBox.Text = dt.ToShortTimeString() + " " + dt.ToString("M/d/y");
            dt = DateTime.Parse(shopifyQueryOrderResultList[4]);
            dateCreatedTextBox.Text = dt.ToShortTimeString() + " " + dt.ToString("M/d/y");
            accountNameTextBox.Text = shopifyQueryOrderResultList[5];
            statusTextBox.Text = shopifyQueryOrderResultList[6];
            accountNumberTextBox.Text = accountNum + "";
            if (referenceNumberTextBox.Text.Contains(errorStr + ""))//the order exists in nChannel
            {
                shopify_OrderInfoLabel.Text = "Order found in nChannel";
                foreColor = Color.DarkGreen;
                updateOrderInfo(foreColor);
                shopify_OrderInfoLabel.Visible = true;
            }
            else
            {
                shopify_OrderInfoLabel.Text = "Order info IS NOT the selected order";
                foreColor = Color.Black;
                updateOrderInfo(foreColor);
                shopify_OrderInfoLabel.Visible = true;
            }
            ignoreSelectedIndexChanged = false;
        }


        private void idTextBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            idTextBox.SelectAll();
        }

        private void resetOptionsButton_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Would you like to reset the options?", "Reset", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                days = 7;
                daysNumericUpDown.Value = 7;
                deleted = false;
                deletedCheckBox.Checked = false;
                accountsQueryListBox.SelectedIndex = 0;
                orderTypeIDNumericUpDown.Value = 1;
            }
        }

        private void accountListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                e.SuppressKeyPress = true;
                try
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (object row in accountListBox.SelectedItems)
                    {
                        sb.Append(row.ToString());
                        sb.AppendLine();
                    }
                    //Console.WriteLine(sb);
                    sb.Remove(sb.Length - 1, 1); // Just to avoid copying last empty row
                    Clipboard.SetData(System.Windows.Forms.DataFormats.Text, sb.ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void possibleErrorListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                e.SuppressKeyPress = true;
                try
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (object row in possibleErrorListBox.SelectedItems)
                    {
                        sb.Append(row.ToString());
                        sb.AppendLine();
                    }
                    //Console.WriteLine(sb);
                    sb.Remove(sb.Length - 1, 1); // Just to avoid copying last empty row
                    Clipboard.SetData(System.Windows.Forms.DataFormats.Text, sb.ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void possibleAccountListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                e.SuppressKeyPress = true;
                try
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (object row in possibleAccountListBox.SelectedItems)
                    {
                        sb.Append(row.ToString());
                        sb.AppendLine();
                    }
                    //  Console.WriteLine(sb);
                    sb.Remove(sb.Length - 1, 1); // Just to avoid copying last empty row
                    Clipboard.SetData(System.Windows.Forms.DataFormats.Text, sb.ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void openSaveFileButton_Click(object sender, EventArgs e)
        {

            if (!File.Exists(saveFile))
            {
                MessageBox.Show("Error opening file. File does not exist or is corrupted.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            try
            {
                Process.Start("notepad.exe", saveFile);
                /* Open the stream and read it back.
                using (StreamReader sr = File.OpenText(path))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(s);
                    }
                }*/
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error opening file. File does not exist or is corrupted.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                addOutput("Error opening file. File does not exist or is corrupted.");
                addOutput(ex.Message);
                return;
            }

        }

        private void accountsQueryListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                e.SuppressKeyPress = true;
                try
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (object row in accountsQueryListBox.SelectedItems)
                    {
                        sb.Append(row.ToString());
                        sb.AppendLine();
                    }
                    //  Console.WriteLine(sb);
                    sb.Remove(sb.Length - 1, 1); // Just to avoid copying last empty row
                    Clipboard.SetData(System.Windows.Forms.DataFormats.Text, sb.ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void addErrorToTextFile(int accountNum, string erroNum)
        {
            List<string> list = new List<string>();
            list = System.IO.File.ReadAllLines(saveFile).ToList();
            int firstPossibleIndex = list.IndexOf("#PossibleErrors_Start");
            if (firstPossibleIndex == -1)
            {
                MessageBox.Show("The file is corrupted, could not find where to store possible errors.", "Error Adding To File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            string newPossibleError = accountNum + "," + erroNum;
            list.Insert(firstPossibleIndex + 1, newPossibleError);
            // Example #2: Write one string to a text file.
            // string text = "A class is the most powerful data type in C#. Like a structure, ";
            // WriteAllText creates a file, writes the specified string to the file,
            // and then closes the file.    You do NOT need to call Flush() or Close().
            System.IO.File.WriteAllLines(saveFile, list);
        }

        private void moveFromErrorsToPossibleList(int accountNum, long errorNum)
        {
            List<string> items = errorListBox.Items.Cast<String>().ToList();
            items.Remove(errorNum + "");
            errorListBox.DataSource = items;
            setPossibleErrorListBox(accountNum);
            items = possibleErrorListBox.Items.Cast<String>().ToList();
            items.Add(errorNum + "");
            possibleErrorListBox.DataSource = items;
        }

        private void loadSettingsButton_Click(object sender, EventArgs e)
        {
            loadTextFile();
        }

        public void setNRAStatusText(string text)
        {
            nraStatusLabel.Text = "Status: " + text;
            nraStatusLabel.Refresh();
        }

        public void nraAddPendingLists()
        {
            List<string> orders = nraManager.nraPendingErrorList;
            for (int i = orders.Count - 1; i >= 0; i--)
            {
                int index = orders.IndexOf(orders[i]);
                int starIndex = orders[i].IndexOf('*');
                orders[index] = orders[i].Substring(0, starIndex);
            }
            orders = orders.Distinct<string>().ToList();

            List<string> distributorsList = new List<string>();// = nraManager.distributorNameList;
            foreach (string x in nraManager.pending_given_OrdersList)
            {
                if (orders.Contains(x))
                {
                    distributorsList.Add(nraManager.pending_distributorNameList[nraManager.pending_given_OrdersList.IndexOf(x)]);
                }
            }
            ignoreSelectedIndexChanged = true;
            distributorsList = distributorsList.Distinct<string>().ToList<string>();
            pending_distributorListBox.DataSource = distributorsList;
            // pending_orderListBox.DataSource = orders;
            pending_distributorListBox.ClearSelected();
            //  pending_orderListBox.ClearSelected();
            ignoreSelectedIndexChanged = false;
        }

        public void nraAddAcknowledgeLists()
        {
            //add acknowledged lists
            List<string> orders = nraManager.nraAcknowledgeErrorList;
            for (int i = orders.Count - 1; i >= 0; i--)
            {
                try
                {
                    int index = orders.IndexOf(orders[i]);
                    int starIndex = orders[i].IndexOf('*');
                    orders[index] = orders[i].Substring(0, starIndex);
                }
                catch (Exception ex)
                {

                }
            }
            orders = orders.Distinct<string>().ToList();

            List<string> distributorsList = new List<string>();// = nraManager.distributorNameList;
            foreach (string x in nraManager.acknowledge_given_OrdersList)
            {
                if (orders.Contains(x))
                {
                    distributorsList.Add(nraManager.acknowledge_distributorNameList[nraManager.acknowledge_given_OrdersList.IndexOf(x)]);
                }
            }
            ignoreSelectedIndexChanged = true;
            distributorsList = distributorsList.Distinct<string>().ToList<string>();
            acknowledge_distributorsListBox.DataSource = distributorsList;
            acknowledge_distributorsListBox.ClearSelected();
            // acknowledge_ordersListBox.DataSource = orders;
            // acknowledge_ordersListBox.ClearSelected();
            ignoreSelectedIndexChanged = false;
        }

        private void startProcessButton_Click(object sender, EventArgs e)
        {
            if (givenExcelFileTextBox.Text == "")
            {
                MessageBox.Show("You must select a file first!", "Cannot process NRA", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            //clear lists
            ignoreSelectedIndexChanged = true;
            pending_distributorListBox.DataSource = null;
            pending_distributorListBox.ClearSelected();
            pending_orderListBox.DataSource = null;
            pending_orderListBox.ClearSelected();
            acknowledge_distributorsListBox.DataSource = null;
            acknowledge_distributorsListBox.ClearSelected();
            acknowledge_ordersListBox.DataSource = null;
            acknowledge_ordersListBox.ClearSelected();
            ignoreSelectedIndexChanged = false;

            addOutput("============================");
            addOutput("Process NRA Excel Button Pressed");
            addOutput("============================");
            setNRAStatusText("Processing...");

            processPending = nra_PendingOnlyRadioButton.Checked;
            processAcknowledge = nra_AcknowledgeRadioButton.Checked;
            if (nra_ProcessBothRadioButton.Checked)
            {
                processPending = true;
                processAcknowledge = true;
            }
            if (!nraExcelWorker.IsBusy)
            {
                nraExcelWorker.RunWorkerAsync();
            }

        }

        private void removeErrorFromList(int accountNum, string errorNum)
        {
            ignoreSelectedIndexChanged = true;
            List<string> items = errorListBox.Items.Cast<String>().ToList();
            items.Remove(errorNum);
            errorListBox.DataSource = items;
            ShopifyManager.errors_Account_List[accountNum] = items;
            ignoreSelectedIndexChanged = false;
        }

        private void recurringButton_Click(object sender, EventArgs e)
        {
            if (accountListBox.SelectedIndex == 0 || accountListBox.SelectedIndex == -1)
            {
                MessageBox.Show("You must select one account and one reference number from the error list to add to recurring.", "Cannot Add To Recurring", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (errorListBox.SelectedIndex == -1)
            {
                MessageBox.Show("You must select one account and one reference number from the error list to add to recurring.", "Cannot Add To Recurring", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int accountNum = Convert.ToInt32(accountListBox.SelectedItem);
           

            string errorListString = "\nAccount: " + accountNum+ "\nReference Numbers:\n";
            foreach(string error in errorListBox.SelectedItems)
            {
                string errorStr = error;
                if (errorStr.Contains(checkedMark))
                {
                    errorStr = errorStr.Replace(checkedMark, "");
                }
                errorListString += "\t" + errorStr + "\n";
            }
            DialogResult result = MessageBox.Show("Move the selected order to recurring list?\nThis will move the order to possible list automatically."+errorListString, "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.No)
            {
                return;
            }
            addOutput("============================");
            addOutput("Add Recurring Button Pressed");
            addOutput("============================");
            foreach(string error in errorListBox.SelectedItems)
            {
                string errorStr = error;
                if (errorStr.Contains(checkedMark))
                {
                    errorStr = errorStr.Replace(checkedMark, "");
                }
                addErrorToTextFile(accountNum, errorStr);
                removeErrorFromList(accountNum, errorStr);
            }
            loadTextFile();
        }

        private void Main_Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            //this.Close();
            Application.Exit();
        }

        private void lastQueryRichTextBox_DoubleClick(object sender, EventArgs e)
        {
            lastQueryRichTextBox.SelectAll();
            return;
        }

        private void Main_Form_Load(object sender, EventArgs e)
        {
            nraFirstDateTimePicker.Value = DateTime.Today.AddDays(-7);
            idleWorker.RunWorkerAsync();
        }

        private void selectExcelFileButton_Click(object sender, EventArgs e)
        {
            // Create an instance of the open file dialog box.
            NRAopenFileDialog = new OpenFileDialog();

            // Set filter options and filter index.
            NRAopenFileDialog.Filter = "Excel Files (.xls, .xlsx)|*.xls;*.xlsx|All Files (*.*)|*.*";
            NRAopenFileDialog.FilterIndex = 1;

            NRAopenFileDialog.Multiselect = true;

            // Call the ShowDialog method to show the dialog box.
            DialogResult result = NRAopenFileDialog.ShowDialog();

            // Process input if the user clicked OK.
            if (result == DialogResult.OK)
            {
                // Open the selected file to read.
                System.IO.Stream fileStream;
                try
                {
                    fileStream = NRAopenFileDialog.OpenFile();
                }
                catch (System.IO.IOException ex)
                {
                    MessageBox.Show("Could not load excel file. Perhaps the file is open?\n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string completeFileName = "";
                completeFileName += nraManager.setFileLocation(NRAopenFileDialog.FileName);
                completeFileName += nraManager.setFileName(NRAopenFileDialog.FileName);
                // addOutput(NRAopenFileDialog.FileName);
                using (System.IO.StreamReader reader = new System.IO.StreamReader(fileStream))
                {
                    // Read the first line from the file and write it the textbox.
                    //  tbResults.Text = reader.ReadLine();
                }
                fileStream.Close();
                givenExcelFileTextBox.Text = completeFileName;
            }
        }

        private void pending_orderListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged)
            {
                return;
            }
            if (pending_orderListBox.SelectedIndex == -1)
            {
                return;
            }
            ignoreSelectedIndexChanged = true;
            acknowledge_distributorsListBox.ClearSelected();
            acknowledge_ordersListBox.DataSource = null;
            acknowledge_ordersListBox.ClearSelected();
            ignoreSelectedIndexChanged = false;

            string selectedItem = pending_orderListBox.SelectedItem.ToString();
            try
            {
                committeeNumberTextBox.Text = nraManager.pending_comitteeNumberList[nraManager.pending_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set committe text box");
            }
            try
            {
                lineItemIDTextBox.Text = nraManager.pending_lineItemIDList[nraManager.pending_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set lineItemID text box");
            }
            try
            {
                productCodeTextBox.Text = nraManager.pending_given_ProductCodeList[nraManager.pending_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set productCode text box");
            }
            try
            {
                dateRequiredTextBox.Text = nraManager.pending_dateRequiredList[nraManager.pending_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set dateRequired text box");
            }
            try
            {
                productDescriptionTextBox.Text = nraManager.pending_descriptionList[nraManager.pending_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set productDescription text box");
            }
            try
            {
                eventDateTextBox.Text = nraManager.pending_eventDateList[nraManager.pending_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set eventDate text box");
            }
            try
            {
                orderDateTextBox.Text = nraManager.pending_orderDateList[nraManager.pending_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set orderDate text box");
            }
            try
            {
                nraReferenceNumberTextBox.Text = nraManager.pending_referenceNumberList[nraManager.pending_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set  nraReferenceNumber text box");
            }
            try
            {
                skuTextBox.Text = nraManager.pending_skuList[nraManager.pending_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set sku text box");
            }
            try
            {
                orderStatusTextBox.Text = nraManager.pending_orderStatusList[nraManager.pending_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set orderStatus text box");
            }
        }

        private void acknowledge_ordersListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged)
            {
                return;
            }
            if (acknowledge_ordersListBox.SelectedIndex == -1)
            {
                return;
            }
            ignoreSelectedIndexChanged = true;
            pending_orderListBox.DataSource = null;
            pending_orderListBox.ClearSelected();
            pending_distributorListBox.ClearSelected();
            ignoreSelectedIndexChanged = false;
            string selectedItem = acknowledge_ordersListBox.SelectedItem.ToString();
            try
            {
                committeeNumberTextBox.Text = nraManager.acknowledge_comitteeNumberList[nraManager.acknowledge_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set committe text box");
            }
            try
            {
                lineItemIDTextBox.Text = nraManager.acknowledge_lineItemIDList[nraManager.acknowledge_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set lineItemID text box");
            }
            try
            {
                productCodeTextBox.Text = nraManager.acknowledge_given_ProductCodeList[nraManager.acknowledge_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set productCode text box");
            }
            try
            {
                dateRequiredTextBox.Text = nraManager.acknowledge_dateRequiredList[nraManager.acknowledge_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set dateRequired text box");
            }
            try
            {
                productDescriptionTextBox.Text = nraManager.acknowledge_descriptionList[nraManager.acknowledge_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set productDescription text box");
            }
            try
            {
                eventDateTextBox.Text = nraManager.acknowledge_eventDateList[nraManager.acknowledge_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set eventDate text box");
            }
            try
            {
                orderDateTextBox.Text = nraManager.acknowledge_orderDateList[nraManager.acknowledge_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set orderDate text box");
            }
            try
            {
                nraReferenceNumberTextBox.Text = nraManager.acknowledge_referenceNumberList[nraManager.acknowledge_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set  nraReferenceNumber text box");
            }
            try
            {
                skuTextBox.Text = nraManager.acknowledge_skuList[nraManager.acknowledge_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set sku text box");
            }
            try
            {
                orderStatusTextBox.Text = nraManager.acknowledge_orderStatusList[nraManager.acknowledge_orderIDList.IndexOf(selectedItem)];
            }
            catch (Exception ex)
            {
                addOutput("NRA Check: Could not set orderStatus text box");
            }
        }

        private void pending_distributorListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged)
            {
                return;
            }
            if (pending_distributorListBox.SelectedIndex == -1)
            {
                return;
            }
            ignoreSelectedIndexChanged = true;
            pending_orderListBox.DataSource = null;
            pending_orderListBox.ClearSelected();
            acknowledge_distributorsListBox.ClearSelected();
            acknowledge_ordersListBox.DataSource = null;
            acknowledge_ordersListBox.ClearSelected();
            ignoreSelectedIndexChanged = false;

            string selectedName = pending_distributorListBox.SelectedItem.ToString();
            List<string> orders = new List<string>();
            List<string> theseOrders = new List<string>();

            orders = nraManager.nraPendingErrorList;

            List<string> distributorsList = new List<string>();// = nraManager.distributorNameList;
            foreach (string x in nraManager.pending_given_OrdersList)
            {
                if (orders.Contains(x))
                {
                    theseOrders.Add(x);
                    //distributorsList.Add(nraManager.distributorNameList[nraManager.orderIDList.IndexOf(x)]);
                }
            }
            //  distributorsList = distributorsList.Distinct<string>().ToList<string>();
            // pending_distributorListBox.DataSource = distributorsList;
            theseOrders = theseOrders.Distinct<string>().ToList();
            pending_orderListBox.DataSource = theseOrders;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox aboutBox = new AboutBox();
            aboutBox.Show();
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void acknowledge_distributorsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged)
            {
                return;
            }
            ignoreSelectedIndexChanged = true;
            pending_distributorListBox.ClearSelected();
            pending_orderListBox.DataSource = null;
            pending_orderListBox.ClearSelected();
            acknowledge_ordersListBox.DataSource = null;
            acknowledge_ordersListBox.ClearSelected();
            ignoreSelectedIndexChanged = false;

            string selectedName = acknowledge_distributorsListBox.SelectedItem.ToString();
            List<string> orders = new List<string>();
            List<string> theseOrders = new List<string>();

            orders = nraManager.nraAcknowledgeErrorList;

            List<string> distributorsList = new List<string>();// = nraManager.distributorNameList;
            foreach (string x in nraManager.acknowledge_orderIDList)
            {
                if (orders.Contains(x))
                {
                    theseOrders.Add(x);
                    //distributorsList.Add(nraManager.distributorNameList[nraManager.orderIDList.IndexOf(x)]);
                }
            }
            //  distributorsList = distributorsList.Distinct<string>().ToList<string>();
            // pending_distributorListBox.DataSource = distributorsList;
            theseOrders = theseOrders.Distinct<string>().ToList();
            acknowledge_ordersListBox.DataSource = theseOrders;
        }

        private void shopify_CheckedCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged)
            {
                return;
            }
            if (errorListBox.SelectedIndex == -1)
            {
                MessageBox.Show("You must select at least one order in the errors list to mark.", "Cannot Mark Checked", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                addOutput("Must select an order to check as marked.");
                ignoreSelectedIndexChanged = true;
                shopify_CheckedCheckBox.Checked = false;
                ignoreSelectedIndexChanged = false;
                return;
            }
            ignoreSelectedIndexChanged = true;
            List<string> items = errorListBox.Items.Cast<string>().ToList();
            List<int> indices = new List<int>();
            for (int i = 0; i < errorListBox.Items.Count; i++)
            {
                if (!errorListBox.SelectedItems.Contains(errorListBox.Items[i]))
                {
                    continue;
                }
                string selectedItem = errorListBox.Items[i].ToString();
                if (shopify_CheckedCheckBox.Checked)
                {
                    //check if it has a space, if it does we already have the mark, dont need to do anything, otherwise add the mark
                    if (!selectedItem.Contains(" "))
                    {
                        items[i] = selectedItem + checkedMark;
                    }
                }
                else
                {
                    //check if it has a space, if it does add the mark, if not its already not marked dont need to touch it
                    if (selectedItem.Contains(checkedMark))
                    {
                        items[i] = selectedItem.Substring(0, selectedItem.IndexOf(" "));
                    }
                }
            }
            foreach (int x in errorListBox.SelectedIndices.Cast<int>().ToList())
            {
                indices.Add(x);
            }

            errorListBox.DataSource = null;
            errorListBox.DataSource = items;
            errorListBox.ClearSelected();
            foreach (int i in indices)
            {
                errorListBox.SelectedIndex = i;
            }
            errorListBox.Refresh();

            ignoreSelectedIndexChanged = false;
        }
        /*Added July 5 2016 
        private void nraFirstStartQueryButton_Click(object sender, EventArgs e)
        {
            if (nraManager.firstCheck(nraFirstDateTimePicker.Value) == NRAManager.SUCCESS)
            {
                ignoreSelectedIndexChanged = true;
                nraFirst_AccountsListBox.DataSource = nraManager.firstCheckDictionary.Keys.Cast<string>().ToList();
                nraFirst_AccountsListBox.ClearSelected();
                ignoreSelectedIndexChanged = false;
            }

        }
*/
        public void setNRAFirstCheckQuery(string queryText)
        {
            nraFirst_QueryRichTextBox.Text = queryText;
        }

        private void nraFirst_AccountsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged)
            {
                return;
            }
            if (nraFirst_AccountsListBox.SelectedItems.Count > 1)
            {
                return;
            }
            ignoreSelectedIndexChanged = true;

            nraFirst_ID_Status5_ListBox.DataSource = null;
            if(nraFirst_AccountsListBox.SelectedIndex == 0)
            {
                List<string> status5List = new List<string>();// nraFirstCheck.firstCheckDictionary[nraFirst_AccountsListBox.SelectedItem.ToString()];
                List<string> otherList = new List<string>();
                foreach(string account in nraFirst_AccountsListBox.Items)
                {
                    if(account == "All")
                    {
                        continue;
                    }
                    foreach(string orders in nraFirstCheck.firstCheckDictionary_Status5[account])
                    {
                        status5List.Add(orders);
                    }
                    foreach (string orders in nraFirstCheck.firstCheckDictionary[account])
                    {
                        otherList.Add(orders);
                    }
                }
                status5List = status5List.Distinct<string>().ToList<string>();
                nraFirst_ID_Status5_ListBox.DataSource = status5List;
                nraFirst_ID_Status5_ListBox.ClearSelected();

                otherList = otherList.Distinct<string>().ToList<string>();
                nraFirst_ID_Other_ListBox.DataSource = otherList;
                nraFirst_ID_Other_ListBox.ClearSelected();
                ignoreSelectedIndexChanged = false;
            }
            else
            {
                List<string> status5List = nraFirstCheck.firstCheckDictionary_Status5[nraFirst_AccountsListBox.SelectedItem.ToString()];
                List<string> otherList = nraFirstCheck.firstCheckDictionary[nraFirst_AccountsListBox.SelectedItem.ToString()];
               
                status5List = status5List.Distinct<string>().ToList<string>();
                nraFirst_ID_Status5_ListBox.DataSource = status5List;
                nraFirst_ID_Status5_ListBox.ClearSelected();

                otherList = otherList.Distinct<string>().ToList<string>();
                nraFirst_ID_Other_ListBox.DataSource = otherList;
                nraFirst_ID_Other_ListBox.ClearSelected();
                ignoreSelectedIndexChanged = false;
            }
            
        }


        private void ProcessOrder()
        {

        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new AboutBox().Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void shopifyTab_Click(object sender, EventArgs e)
        {

        }

        private void nraFirstCheck_StartQueryButton_Click(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged)
            {
                MessageBox.Show("Something in the background might be changing the lists, please try again.", "Query More Info Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            addOutput("==========================================");
            addOutput("NRA First Check Start Query Button Pressed");
            addOutput("==========================================");
            ignoreSelectedIndexChanged = true;
            nraFirstCheck.firstCheckDictionary = new Dictionary<string, List<string>>();
            nraFirst_AccountsListBox.DataSource = null;
            nraFirst_ID_Status5_ListBox.DataSource = null;
            nraFirst_ID_Other_ListBox.DataSource = null;
            ignoreSelectedIndexChanged = false;
            if (nraFirstCheckQueryWorker.IsBusy != true)
            {
                nraFirstCheckQueryWorker.RunWorkerAsync();
            }
            return;
            if (nraFirstCheck.queryOrders(nraFirstDateTimePicker.Value) == NRAManager.SUCCESS)
            {
                ignoreSelectedIndexChanged = true;
                // List<string> testList = new List<string>();
                // testList.Add("dc2fca43-5bc9-4376-9117-cf040c5b710f");
                //testList.Add("3502b8af-bf4d-42ea-87eb-97a4e1087a42");
                // nraFirstCheck.firstCheckDictionary.Add("1272", testList);
                nraFirst_AccountsListBox.DataSource = nraFirstCheck.firstCheckDictionary.Keys.Cast<string>().ToList();
                nraFirst_AccountsListBox.ClearSelected();
                nraManager.loadChannelChannels();
                ignoreSelectedIndexChanged = false;
            }
        }

        public ComboBox GetNRAFirstCheck_ChannelComboBox()
        {
            return nraFirstCheck_channelComboBox;
        }

        private void nraFirstCheck_channelComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged || nraFirstCheck_channelComboBox.SelectedIndex == 0)
            {
                return;
            }
            string channelValue = nraFirstCheck_channelComboBox.SelectedValue.ToString();
            int bracketStart = channelValue.IndexOf("[");
            int commaIndex = channelValue.IndexOf(",");
            channelValue = channelValue.Substring(bracketStart + 1, commaIndex - 1);
            Console.WriteLine("Channel Value: " + channelValue);
            Account_Locations entities = nraManager.loadLocationsByChannel(channelValue);
            Dictionary<string, string> items = new Dictionary<string, string>();
            items.Add("-1", "Select Locations..");
            entities.ForEach(delegate (Account_Location x)
            {
                items.Add(x.ID.ToString(), x.Name);
            });
            ignoreSelectedIndexChanged = true;
            nraFirstCheck_locationComboBox.DataSource = new BindingSource(items, null);
            ignoreSelectedIndexChanged = false;
        }

        private void nraFirstCheck_locationComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged || nraFirstCheck_channelComboBox.SelectedIndex == 0 || nraFirstCheck_locationComboBox.SelectedIndex == 0)
            {
                return;
            }
            string channelValue = nraFirstCheck_channelComboBox.SelectedValue.ToString();
            int bracketStart = channelValue.IndexOf("[");
            int commaIndex = channelValue.IndexOf(",");
            channelValue = channelValue.Substring(bracketStart + 1, commaIndex - 1);

            string locationValue = nraFirstCheck_locationComboBox.SelectedValue.ToString();
            bracketStart = locationValue.IndexOf("[");
            commaIndex = locationValue.IndexOf(",");
            locationValue = locationValue.Substring(bracketStart + 1, commaIndex - 1);

            Console.WriteLine("locationValue Value: " + locationValue);

            Account_ChannelLocations entities = nraManager.loadChannelLocation(channelValue, locationValue);

            Dictionary<string, string> items = new Dictionary<string, string>();
            items.Add("-1", "Select WorkFlow..");
            entities.ForEach(delegate (Account_ChannelLocation x)
            {
                // Console.WriteLine(x);
                x.Account_ChannelTransactions.ForEach(delegate (Account_ChannelTransaction ct)
                {
                    ct.Work_WorkFlows.ForEach(delegate (Work_WorkFlow wf)
                    {
                        items.Add(wf.ID.ToString(), wf.Name);
                    });
                });
            });
            ignoreSelectedIndexChanged = true;
            nraFirstCheck_workflowsComboBox.DataSource = new BindingSource(items, null);
            ignoreSelectedIndexChanged = false;
        }
      
        private void outputToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            showOutputLevel = outputToolStripComboBox.SelectedIndex + 1;
            //MessageBox.Show(showOutputLevel + " = " + outputToolStripComboBox.SelectedIndex);
        }

        private void shopify_checkErrorsCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            shopifyDoCheckErrors = shopify_checkErrorsCheckBox.Checked;
        }

        private void nraFirstCheck_runOrderProcessButton_Click(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged)
            {
                MessageBox.Show("Something in the background might be changing the lists, please try again.", "Query More Info Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (nraFirst_AccountsListBox.Items.Count == 0)
            {
                setNRAFirstCheckQueryStatus("Must Query First");
                return;
            }
            if (nraFirst_AccountsListBox.SelectedIndex == -1 || nraFirst_AccountsListBox.Items.Count == 1)//check if only 'All' shows or if nothing selected
            {
                setNRAFirstCheckQueryStatus("Nothing To Do");
                return;
            }
            if (nraFirstCheckOrderProcessWorker.IsBusy != true)
            {
                nraFirstCheckOrderProcessWorker.RunWorkerAsync();
            }
            return;
            List<string> ordersList = new List<string>();
            ordersList.Add("3502b8af-bf4d-42ea-87eb-97a4e1087a42");
            string workflowID = nraFirstCheck_workflowsComboBox.SelectedValue.ToString();
            int bracketStart = workflowID.IndexOf("[");
            int commaIndex = workflowID.IndexOf(",");
            workflowID = workflowID.Substring(bracketStart + 1, commaIndex - 1);
            nraManager.executeOrderShipping(workflowID, ordersList);
        }

        private void nraFirst_ID_Other_ListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                e.SuppressKeyPress = true;
                // Console.WriteLine("Key Up");
                try
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (object row in nraFirst_ID_Other_ListBox.SelectedItems)
                    {
                        sb.Append(row.ToString());
                        sb.AppendLine();
                    }
                    Console.WriteLine(sb);
                    //sb.Remove(sb.Length - 1, 1); // Just to avoid copying last empty row
                    Clipboard.SetData(System.Windows.Forms.DataFormats.Text, sb.ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void nraFirst_ID_Status5_ListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged)
            {
                return;
            }
            nraFirst_ID_Other_ListBox.ClearSelected();
        }

        private void nraFirst_ID_Other_ListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged)
            {
                return;
            }
            nraFirst_ID_Status5_ListBox.ClearSelected();
        }

       

        private void tabControl_Leave(object sender, EventArgs e)
        {

        }

       

        private void nraFirst_IDListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.C)
            {
                e.SuppressKeyPress = true;
               // Console.WriteLine("Key Up");
                try
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (object row in nraFirst_ID_Status5_ListBox.SelectedItems)
                    {
                        sb.Append(row.ToString());
                        sb.AppendLine();
                    }
                    Console.WriteLine(sb);
                    //sb.Remove(sb.Length - 1, 1); // Just to avoid copying last empty row
                    Clipboard.SetData(System.Windows.Forms.DataFormats.Text, sb.ToString());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void shopify_QueryAllForAccount_Click(object sender, EventArgs e)
        {
            if (ignoreSelectedIndexChanged)
            {
                MessageBox.Show("Something in the background might be changing the lists, please try again.", "Query More Info Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (accountListBox.SelectedIndex == -1 && possibleAccountListBox.SelectedIndex == -1)
            {
                MessageBox.Show("Please choose a single account in the regular error list to check.", "Error on Check Errors", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (possibleAccountListBox.SelectedIndex != -1)
            {
                MessageBox.Show("You cannot check possible errors, you can query for more info on individual orders. Please choose a single account in the regular error list.", "Error on Check Errors", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (accountListBox.SelectedIndex != -1)
            {
                /* if(accountListBox.SelectedIndex == 0)//cannot query all
                 {
                     MessageBox.Show("You cannot query all accounts. Please choose a single account.", "Error on Check Errors", MessageBoxButtons.OK, MessageBoxIcon.Error);
                     return;
                 }*/
                if (errorListBox.SelectedIndex != -1)//must deselect errors
                {
                    MessageBox.Show("You must only select an account number. Deselect errors by clicking the account number again.", "Error on Check Errors", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            if (accountListBox.SelectedIndex != -1)
            {
                accountNum = accountListBox.SelectedItem.ToString();
            }
            else
            {
                MessageBox.Show("You must only select an account number. Deselect errors by clicking the account number again.", "Error on Check Errors", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (errorListBox.Items.Count > 1000)
            {
                MessageBox.Show("There are too many errors to check, this will probably overload SQL.\nTry changing the day in options.", "Error on Check Errors", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            addOutput("===============================");
            addOutput("Check All Errors Button Pressed");
            addOutput("===============================");

            if (!shopifyCheckErrorWorker.IsBusy)
            {
                shopifyCheckErrorWorker.RunWorkerAsync();
            }
            
            return;
            if (accountNum.Equals("All"))//execute all accounts
            {
                List<int> acconts_ErrorsList = new List<int>();
                List<string> accountsList = new List<string>();
                acconts_ErrorsList = ShopifyManager.errors_Account_List.Keys.ToList();
                List<int> addToPossibleList = new List<int>();

                foreach (string account in accountListBox.Items)
                {
                    if (account.Equals("All"))
                    {
                        continue;
                    }
                    accountNum = account;
                    List<string> errorList = ShopifyManager.errors_Account_List[Convert.ToInt32(accountNum)].Cast<string>().ToList();
                    List<string> resultList = new List<string>();
                    resultList = shopifyManager.checkAllErrors(accountNum, errorList, deleted, orderTypeID);
                    Console.WriteLine("Number of reference numbers in nChannel: " + (resultList.Count - 1) + " of " + errorList.Count + " in " + accountNum);
                    if (resultList.Count - 1 == errorList.Count)//remove all the errors in the selected account
                    {
                        ShopifyManager.errors_Account_List.Remove(Convert.ToInt32(accountNum));
                    }
                    else
                    {
                        ShopifyManager.errors_Account_List[Convert.ToInt32(accountNum)] = errorList;
                    }
                }
                ignoreSelectedIndexChanged = true;
                errorListBox.DataSource = null;
                errorsLabel.Text = "Errors";
                updateOrderInfo(Color.DarkGray);
                acconts_ErrorsList = ShopifyManager.errors_Account_List.Keys.ToList();
                accountsList = ShopifyManager.errors_Account_List.Keys.Cast<string>().ToList();
                accountsList.Add("All");
                foreach (int x in acconts_ErrorsList)
                {
                    accountsList.Add(x + "");
                }
                accountListBox.DataSource = accountsList;
                accountListBox.Refresh();
                ignoreSelectedIndexChanged = false;
            }
            else//execute for the selected account
            {
                List<string> errorList = errorListBox.Items.Cast<string>().ToList();
                List<string> resultList = new List<string>();
                resultList = shopifyManager.checkAllErrors(accountNum, errorList, deleted, orderTypeID);
                Console.WriteLine("Number of reference numbers in nChannel: " + resultList.Count + " of " + errorList.Count + " in " + accountNum);
                if (resultList.Count - 1 == errorList.Count)//remove all the errors in the selected account
                {
                    ignoreSelectedIndexChanged = true;
                    errorListBox.DataSource = null;
                    errorListBox.Refresh();
                    ShopifyManager.errors_Account_List.Remove(Convert.ToInt32(accountNum));
                    accountListBox.DataSource = null;

                    List<int> acconts_ErrorsList = new List<int>();
                    List<string> accountsList = new List<string>();

                    acconts_ErrorsList = ShopifyManager.errors_Account_List.Keys.ToList();

                    List<int> addToPossibleList = new List<int>();

                    accountsList.Add("All");
                    foreach (int x in acconts_ErrorsList)
                    {
                        accountsList.Add(x + "");
                    }
                    accountListBox.DataSource = accountsList;
                    accountListBox.Refresh();
                    ignoreSelectedIndexChanged = false;
                }
                else
                {
                    ignoreSelectedIndexChanged = true;
                    errorList = errorList.Except(resultList).ToList();
                    errorListBox.DataSource = null;
                    ShopifyManager.errors_Account_List[Convert.ToInt32(accountNum)] = errorList;
                    ignoreSelectedIndexChanged = false;
                }
            }


            return;

            Color foreColor = Color.Black;
            //Makes the info text boxes grey to make clear they are not neccessarily the current info
            //change the text and color for each info textbox
            //must change backcolor first to change forecolor on readonly textbox
            foreColor = Color.Gray;
            updateOrderInfo(foreColor);
            foreColor = Color.Black;
            shopify_OrderInfoLabel.Visible = false;

            List<string> results = new List<string>();
            string errorNum = "";
            if (errorListBox.SelectedIndex != -1)
            {
                accountNum = accountListBox.SelectedItem.ToString();
                if (errorListBox.SelectedItem.ToString().Contains(" "))
                {
                    errorNum = errorListBox.SelectedItem.ToString().Substring(0, errorListBox.SelectedItem.ToString().IndexOf(" "));
                }
                else
                {
                    errorNum = errorListBox.SelectedItem.ToString();
                }
                queryStatusLabel.Text = "Query Status: Working on " + accountNum + "...";
                queryStatusLabel.Refresh();
                results = shopifyManager.queryMoreInfo(accountNum, errorNum, deleted, orderTypeID, true);
            }
            else if (possibleErrorListBox.SelectedIndex != -1)
            {
                accountNum = possibleAccountListBox.SelectedItem.ToString();
                errorNum = possibleErrorListBox.SelectedItem.ToString();
                queryStatusLabel.Text = "Query Status: Working on " + accountNum + "...";
                queryStatusLabel.Refresh();
                results = shopifyManager.queryMoreInfo(accountNum, errorNum, deleted, orderTypeID, true);
            }
            if (results.Count == 0 || results.Count == 1)
            {
                queryStatusLabel.Text = "Query Status: No results found.";
                queryStatusLabel.Refresh();
                Console.WriteLine("Query!!!");
                //Makes the info text boxes grey to make clear they are not neccessarily the current info
                //change the text and color for each info textbox
                //must change backcolor first to change forecolor on readonly textbox
                foreColor = Color.Gray;
                updateOrderInfo(foreColor);
                return;
            }
            //change the text and color for each info textbox
            //must change backcolor first to change forecolor on readonly textbox

            idTextBox.Text = results[1];
            referenceNumberTextBox.Text = results[2];
            DateTime dt = DateTime.Parse(results[3]);
            dateModifiedTextBox.Text = dt.ToShortTimeString() + " " + dt.ToString("M/d/y");
            dt = DateTime.Parse(results[4]);
            dateCreatedTextBox.Text = dt.ToShortTimeString() + " " + dt.ToString("M/d/y");
            accountNameTextBox.Text = results[5];
            statusTextBox.Text = results[6];
            accountNumberTextBox.Text = accountNum + "";
            if (referenceNumberTextBox.Text.Contains(errorNum + ""))//the order exists in nChannel
            {
                shopify_OrderInfoLabel.Text = "Order found in nChannel";
                foreColor = Color.DarkGreen;
                updateOrderInfo(foreColor);
                shopify_OrderInfoLabel.Visible = true;
            }
            else
            {
                shopify_OrderInfoLabel.Text = "Order info IS NOT the selected order";
                foreColor = Color.Black;
                updateOrderInfo(foreColor);
                shopify_OrderInfoLabel.Visible = true;
            }
            ignoreSelectedIndexChanged = false;
        }

        private void allTasksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            shopifyWorker.CancelAsync();
            nraFirstCheckQueryWorker.CancelAsync();
            nraFirstCheckOrderProcessWorker.CancelAsync();
            bgWorker.CancelAsync();
            nraExcelWorker.CancelAsync();
        }
    }
}