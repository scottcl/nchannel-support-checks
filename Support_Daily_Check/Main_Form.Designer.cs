﻿namespace nChannel_Daily_Program
{
    partial class Main_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Form));
            this.startButton = new System.Windows.Forms.Button();
            this.ordersErrorlabel = new System.Windows.Forms.Label();
            this.errorListBox = new System.Windows.Forms.ListBox();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.shopifyTab = new System.Windows.Forms.TabPage();
            this.shopify_QueryAllForAccount = new System.Windows.Forms.Button();
            this.shopify_OrderInfoLabel = new System.Windows.Forms.Label();
            this.shopify_CheckedCheckBox = new System.Windows.Forms.CheckBox();
            this.recurringButton = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.lastQueryRichTextBox = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.shopify_checkErrorsCheckBox = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.orderTypeIDNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.loadSettingsButton = new System.Windows.Forms.Button();
            this.openSaveFileButton = new System.Windows.Forms.Button();
            this.resetOptionsButton = new System.Windows.Forms.Button();
            this.deletedCheckBox = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.accountsQueryListBox = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.daysNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.statusTextBox = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.accountNumberTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.accountNameTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.referenceNumberTextBox = new System.Windows.Forms.TextBox();
            this.idTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dateModifiedTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dateCreatedTextBox = new System.Windows.Forms.TextBox();
            this.referenceNumberLabel = new System.Windows.Forms.Label();
            this.queryMoreInfoButton = new System.Windows.Forms.Button();
            this.errorsLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.possibleAccountListBox = new System.Windows.Forms.ListBox();
            this.possibleErrorsLabel = new System.Windows.Forms.Label();
            this.possibleErrorListBox = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.queryStatusLabel = new System.Windows.Forms.Label();
            this.elapsedTimeLabel = new System.Windows.Forms.Label();
            this.accountListBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nraFirstCheckTabPage = new System.Windows.Forms.TabPage();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.nraFirst_ID_Other_ListBox = new System.Windows.Forms.ListBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.nraFirstCheck_channelComboBox = new System.Windows.Forms.ComboBox();
            this.nraFirstCheck_locationComboBox = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.nraFirstCheck_workflowsComboBox = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.nraFirstCheck_StatusLabel = new System.Windows.Forms.Label();
            this.nraFirstCheck_runOrderProcessButton = new System.Windows.Forms.Button();
            this.nraFirst_QueryRichTextBox = new System.Windows.Forms.RichTextBox();
            this.nraFirstDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.nraFirst_ID_Status5_ListBox = new System.Windows.Forms.ListBox();
            this.nraFirst_AccountsListBox = new System.Windows.Forms.ListBox();
            this.nraFirstCheck_StartQueryButton = new System.Windows.Forms.Button();
            this.NRATabPage = new System.Windows.Forms.TabPage();
            this.nra_ProcessGroupBox = new System.Windows.Forms.GroupBox();
            this.nra_ProcessBothRadioButton = new System.Windows.Forms.RadioButton();
            this.nra_PendingOnlyRadioButton = new System.Windows.Forms.RadioButton();
            this.nra_AcknowledgeRadioButton = new System.Windows.Forms.RadioButton();
            this.nraStatusLabel = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.acknowledge_distributorsListBox = new System.Windows.Forms.ListBox();
            this.label21 = new System.Windows.Forms.Label();
            this.acknowledge_ordersListBox = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.orderStatusTextBox = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.orderDateTextBox = new System.Windows.Forms.TextBox();
            this.nraReferenceNumberTextBox = new System.Windows.Forms.TextBox();
            this.skuTextBox = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.eventDateTextBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.productDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.lineItemIDTextBox = new System.Windows.Forms.TextBox();
            this.productCodeTextBox = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.committeeNumberTextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.dateRequiredTextBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.pendingGroupBox = new System.Windows.Forms.GroupBox();
            this.distributorListLabel = new System.Windows.Forms.Label();
            this.pending_distributorListBox = new System.Windows.Forms.ListBox();
            this.label19 = new System.Windows.Forms.Label();
            this.pending_orderListBox = new System.Windows.Forms.ListBox();
            this.fileGroupBox = new System.Windows.Forms.GroupBox();
            this.selectExcelFileButton = new System.Windows.Forms.Button();
            this.givenExcelFileTextBox = new System.Windows.Forms.TextBox();
            this.givenExcelFileLabel = new System.Windows.Forms.Label();
            this.startProcessButton = new System.Windows.Forms.Button();
            this.outputTab = new System.Windows.Forms.TabPage();
            this.outputTextBox = new System.Windows.Forms.RichTextBox();
            this.NRAopenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.ShopifyToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.abortOperationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allTasksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.outputToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.shopifyTab.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderTypeIDNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.daysNumericUpDown)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.nraFirstCheckTabPage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.NRATabPage.SuspendLayout();
            this.nra_ProcessGroupBox.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.pendingGroupBox.SuspendLayout();
            this.fileGroupBox.SuspendLayout();
            this.outputTab.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(388, 507);
            this.startButton.Margin = new System.Windows.Forms.Padding(4);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(231, 68);
            this.startButton.TabIndex = 0;
            this.startButton.Text = "Start Query";
            this.ShopifyToolTip.SetToolTip(this.startButton, "Press to find all missing orders from the selected account numbers in the \'\r\nAcco" +
        "unts To Query\' list");
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // ordersErrorlabel
            // 
            this.ordersErrorlabel.AutoSize = true;
            this.ordersErrorlabel.Location = new System.Drawing.Point(812, 219);
            this.ordersErrorlabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ordersErrorlabel.Name = "ordersErrorlabel";
            this.ordersErrorlabel.Size = new System.Drawing.Size(132, 17);
            this.ordersErrorlabel.TabIndex = 2;
            this.ordersErrorlabel.Text = "Reference Number:";
            // 
            // errorListBox
            // 
            this.errorListBox.FormattingEnabled = true;
            this.errorListBox.ItemHeight = 16;
            this.errorListBox.Location = new System.Drawing.Point(816, 239);
            this.errorListBox.Margin = new System.Windows.Forms.Padding(4);
            this.errorListBox.Name = "errorListBox";
            this.errorListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.errorListBox.Size = new System.Drawing.Size(319, 340);
            this.errorListBox.TabIndex = 3;
            this.ShopifyToolTip.SetToolTip(this.errorListBox, "All the reference numbers that are missing. Any characters such as store name is " +
        "removed.");
            this.errorListBox.SelectedIndexChanged += new System.EventHandler(this.errorListBox_SelectedIndexChanged);
            this.errorListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.errorListBox_KeyDown);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.shopifyTab);
            this.tabControl.Controls.Add(this.nraFirstCheckTabPage);
            this.tabControl.Controls.Add(this.NRATabPage);
            this.tabControl.Controls.Add(this.outputTab);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 32);
            this.tabControl.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1151, 622);
            this.tabControl.TabIndex = 2;
            this.tabControl.Leave += new System.EventHandler(this.tabControl_Leave);
            // 
            // shopifyTab
            // 
            this.shopifyTab.Controls.Add(this.shopify_QueryAllForAccount);
            this.shopifyTab.Controls.Add(this.shopify_OrderInfoLabel);
            this.shopifyTab.Controls.Add(this.shopify_CheckedCheckBox);
            this.shopifyTab.Controls.Add(this.recurringButton);
            this.shopifyTab.Controls.Add(this.label11);
            this.shopifyTab.Controls.Add(this.lastQueryRichTextBox);
            this.shopifyTab.Controls.Add(this.groupBox2);
            this.shopifyTab.Controls.Add(this.groupBox1);
            this.shopifyTab.Controls.Add(this.queryMoreInfoButton);
            this.shopifyTab.Controls.Add(this.errorsLabel);
            this.shopifyTab.Controls.Add(this.label6);
            this.shopifyTab.Controls.Add(this.possibleAccountListBox);
            this.shopifyTab.Controls.Add(this.possibleErrorsLabel);
            this.shopifyTab.Controls.Add(this.possibleErrorListBox);
            this.shopifyTab.Controls.Add(this.label5);
            this.shopifyTab.Controls.Add(this.queryStatusLabel);
            this.shopifyTab.Controls.Add(this.elapsedTimeLabel);
            this.shopifyTab.Controls.Add(this.accountListBox);
            this.shopifyTab.Controls.Add(this.label1);
            this.shopifyTab.Controls.Add(this.errorListBox);
            this.shopifyTab.Controls.Add(this.startButton);
            this.shopifyTab.Controls.Add(this.ordersErrorlabel);
            this.shopifyTab.Location = new System.Drawing.Point(4, 25);
            this.shopifyTab.Margin = new System.Windows.Forms.Padding(4);
            this.shopifyTab.Name = "shopifyTab";
            this.shopifyTab.Padding = new System.Windows.Forms.Padding(4);
            this.shopifyTab.Size = new System.Drawing.Size(1143, 593);
            this.shopifyTab.TabIndex = 0;
            this.shopifyTab.Text = "Shopify";
            this.shopifyTab.UseVisualStyleBackColor = true;
            this.shopifyTab.Click += new System.EventHandler(this.shopifyTab_Click);
            // 
            // shopify_QueryAllForAccount
            // 
            this.shopify_QueryAllForAccount.Location = new System.Drawing.Point(388, 390);
            this.shopify_QueryAllForAccount.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.shopify_QueryAllForAccount.Name = "shopify_QueryAllForAccount";
            this.shopify_QueryAllForAccount.Size = new System.Drawing.Size(231, 39);
            this.shopify_QueryAllForAccount.TabIndex = 31;
            this.shopify_QueryAllForAccount.Text = "Check All Errors In Account";
            this.ShopifyToolTip.SetToolTip(this.shopify_QueryAllForAccount, "This will prompt you to select statuses for orders that are not errors. It will t" +
        "hen query for all orders in the selected account and move each order that has th" +
        "e selected status to possible errors.");
            this.shopify_QueryAllForAccount.UseVisualStyleBackColor = true;
            this.shopify_QueryAllForAccount.Click += new System.EventHandler(this.shopify_QueryAllForAccount_Click);
            // 
            // shopify_OrderInfoLabel
            // 
            this.shopify_OrderInfoLabel.AutoSize = true;
            this.shopify_OrderInfoLabel.Location = new System.Drawing.Point(512, 167);
            this.shopify_OrderInfoLabel.Name = "shopify_OrderInfoLabel";
            this.shopify_OrderInfoLabel.Size = new System.Drawing.Size(120, 17);
            this.shopify_OrderInfoLabel.TabIndex = 30;
            this.shopify_OrderInfoLabel.Text = "Order Info Status:";
            this.shopify_OrderInfoLabel.Visible = false;
            // 
            // shopify_CheckedCheckBox
            // 
            this.shopify_CheckedCheckBox.AutoSize = true;
            this.shopify_CheckedCheckBox.Location = new System.Drawing.Point(1025, 185);
            this.shopify_CheckedCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.shopify_CheckedCheckBox.Name = "shopify_CheckedCheckBox";
            this.shopify_CheckedCheckBox.Size = new System.Drawing.Size(85, 21);
            this.shopify_CheckedCheckBox.TabIndex = 29;
            this.shopify_CheckedCheckBox.Text = "Checked";
            this.ShopifyToolTip.SetToolTip(this.shopify_CheckedCheckBox, "Check this to mark the selected order. Helps keep track of orders you have alread" +
        "y looked at.");
            this.shopify_CheckedCheckBox.UseVisualStyleBackColor = true;
            this.shopify_CheckedCheckBox.CheckedChanged += new System.EventHandler(this.shopify_CheckedCheckBox_CheckedChanged);
            // 
            // recurringButton
            // 
            this.recurringButton.Location = new System.Drawing.Point(388, 348);
            this.recurringButton.Margin = new System.Windows.Forms.Padding(4);
            this.recurringButton.Name = "recurringButton";
            this.recurringButton.Size = new System.Drawing.Size(231, 34);
            this.recurringButton.TabIndex = 28;
            this.recurringButton.Text = "Add Selected To Recurring";
            this.recurringButton.UseVisualStyleBackColor = true;
            this.recurringButton.Click += new System.EventHandler(this.recurringButton_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 237);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 17);
            this.label11.TabIndex = 27;
            this.label11.Text = "Last Query:";
            // 
            // lastQueryRichTextBox
            // 
            this.lastQueryRichTextBox.Location = new System.Drawing.Point(9, 258);
            this.lastQueryRichTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lastQueryRichTextBox.Name = "lastQueryRichTextBox";
            this.lastQueryRichTextBox.Size = new System.Drawing.Size(317, 87);
            this.lastQueryRichTextBox.TabIndex = 26;
            this.lastQueryRichTextBox.Text = "";
            this.ShopifyToolTip.SetToolTip(this.lastQueryRichTextBox, "This is the text of the last query ran.");
            this.lastQueryRichTextBox.DoubleClick += new System.EventHandler(this.lastQueryRichTextBox_DoubleClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.shopify_checkErrorsCheckBox);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.orderTypeIDNumericUpDown);
            this.groupBox2.Controls.Add(this.loadSettingsButton);
            this.groupBox2.Controls.Add(this.openSaveFileButton);
            this.groupBox2.Controls.Add(this.resetOptionsButton);
            this.groupBox2.Controls.Add(this.deletedCheckBox);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.accountsQueryListBox);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.daysNumericUpDown);
            this.groupBox2.Location = new System.Drawing.Point(9, 4);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(447, 231);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Options";
            // 
            // shopify_checkErrorsCheckBox
            // 
            this.shopify_checkErrorsCheckBox.AutoSize = true;
            this.shopify_checkErrorsCheckBox.Checked = true;
            this.shopify_checkErrorsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.shopify_checkErrorsCheckBox.Location = new System.Drawing.Point(11, 102);
            this.shopify_checkErrorsCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.shopify_checkErrorsCheckBox.Name = "shopify_checkErrorsCheckBox";
            this.shopify_checkErrorsCheckBox.Size = new System.Drawing.Size(112, 21);
            this.shopify_checkErrorsCheckBox.TabIndex = 11;
            this.shopify_checkErrorsCheckBox.Text = "Check Errors";
            this.ShopifyToolTip.SetToolTip(this.shopify_checkErrorsCheckBox, resources.GetString("shopify_checkErrorsCheckBox.ToolTip"));
            this.shopify_checkErrorsCheckBox.UseVisualStyleBackColor = true;
            this.shopify_checkErrorsCheckBox.CheckedChanged += new System.EventHandler(this.shopify_checkErrorsCheckBox_CheckedChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 74);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(102, 17);
            this.label12.TabIndex = 9;
            this.label12.Text = "Order Type ID:";
            // 
            // orderTypeIDNumericUpDown
            // 
            this.orderTypeIDNumericUpDown.Location = new System.Drawing.Point(111, 71);
            this.orderTypeIDNumericUpDown.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.orderTypeIDNumericUpDown.Name = "orderTypeIDNumericUpDown";
            this.orderTypeIDNumericUpDown.Size = new System.Drawing.Size(67, 22);
            this.orderTypeIDNumericUpDown.TabIndex = 8;
            this.ShopifyToolTip.SetToolTip(this.orderTypeIDNumericUpDown, "The order type to query. Default: 1");
            this.orderTypeIDNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // loadSettingsButton
            // 
            this.loadSettingsButton.Location = new System.Drawing.Point(145, 143);
            this.loadSettingsButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.loadSettingsButton.Name = "loadSettingsButton";
            this.loadSettingsButton.Size = new System.Drawing.Size(132, 28);
            this.loadSettingsButton.TabIndex = 7;
            this.loadSettingsButton.Text = "Load Save File";
            this.ShopifyToolTip.SetToolTip(this.loadSettingsButton, "Loads the text file. Press this if changing text file.");
            this.loadSettingsButton.UseVisualStyleBackColor = true;
            this.loadSettingsButton.Click += new System.EventHandler(this.loadSettingsButton_Click);
            // 
            // openSaveFileButton
            // 
            this.openSaveFileButton.Location = new System.Drawing.Point(7, 143);
            this.openSaveFileButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.openSaveFileButton.Name = "openSaveFileButton";
            this.openSaveFileButton.Size = new System.Drawing.Size(132, 28);
            this.openSaveFileButton.TabIndex = 6;
            this.openSaveFileButton.Text = "Open Save File";
            this.ShopifyToolTip.SetToolTip(this.openSaveFileButton, "Opens text file with saved orders/settings in notepad");
            this.openSaveFileButton.UseVisualStyleBackColor = true;
            this.openSaveFileButton.Click += new System.EventHandler(this.openSaveFileButton_Click);
            // 
            // resetOptionsButton
            // 
            this.resetOptionsButton.Location = new System.Drawing.Point(7, 186);
            this.resetOptionsButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.resetOptionsButton.Name = "resetOptionsButton";
            this.resetOptionsButton.Size = new System.Drawing.Size(116, 25);
            this.resetOptionsButton.TabIndex = 5;
            this.resetOptionsButton.Text = "Reset Options";
            this.ShopifyToolTip.SetToolTip(this.resetOptionsButton, "Sets all options to default. Does not change text file");
            this.resetOptionsButton.UseVisualStyleBackColor = true;
            this.resetOptionsButton.Click += new System.EventHandler(this.resetOptionsButton_Click);
            // 
            // deletedCheckBox
            // 
            this.deletedCheckBox.AutoSize = true;
            this.deletedCheckBox.Location = new System.Drawing.Point(161, 38);
            this.deletedCheckBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.deletedCheckBox.Name = "deletedCheckBox";
            this.deletedCheckBox.Size = new System.Drawing.Size(112, 21);
            this.deletedCheckBox.TabIndex = 4;
            this.deletedCheckBox.Text = "Only Deleted";
            this.ShopifyToolTip.SetToolTip(this.deletedCheckBox, "Will only check for deleted orders. Default: False");
            this.deletedCheckBox.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(293, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(134, 17);
            this.label8.TabIndex = 3;
            this.label8.Text = "Accounts To Query:";
            // 
            // accountsQueryListBox
            // 
            this.accountsQueryListBox.FormattingEnabled = true;
            this.accountsQueryListBox.ItemHeight = 16;
            this.accountsQueryListBox.Location = new System.Drawing.Point(297, 46);
            this.accountsQueryListBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.accountsQueryListBox.Name = "accountsQueryListBox";
            this.accountsQueryListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.accountsQueryListBox.Size = new System.Drawing.Size(131, 164);
            this.accountsQueryListBox.TabIndex = 2;
            this.accountsQueryListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.accountsQueryListBox_KeyDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Days:";
            // 
            // daysNumericUpDown
            // 
            this.daysNumericUpDown.Location = new System.Drawing.Point(57, 37);
            this.daysNumericUpDown.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.daysNumericUpDown.Name = "daysNumericUpDown";
            this.daysNumericUpDown.Size = new System.Drawing.Size(67, 22);
            this.daysNumericUpDown.TabIndex = 0;
            this.ShopifyToolTip.SetToolTip(this.daysNumericUpDown, "Number of days from right now to look for. Default: 7\r\n");
            this.daysNumericUpDown.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.statusTextBox);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.accountNumberTextBox);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.accountNameTextBox);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.referenceNumberTextBox);
            this.groupBox1.Controls.Add(this.idTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dateModifiedTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dateCreatedTextBox);
            this.groupBox1.Controls.Add(this.referenceNumberLabel);
            this.groupBox1.Location = new System.Drawing.Point(515, 7);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(607, 159);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Order Info";
            this.ShopifyToolTip.SetToolTip(this.groupBox1, "This may not be accurate. It is displayed to show the most similar order to the s" +
        "elected error. \r\n\r\nIf this is grayed out, then the last query returned no result" +
        "s.");
            // 
            // statusTextBox
            // 
            this.statusTextBox.Location = new System.Drawing.Point(71, 121);
            this.statusTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.statusTextBox.Name = "statusTextBox";
            this.statusTextBox.ReadOnly = true;
            this.statusTextBox.Size = new System.Drawing.Size(39, 22);
            this.statusTextBox.TabIndex = 29;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(12, 124);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(52, 17);
            this.label26.TabIndex = 28;
            this.label26.Text = "Status:";
            // 
            // accountNumberTextBox
            // 
            this.accountNumberTextBox.Location = new System.Drawing.Point(519, 121);
            this.accountNumberTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.accountNumberTextBox.Name = "accountNumberTextBox";
            this.accountNumberTextBox.ReadOnly = true;
            this.accountNumberTextBox.Size = new System.Drawing.Size(80, 22);
            this.accountNumberTextBox.TabIndex = 27;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(451, 124);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 17);
            this.label10.TabIndex = 26;
            this.label10.Text = "Number:";
            // 
            // accountNameTextBox
            // 
            this.accountNameTextBox.Location = new System.Drawing.Point(193, 121);
            this.accountNameTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.accountNameTextBox.Name = "accountNameTextBox";
            this.accountNameTextBox.ReadOnly = true;
            this.accountNameTextBox.Size = new System.Drawing.Size(251, 22);
            this.accountNameTextBox.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(116, 124);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 17);
            this.label9.TabIndex = 24;
            this.label9.Text = "Company:";
            // 
            // referenceNumberTextBox
            // 
            this.referenceNumberTextBox.Location = new System.Drawing.Point(417, 25);
            this.referenceNumberTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.referenceNumberTextBox.Name = "referenceNumberTextBox";
            this.referenceNumberTextBox.ReadOnly = true;
            this.referenceNumberTextBox.Size = new System.Drawing.Size(183, 22);
            this.referenceNumberTextBox.TabIndex = 14;
            // 
            // idTextBox
            // 
            this.idTextBox.Location = new System.Drawing.Point(309, 66);
            this.idTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.idTextBox.Name = "idTextBox";
            this.idTextBox.ReadOnly = true;
            this.idTextBox.Size = new System.Drawing.Size(289, 22);
            this.idTextBox.TabIndex = 23;
            this.idTextBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.idTextBox_MouseDoubleClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Date Modified:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(279, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 17);
            this.label4.TabIndex = 22;
            this.label4.Text = "ID:";
            // 
            // dateModifiedTextBox
            // 
            this.dateModifiedTextBox.Location = new System.Drawing.Point(111, 21);
            this.dateModifiedTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateModifiedTextBox.Name = "dateModifiedTextBox";
            this.dateModifiedTextBox.ReadOnly = true;
            this.dateModifiedTextBox.Size = new System.Drawing.Size(151, 22);
            this.dateModifiedTextBox.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Date Created:";
            // 
            // dateCreatedTextBox
            // 
            this.dateCreatedTextBox.Location = new System.Drawing.Point(109, 66);
            this.dateCreatedTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateCreatedTextBox.Name = "dateCreatedTextBox";
            this.dateCreatedTextBox.ReadOnly = true;
            this.dateCreatedTextBox.Size = new System.Drawing.Size(151, 22);
            this.dateCreatedTextBox.TabIndex = 9;
            // 
            // referenceNumberLabel
            // 
            this.referenceNumberLabel.AutoSize = true;
            this.referenceNumberLabel.Location = new System.Drawing.Point(279, 25);
            this.referenceNumberLabel.Name = "referenceNumberLabel";
            this.referenceNumberLabel.Size = new System.Drawing.Size(132, 17);
            this.referenceNumberLabel.TabIndex = 13;
            this.referenceNumberLabel.Text = "Reference Number:";
            // 
            // queryMoreInfoButton
            // 
            this.queryMoreInfoButton.Location = new System.Drawing.Point(388, 462);
            this.queryMoreInfoButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.queryMoreInfoButton.Name = "queryMoreInfoButton";
            this.queryMoreInfoButton.Size = new System.Drawing.Size(231, 39);
            this.queryMoreInfoButton.TabIndex = 21;
            this.queryMoreInfoButton.Text = "Query Selected Error";
            this.ShopifyToolTip.SetToolTip(this.queryMoreInfoButton, "Attempts to find the \'Order Info\' as displayed in the top right. If no results ar" +
        "e found, it will decrement the reference number selected by 1, and try again. ");
            this.queryMoreInfoButton.UseVisualStyleBackColor = true;
            this.queryMoreInfoButton.Click += new System.EventHandler(this.queryMoreInfoButton_Click);
            // 
            // errorsLabel
            // 
            this.errorsLabel.AutoSize = true;
            this.errorsLabel.Location = new System.Drawing.Point(772, 190);
            this.errorsLabel.Name = "errorsLabel";
            this.errorsLabel.Size = new System.Drawing.Size(47, 17);
            this.errorsLabel.TabIndex = 20;
            this.errorsLabel.Text = "Errors";
            this.ShopifyToolTip.SetToolTip(this.errorsLabel, "Number of errors selected. If nothing selected, displays total errors.");
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 375);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 17);
            this.label6.TabIndex = 19;
            this.label6.Text = "Accounts:";
            // 
            // possibleAccountListBox
            // 
            this.possibleAccountListBox.FormattingEnabled = true;
            this.possibleAccountListBox.ItemHeight = 16;
            this.possibleAccountListBox.Location = new System.Drawing.Point(23, 399);
            this.possibleAccountListBox.Margin = new System.Windows.Forms.Padding(4);
            this.possibleAccountListBox.Name = "possibleAccountListBox";
            this.possibleAccountListBox.Size = new System.Drawing.Size(109, 180);
            this.possibleAccountListBox.TabIndex = 18;
            this.possibleAccountListBox.SelectedIndexChanged += new System.EventHandler(this.possibleAccountListBox_SelectedIndexChanged);
            this.possibleAccountListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.possibleAccountListBox_KeyDown);
            // 
            // possibleErrorsLabel
            // 
            this.possibleErrorsLabel.AutoSize = true;
            this.possibleErrorsLabel.Location = new System.Drawing.Point(84, 348);
            this.possibleErrorsLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.possibleErrorsLabel.Name = "possibleErrorsLabel";
            this.possibleErrorsLabel.Size = new System.Drawing.Size(104, 17);
            this.possibleErrorsLabel.TabIndex = 17;
            this.possibleErrorsLabel.Text = "Possible Errors";
            // 
            // possibleErrorListBox
            // 
            this.possibleErrorListBox.FormattingEnabled = true;
            this.possibleErrorListBox.ItemHeight = 16;
            this.possibleErrorListBox.Location = new System.Drawing.Point(153, 399);
            this.possibleErrorListBox.Margin = new System.Windows.Forms.Padding(4);
            this.possibleErrorListBox.Name = "possibleErrorListBox";
            this.possibleErrorListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.possibleErrorListBox.Size = new System.Drawing.Size(173, 180);
            this.possibleErrorListBox.TabIndex = 16;
            this.possibleErrorListBox.SelectedIndexChanged += new System.EventHandler(this.possibleErrorListBox_SelectedIndexChanged);
            this.possibleErrorListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.possibleErrorListBox_KeyDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(149, 378);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 17);
            this.label5.TabIndex = 15;
            this.label5.Text = "Errors:";
            // 
            // queryStatusLabel
            // 
            this.queryStatusLabel.AutoSize = true;
            this.queryStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.queryStatusLabel.Location = new System.Drawing.Point(333, 258);
            this.queryStatusLabel.Name = "queryStatusLabel";
            this.queryStatusLabel.Size = new System.Drawing.Size(125, 20);
            this.queryStatusLabel.TabIndex = 12;
            this.queryStatusLabel.Text = "Query Status:";
            // 
            // elapsedTimeLabel
            // 
            this.elapsedTimeLabel.AutoSize = true;
            this.elapsedTimeLabel.Location = new System.Drawing.Point(335, 293);
            this.elapsedTimeLabel.Name = "elapsedTimeLabel";
            this.elapsedTimeLabel.Size = new System.Drawing.Size(98, 17);
            this.elapsedTimeLabel.TabIndex = 11;
            this.elapsedTimeLabel.Text = "Elapsed Time:";
            // 
            // accountListBox
            // 
            this.accountListBox.FormattingEnabled = true;
            this.accountListBox.ItemHeight = 16;
            this.accountListBox.Location = new System.Drawing.Point(685, 239);
            this.accountListBox.Margin = new System.Windows.Forms.Padding(4);
            this.accountListBox.Name = "accountListBox";
            this.accountListBox.Size = new System.Drawing.Size(109, 340);
            this.accountListBox.TabIndex = 5;
            this.ShopifyToolTip.SetToolTip(this.accountListBox, "All the account numbers with an error. Select one to view the reference numbers t" +
        "hat are missing.");
            this.accountListBox.SelectedIndexChanged += new System.EventHandler(this.accountListBox_SelectedIndexChanged);
            this.accountListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.accountListBox_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(681, 219);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Accounts:";
            // 
            // nraFirstCheckTabPage
            // 
            this.nraFirstCheckTabPage.Controls.Add(this.label38);
            this.nraFirstCheckTabPage.Controls.Add(this.label37);
            this.nraFirstCheckTabPage.Controls.Add(this.label36);
            this.nraFirstCheckTabPage.Controls.Add(this.label35);
            this.nraFirstCheckTabPage.Controls.Add(this.label34);
            this.nraFirstCheckTabPage.Controls.Add(this.nraFirst_ID_Other_ListBox);
            this.nraFirstCheckTabPage.Controls.Add(this.label33);
            this.nraFirstCheckTabPage.Controls.Add(this.label32);
            this.nraFirstCheckTabPage.Controls.Add(this.groupBox5);
            this.nraFirstCheckTabPage.Controls.Add(this.label31);
            this.nraFirstCheckTabPage.Controls.Add(this.label30);
            this.nraFirstCheckTabPage.Controls.Add(this.nraFirstCheck_StatusLabel);
            this.nraFirstCheckTabPage.Controls.Add(this.nraFirstCheck_runOrderProcessButton);
            this.nraFirstCheckTabPage.Controls.Add(this.nraFirst_QueryRichTextBox);
            this.nraFirstCheckTabPage.Controls.Add(this.nraFirstDateTimePicker);
            this.nraFirstCheckTabPage.Controls.Add(this.nraFirst_ID_Status5_ListBox);
            this.nraFirstCheckTabPage.Controls.Add(this.nraFirst_AccountsListBox);
            this.nraFirstCheckTabPage.Controls.Add(this.nraFirstCheck_StartQueryButton);
            this.nraFirstCheckTabPage.Location = new System.Drawing.Point(4, 25);
            this.nraFirstCheckTabPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.nraFirstCheckTabPage.Name = "nraFirstCheckTabPage";
            this.nraFirstCheckTabPage.Size = new System.Drawing.Size(1143, 593);
            this.nraFirstCheckTabPage.TabIndex = 4;
            this.nraFirstCheckTabPage.Text = "NRA First Check";
            this.nraFirstCheckTabPage.UseVisualStyleBackColor = true;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(663, 224);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(452, 17);
            this.label38.TabIndex = 23;
            this.label38.Text = "Click \"Run Workflows\" to execute order shipping then order processing";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(663, 194);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(413, 17);
            this.label37.TabIndex = 22;
            this.label37.Text = "Select an account to view orders or select \'All\' to view everything";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(663, 165);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(356, 17);
            this.label36.TabIndex = 21;
            this.label36.Text = "Click \"Start Query\" button and wait for Status: Complete";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(663, 135);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(48, 17);
            this.label35.TabIndex = 20;
            this.label35.Text = "Steps:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(404, 114);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(150, 17);
            this.label34.TabIndex = 19;
            this.label34.Text = "Other Order Entry IDs:";
            // 
            // nraFirst_ID_Other_ListBox
            // 
            this.nraFirst_ID_Other_ListBox.FormattingEnabled = true;
            this.nraFirst_ID_Other_ListBox.ItemHeight = 16;
            this.nraFirst_ID_Other_ListBox.Location = new System.Drawing.Point(407, 135);
            this.nraFirst_ID_Other_ListBox.Margin = new System.Windows.Forms.Padding(4);
            this.nraFirst_ID_Other_ListBox.Name = "nraFirst_ID_Other_ListBox";
            this.nraFirst_ID_Other_ListBox.Size = new System.Drawing.Size(223, 340);
            this.nraFirst_ID_Other_ListBox.TabIndex = 18;
            this.nraFirst_ID_Other_ListBox.SelectedIndexChanged += new System.EventHandler(this.nraFirst_ID_Other_ListBox_SelectedIndexChanged);
            this.nraFirst_ID_Other_ListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.nraFirst_ID_Other_ListBox_KeyDown);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(669, 305);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(137, 17);
            this.label33.TabIndex = 17;
            this.label33.Text = "Date to Query From:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(669, 338);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(51, 17);
            this.label32.TabIndex = 16;
            this.label32.Text = "Query:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Controls.Add(this.nraFirstCheck_channelComboBox);
            this.groupBox5.Controls.Add(this.nraFirstCheck_locationComboBox);
            this.groupBox5.Controls.Add(this.label28);
            this.groupBox5.Controls.Add(this.nraFirstCheck_workflowsComboBox);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Location = new System.Drawing.Point(29, 12);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(933, 82);
            this.groupBox5.TabIndex = 15;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Workflow Selection";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(17, 25);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(71, 17);
            this.label27.TabIndex = 6;
            this.label27.Text = "Channels:";
            // 
            // nraFirstCheck_channelComboBox
            // 
            this.nraFirstCheck_channelComboBox.FormattingEnabled = true;
            this.nraFirstCheck_channelComboBox.Location = new System.Drawing.Point(17, 47);
            this.nraFirstCheck_channelComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.nraFirstCheck_channelComboBox.Name = "nraFirstCheck_channelComboBox";
            this.nraFirstCheck_channelComboBox.Size = new System.Drawing.Size(277, 24);
            this.nraFirstCheck_channelComboBox.TabIndex = 5;
            this.nraFirstCheck_channelComboBox.SelectedIndexChanged += new System.EventHandler(this.nraFirstCheck_channelComboBox_SelectedIndexChanged);
            // 
            // nraFirstCheck_locationComboBox
            // 
            this.nraFirstCheck_locationComboBox.FormattingEnabled = true;
            this.nraFirstCheck_locationComboBox.Location = new System.Drawing.Point(325, 47);
            this.nraFirstCheck_locationComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.nraFirstCheck_locationComboBox.Name = "nraFirstCheck_locationComboBox";
            this.nraFirstCheck_locationComboBox.Size = new System.Drawing.Size(277, 24);
            this.nraFirstCheck_locationComboBox.TabIndex = 7;
            this.nraFirstCheck_locationComboBox.SelectedIndexChanged += new System.EventHandler(this.nraFirstCheck_locationComboBox_SelectedIndexChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(323, 28);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(73, 17);
            this.label28.TabIndex = 8;
            this.label28.Text = "Locations:";
            // 
            // nraFirstCheck_workflowsComboBox
            // 
            this.nraFirstCheck_workflowsComboBox.FormattingEnabled = true;
            this.nraFirstCheck_workflowsComboBox.Location = new System.Drawing.Point(637, 47);
            this.nraFirstCheck_workflowsComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.nraFirstCheck_workflowsComboBox.Name = "nraFirstCheck_workflowsComboBox";
            this.nraFirstCheck_workflowsComboBox.Size = new System.Drawing.Size(277, 24);
            this.nraFirstCheck_workflowsComboBox.TabIndex = 9;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(635, 28);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(76, 17);
            this.label29.TabIndex = 10;
            this.label29.Text = "Workflows:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(162, 114);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(194, 17);
            this.label31.TabIndex = 14;
            this.label31.Text = "Order Entry IDs with Status 5:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(26, 114);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(66, 17);
            this.label30.TabIndex = 13;
            this.label30.Text = "Accounts";
            // 
            // nraFirstCheck_StatusLabel
            // 
            this.nraFirstCheck_StatusLabel.AutoSize = true;
            this.nraFirstCheck_StatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nraFirstCheck_StatusLabel.Location = new System.Drawing.Point(453, 502);
            this.nraFirstCheck_StatusLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.nraFirstCheck_StatusLabel.Name = "nraFirstCheck_StatusLabel";
            this.nraFirstCheck_StatusLabel.Size = new System.Drawing.Size(66, 24);
            this.nraFirstCheck_StatusLabel.TabIndex = 12;
            this.nraFirstCheck_StatusLabel.Text = "Status";
            // 
            // nraFirstCheck_runOrderProcessButton
            // 
            this.nraFirstCheck_runOrderProcessButton.Location = new System.Drawing.Point(639, 540);
            this.nraFirstCheck_runOrderProcessButton.Margin = new System.Windows.Forms.Padding(4);
            this.nraFirstCheck_runOrderProcessButton.Name = "nraFirstCheck_runOrderProcessButton";
            this.nraFirstCheck_runOrderProcessButton.Size = new System.Drawing.Size(175, 44);
            this.nraFirstCheck_runOrderProcessButton.TabIndex = 11;
            this.nraFirstCheck_runOrderProcessButton.Text = "Run Workflows";
            this.nraFirstCheck_runOrderProcessButton.UseVisualStyleBackColor = true;
            this.nraFirstCheck_runOrderProcessButton.Click += new System.EventHandler(this.nraFirstCheck_runOrderProcessButton_Click);
            // 
            // nraFirst_QueryRichTextBox
            // 
            this.nraFirst_QueryRichTextBox.Location = new System.Drawing.Point(672, 359);
            this.nraFirst_QueryRichTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.nraFirst_QueryRichTextBox.Name = "nraFirst_QueryRichTextBox";
            this.nraFirst_QueryRichTextBox.Size = new System.Drawing.Size(444, 126);
            this.nraFirst_QueryRichTextBox.TabIndex = 4;
            this.nraFirst_QueryRichTextBox.Text = "";
            // 
            // nraFirstDateTimePicker
            // 
            this.nraFirstDateTimePicker.Location = new System.Drawing.Point(813, 302);
            this.nraFirstDateTimePicker.Margin = new System.Windows.Forms.Padding(4);
            this.nraFirstDateTimePicker.Name = "nraFirstDateTimePicker";
            this.nraFirstDateTimePicker.Size = new System.Drawing.Size(265, 22);
            this.nraFirstDateTimePicker.TabIndex = 3;
            // 
            // nraFirst_ID_Status5_ListBox
            // 
            this.nraFirst_ID_Status5_ListBox.FormattingEnabled = true;
            this.nraFirst_ID_Status5_ListBox.ItemHeight = 16;
            this.nraFirst_ID_Status5_ListBox.Location = new System.Drawing.Point(165, 135);
            this.nraFirst_ID_Status5_ListBox.Margin = new System.Windows.Forms.Padding(4);
            this.nraFirst_ID_Status5_ListBox.Name = "nraFirst_ID_Status5_ListBox";
            this.nraFirst_ID_Status5_ListBox.Size = new System.Drawing.Size(223, 340);
            this.nraFirst_ID_Status5_ListBox.TabIndex = 2;
            this.nraFirst_ID_Status5_ListBox.SelectedIndexChanged += new System.EventHandler(this.nraFirst_ID_Status5_ListBox_SelectedIndexChanged);
            this.nraFirst_ID_Status5_ListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.nraFirst_IDListBox_KeyDown);
            // 
            // nraFirst_AccountsListBox
            // 
            this.nraFirst_AccountsListBox.FormattingEnabled = true;
            this.nraFirst_AccountsListBox.ItemHeight = 16;
            this.nraFirst_AccountsListBox.Location = new System.Drawing.Point(29, 135);
            this.nraFirst_AccountsListBox.Margin = new System.Windows.Forms.Padding(4);
            this.nraFirst_AccountsListBox.Name = "nraFirst_AccountsListBox";
            this.nraFirst_AccountsListBox.Size = new System.Drawing.Size(111, 340);
            this.nraFirst_AccountsListBox.TabIndex = 1;
            this.nraFirst_AccountsListBox.SelectedIndexChanged += new System.EventHandler(this.nraFirst_AccountsListBox_SelectedIndexChanged);
            // 
            // nraFirstCheck_StartQueryButton
            // 
            this.nraFirstCheck_StartQueryButton.Location = new System.Drawing.Point(453, 540);
            this.nraFirstCheck_StartQueryButton.Margin = new System.Windows.Forms.Padding(4);
            this.nraFirstCheck_StartQueryButton.Name = "nraFirstCheck_StartQueryButton";
            this.nraFirstCheck_StartQueryButton.Size = new System.Drawing.Size(177, 44);
            this.nraFirstCheck_StartQueryButton.TabIndex = 0;
            this.nraFirstCheck_StartQueryButton.Text = "Start Query";
            this.nraFirstCheck_StartQueryButton.UseVisualStyleBackColor = true;
            this.nraFirstCheck_StartQueryButton.Click += new System.EventHandler(this.nraFirstCheck_StartQueryButton_Click);
            // 
            // NRATabPage
            // 
            this.NRATabPage.Controls.Add(this.nra_ProcessGroupBox);
            this.NRATabPage.Controls.Add(this.nraStatusLabel);
            this.NRATabPage.Controls.Add(this.groupBox4);
            this.NRATabPage.Controls.Add(this.groupBox3);
            this.NRATabPage.Controls.Add(this.pendingGroupBox);
            this.NRATabPage.Controls.Add(this.fileGroupBox);
            this.NRATabPage.Controls.Add(this.startProcessButton);
            this.NRATabPage.Location = new System.Drawing.Point(4, 25);
            this.NRATabPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NRATabPage.Name = "NRATabPage";
            this.NRATabPage.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NRATabPage.Size = new System.Drawing.Size(1143, 593);
            this.NRATabPage.TabIndex = 3;
            this.NRATabPage.Text = "NRA";
            this.NRATabPage.UseVisualStyleBackColor = true;
            // 
            // nra_ProcessGroupBox
            // 
            this.nra_ProcessGroupBox.Controls.Add(this.nra_ProcessBothRadioButton);
            this.nra_ProcessGroupBox.Controls.Add(this.nra_PendingOnlyRadioButton);
            this.nra_ProcessGroupBox.Controls.Add(this.nra_AcknowledgeRadioButton);
            this.nra_ProcessGroupBox.Location = new System.Drawing.Point(817, 516);
            this.nra_ProcessGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.nra_ProcessGroupBox.Name = "nra_ProcessGroupBox";
            this.nra_ProcessGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.nra_ProcessGroupBox.Size = new System.Drawing.Size(317, 73);
            this.nra_ProcessGroupBox.TabIndex = 31;
            this.nra_ProcessGroupBox.TabStop = false;
            this.nra_ProcessGroupBox.Text = "Options";
            // 
            // nra_ProcessBothRadioButton
            // 
            this.nra_ProcessBothRadioButton.AutoSize = true;
            this.nra_ProcessBothRadioButton.Checked = true;
            this.nra_ProcessBothRadioButton.Location = new System.Drawing.Point(103, 21);
            this.nra_ProcessBothRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.nra_ProcessBothRadioButton.Name = "nra_ProcessBothRadioButton";
            this.nra_ProcessBothRadioButton.Size = new System.Drawing.Size(113, 21);
            this.nra_ProcessBothRadioButton.TabIndex = 30;
            this.nra_ProcessBothRadioButton.TabStop = true;
            this.nra_ProcessBothRadioButton.Text = "Process Both";
            this.nra_ProcessBothRadioButton.UseVisualStyleBackColor = true;
            // 
            // nra_PendingOnlyRadioButton
            // 
            this.nra_PendingOnlyRadioButton.AutoSize = true;
            this.nra_PendingOnlyRadioButton.Location = new System.Drawing.Point(32, 46);
            this.nra_PendingOnlyRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.nra_PendingOnlyRadioButton.Name = "nra_PendingOnlyRadioButton";
            this.nra_PendingOnlyRadioButton.Size = new System.Drawing.Size(114, 21);
            this.nra_PendingOnlyRadioButton.TabIndex = 28;
            this.nra_PendingOnlyRadioButton.Text = "Pending Only";
            this.nra_PendingOnlyRadioButton.UseVisualStyleBackColor = true;
            // 
            // nra_AcknowledgeRadioButton
            // 
            this.nra_AcknowledgeRadioButton.AutoSize = true;
            this.nra_AcknowledgeRadioButton.Location = new System.Drawing.Point(157, 46);
            this.nra_AcknowledgeRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.nra_AcknowledgeRadioButton.Name = "nra_AcknowledgeRadioButton";
            this.nra_AcknowledgeRadioButton.Size = new System.Drawing.Size(145, 21);
            this.nra_AcknowledgeRadioButton.TabIndex = 29;
            this.nra_AcknowledgeRadioButton.Text = "Acknowledge Only";
            this.nra_AcknowledgeRadioButton.UseVisualStyleBackColor = true;
            // 
            // nraStatusLabel
            // 
            this.nraStatusLabel.AutoSize = true;
            this.nraStatusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nraStatusLabel.Location = new System.Drawing.Point(449, 494);
            this.nraStatusLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.nraStatusLabel.Name = "nraStatusLabel";
            this.nraStatusLabel.Size = new System.Drawing.Size(69, 20);
            this.nraStatusLabel.TabIndex = 27;
            this.nraStatusLabel.Text = "Status:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.acknowledge_distributorsListBox);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.acknowledge_ordersListBox);
            this.groupBox4.Location = new System.Drawing.Point(588, 177);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(547, 311);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Acknowledge";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(4, 25);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(84, 17);
            this.label20.TabIndex = 3;
            this.label20.Text = "Distributors:";
            // 
            // acknowledge_distributorsListBox
            // 
            this.acknowledge_distributorsListBox.FormattingEnabled = true;
            this.acknowledge_distributorsListBox.ItemHeight = 16;
            this.acknowledge_distributorsListBox.Location = new System.Drawing.Point(8, 44);
            this.acknowledge_distributorsListBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.acknowledge_distributorsListBox.Name = "acknowledge_distributorsListBox";
            this.acknowledge_distributorsListBox.Size = new System.Drawing.Size(313, 260);
            this.acknowledge_distributorsListBox.TabIndex = 2;
            this.acknowledge_distributorsListBox.SelectedIndexChanged += new System.EventHandler(this.acknowledge_distributorsListBox_SelectedIndexChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(333, 23);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 17);
            this.label21.TabIndex = 1;
            this.label21.Text = "Orders:";
            // 
            // acknowledge_ordersListBox
            // 
            this.acknowledge_ordersListBox.FormattingEnabled = true;
            this.acknowledge_ordersListBox.ItemHeight = 16;
            this.acknowledge_ordersListBox.Location = new System.Drawing.Point(336, 44);
            this.acknowledge_ordersListBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.acknowledge_ordersListBox.Name = "acknowledge_ordersListBox";
            this.acknowledge_ordersListBox.Size = new System.Drawing.Size(204, 260);
            this.acknowledge_ordersListBox.TabIndex = 0;
            this.acknowledge_ordersListBox.SelectedIndexChanged += new System.EventHandler(this.acknowledge_ordersListBox_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.orderStatusTextBox);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.orderDateTextBox);
            this.groupBox3.Controls.Add(this.nraReferenceNumberTextBox);
            this.groupBox3.Controls.Add(this.skuTextBox);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.eventDateTextBox);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.productDescriptionTextBox);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.lineItemIDTextBox);
            this.groupBox3.Controls.Add(this.productCodeTextBox);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.committeeNumberTextBox);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.dateRequiredTextBox);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Location = new System.Drawing.Point(9, 12);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(1125, 159);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Order Info";
            // 
            // orderStatusTextBox
            // 
            this.orderStatusTextBox.Location = new System.Drawing.Point(548, 123);
            this.orderStatusTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.orderStatusTextBox.Name = "orderStatusTextBox";
            this.orderStatusTextBox.ReadOnly = true;
            this.orderStatusTextBox.Size = new System.Drawing.Size(163, 22);
            this.orderStatusTextBox.TabIndex = 35;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(491, 126);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(52, 17);
            this.label25.TabIndex = 34;
            this.label25.Text = "Status:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(781, 126);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(83, 17);
            this.label24.TabIndex = 33;
            this.label24.Text = "Order Date:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(744, 79);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(40, 17);
            this.label22.TabIndex = 32;
            this.label22.Text = "SKU:";
            // 
            // orderDateTextBox
            // 
            this.orderDateTextBox.Location = new System.Drawing.Point(871, 123);
            this.orderDateTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.orderDateTextBox.Name = "orderDateTextBox";
            this.orderDateTextBox.ReadOnly = true;
            this.orderDateTextBox.Size = new System.Drawing.Size(235, 22);
            this.orderDateTextBox.TabIndex = 31;
            // 
            // nraReferenceNumberTextBox
            // 
            this.nraReferenceNumberTextBox.Location = new System.Drawing.Point(717, 28);
            this.nraReferenceNumberTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.nraReferenceNumberTextBox.Name = "nraReferenceNumberTextBox";
            this.nraReferenceNumberTextBox.ReadOnly = true;
            this.nraReferenceNumberTextBox.Size = new System.Drawing.Size(183, 22);
            this.nraReferenceNumberTextBox.TabIndex = 29;
            // 
            // skuTextBox
            // 
            this.skuTextBox.Location = new System.Drawing.Point(804, 76);
            this.skuTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.skuTextBox.Name = "skuTextBox";
            this.skuTextBox.ReadOnly = true;
            this.skuTextBox.Size = new System.Drawing.Size(289, 22);
            this.skuTextBox.TabIndex = 30;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(579, 31);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(132, 17);
            this.label23.TabIndex = 28;
            this.label23.Text = "Reference Number:";
            // 
            // eventDateTextBox
            // 
            this.eventDateTextBox.Location = new System.Drawing.Point(991, 28);
            this.eventDateTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.eventDateTextBox.Name = "eventDateTextBox";
            this.eventDateTextBox.ReadOnly = true;
            this.eventDateTextBox.Size = new System.Drawing.Size(112, 22);
            this.eventDateTextBox.TabIndex = 27;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(907, 31);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 17);
            this.label13.TabIndex = 26;
            this.label13.Text = "Event Date:";
            // 
            // productDescriptionTextBox
            // 
            this.productDescriptionTextBox.Location = new System.Drawing.Point(147, 123);
            this.productDescriptionTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.productDescriptionTextBox.Name = "productDescriptionTextBox";
            this.productDescriptionTextBox.ReadOnly = true;
            this.productDescriptionTextBox.Size = new System.Drawing.Size(331, 22);
            this.productDescriptionTextBox.TabIndex = 25;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(56, 126);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 17);
            this.label14.TabIndex = 24;
            this.label14.Text = "Description:";
            // 
            // lineItemIDTextBox
            // 
            this.lineItemIDTextBox.Location = new System.Drawing.Point(389, 28);
            this.lineItemIDTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lineItemIDTextBox.Name = "lineItemIDTextBox";
            this.lineItemIDTextBox.ReadOnly = true;
            this.lineItemIDTextBox.Size = new System.Drawing.Size(183, 22);
            this.lineItemIDTextBox.TabIndex = 14;
            // 
            // productCodeTextBox
            // 
            this.productCodeTextBox.Location = new System.Drawing.Point(435, 76);
            this.productCodeTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.productCodeTextBox.Name = "productCodeTextBox";
            this.productCodeTextBox.ReadOnly = true;
            this.productCodeTextBox.Size = new System.Drawing.Size(289, 22);
            this.productCodeTextBox.TabIndex = 23;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 31);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(132, 17);
            this.label15.TabIndex = 6;
            this.label15.Text = "Committee Number:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(317, 79);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 17);
            this.label16.TabIndex = 22;
            this.label16.Text = "Product Code:";
            // 
            // committeeNumberTextBox
            // 
            this.committeeNumberTextBox.Location = new System.Drawing.Point(143, 28);
            this.committeeNumberTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.committeeNumberTextBox.Name = "committeeNumberTextBox";
            this.committeeNumberTextBox.ReadOnly = true;
            this.committeeNumberTextBox.Size = new System.Drawing.Size(151, 22);
            this.committeeNumberTextBox.TabIndex = 7;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(21, 79);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(104, 17);
            this.label17.TabIndex = 8;
            this.label17.Text = "Date Required:";
            // 
            // dateRequiredTextBox
            // 
            this.dateRequiredTextBox.Location = new System.Drawing.Point(147, 76);
            this.dateRequiredTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateRequiredTextBox.Name = "dateRequiredTextBox";
            this.dateRequiredTextBox.ReadOnly = true;
            this.dateRequiredTextBox.Size = new System.Drawing.Size(151, 22);
            this.dateRequiredTextBox.TabIndex = 9;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(300, 31);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(86, 17);
            this.label18.TabIndex = 13;
            this.label18.Text = "Line Item ID:";
            // 
            // pendingGroupBox
            // 
            this.pendingGroupBox.Controls.Add(this.distributorListLabel);
            this.pendingGroupBox.Controls.Add(this.pending_distributorListBox);
            this.pendingGroupBox.Controls.Add(this.label19);
            this.pendingGroupBox.Controls.Add(this.pending_orderListBox);
            this.pendingGroupBox.Location = new System.Drawing.Point(8, 177);
            this.pendingGroupBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pendingGroupBox.Name = "pendingGroupBox";
            this.pendingGroupBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pendingGroupBox.Size = new System.Drawing.Size(547, 311);
            this.pendingGroupBox.TabIndex = 2;
            this.pendingGroupBox.TabStop = false;
            this.pendingGroupBox.Text = "Pending";
            // 
            // distributorListLabel
            // 
            this.distributorListLabel.AutoSize = true;
            this.distributorListLabel.Location = new System.Drawing.Point(9, 23);
            this.distributorListLabel.Name = "distributorListLabel";
            this.distributorListLabel.Size = new System.Drawing.Size(84, 17);
            this.distributorListLabel.TabIndex = 3;
            this.distributorListLabel.Text = "Distributors:";
            // 
            // pending_distributorListBox
            // 
            this.pending_distributorListBox.FormattingEnabled = true;
            this.pending_distributorListBox.ItemHeight = 16;
            this.pending_distributorListBox.Location = new System.Drawing.Point(5, 44);
            this.pending_distributorListBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pending_distributorListBox.Name = "pending_distributorListBox";
            this.pending_distributorListBox.Size = new System.Drawing.Size(316, 260);
            this.pending_distributorListBox.TabIndex = 2;
            this.pending_distributorListBox.SelectedIndexChanged += new System.EventHandler(this.pending_distributorListBox_SelectedIndexChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(333, 23);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 17);
            this.label19.TabIndex = 1;
            this.label19.Text = "Orders:";
            // 
            // pending_orderListBox
            // 
            this.pending_orderListBox.FormattingEnabled = true;
            this.pending_orderListBox.ItemHeight = 16;
            this.pending_orderListBox.Location = new System.Drawing.Point(336, 44);
            this.pending_orderListBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pending_orderListBox.Name = "pending_orderListBox";
            this.pending_orderListBox.Size = new System.Drawing.Size(204, 260);
            this.pending_orderListBox.TabIndex = 0;
            this.pending_orderListBox.SelectedIndexChanged += new System.EventHandler(this.pending_orderListBox_SelectedIndexChanged);
            // 
            // fileGroupBox
            // 
            this.fileGroupBox.Controls.Add(this.selectExcelFileButton);
            this.fileGroupBox.Controls.Add(this.givenExcelFileTextBox);
            this.fileGroupBox.Controls.Add(this.givenExcelFileLabel);
            this.fileGroupBox.Location = new System.Drawing.Point(8, 494);
            this.fileGroupBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fileGroupBox.Name = "fileGroupBox";
            this.fileGroupBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.fileGroupBox.Size = new System.Drawing.Size(433, 86);
            this.fileGroupBox.TabIndex = 1;
            this.fileGroupBox.TabStop = false;
            this.fileGroupBox.Text = "Files";
            // 
            // selectExcelFileButton
            // 
            this.selectExcelFileButton.Location = new System.Drawing.Point(11, 50);
            this.selectExcelFileButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.selectExcelFileButton.Name = "selectExcelFileButton";
            this.selectExcelFileButton.Size = new System.Drawing.Size(179, 23);
            this.selectExcelFileButton.TabIndex = 2;
            this.selectExcelFileButton.Text = "Select File";
            this.selectExcelFileButton.UseVisualStyleBackColor = true;
            this.selectExcelFileButton.Click += new System.EventHandler(this.selectExcelFileButton_Click);
            // 
            // givenExcelFileTextBox
            // 
            this.givenExcelFileTextBox.Location = new System.Drawing.Point(89, 22);
            this.givenExcelFileTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.givenExcelFileTextBox.Name = "givenExcelFileTextBox";
            this.givenExcelFileTextBox.ReadOnly = true;
            this.givenExcelFileTextBox.Size = new System.Drawing.Size(337, 22);
            this.givenExcelFileTextBox.TabIndex = 1;
            // 
            // givenExcelFileLabel
            // 
            this.givenExcelFileLabel.AutoSize = true;
            this.givenExcelFileLabel.Location = new System.Drawing.Point(7, 22);
            this.givenExcelFileLabel.Name = "givenExcelFileLabel";
            this.givenExcelFileLabel.Size = new System.Drawing.Size(75, 17);
            this.givenExcelFileLabel.TabIndex = 0;
            this.givenExcelFileLabel.Text = "Given File:";
            // 
            // startProcessButton
            // 
            this.startProcessButton.Location = new System.Drawing.Point(447, 537);
            this.startProcessButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.startProcessButton.Name = "startProcessButton";
            this.startProcessButton.Size = new System.Drawing.Size(361, 44);
            this.startProcessButton.TabIndex = 0;
            this.startProcessButton.Text = "Start Processing";
            this.startProcessButton.UseVisualStyleBackColor = true;
            this.startProcessButton.Click += new System.EventHandler(this.startProcessButton_Click);
            // 
            // outputTab
            // 
            this.outputTab.Controls.Add(this.outputTextBox);
            this.outputTab.Location = new System.Drawing.Point(4, 25);
            this.outputTab.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.outputTab.Name = "outputTab";
            this.outputTab.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.outputTab.Size = new System.Drawing.Size(1143, 593);
            this.outputTab.TabIndex = 2;
            this.outputTab.Text = "Output";
            this.outputTab.UseVisualStyleBackColor = true;
            // 
            // outputTextBox
            // 
            this.outputTextBox.Location = new System.Drawing.Point(9, 7);
            this.outputTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.outputTextBox.Name = "outputTextBox";
            this.outputTextBox.Size = new System.Drawing.Size(1127, 573);
            this.outputTextBox.TabIndex = 0;
            this.outputTextBox.Text = "";
            // 
            // ShopifyToolTip
            // 
            this.ShopifyToolTip.AutoPopDelay = 10000;
            this.ShopifyToolTip.InitialDelay = 500;
            this.ShopifyToolTip.ReshowDelay = 100;
            this.ShopifyToolTip.ToolTipTitle = "Shopify Help";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem1,
            this.aboutToolStripMenuItem1,
            this.outputToolStripComboBox});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1151, 32);
            this.menuStrip1.TabIndex = 3;
            // 
            // fileToolStripMenuItem1
            // 
            this.fileToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abortOperationToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem1.Name = "fileToolStripMenuItem1";
            this.fileToolStripMenuItem1.Size = new System.Drawing.Size(44, 28);
            this.fileToolStripMenuItem1.Text = "File";
            // 
            // abortOperationToolStripMenuItem
            // 
            this.abortOperationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.allTasksToolStripMenuItem});
            this.abortOperationToolStripMenuItem.Name = "abortOperationToolStripMenuItem";
            this.abortOperationToolStripMenuItem.Size = new System.Drawing.Size(193, 26);
            this.abortOperationToolStripMenuItem.Text = "Abort Operation";
            // 
            // allTasksToolStripMenuItem
            // 
            this.allTasksToolStripMenuItem.Name = "allTasksToolStripMenuItem";
            this.allTasksToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this.allTasksToolStripMenuItem.Size = new System.Drawing.Size(204, 26);
            this.allTasksToolStripMenuItem.Text = "All Tasks";
            this.allTasksToolStripMenuItem.Click += new System.EventHandler(this.allTasksToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(193, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem1
            // 
            this.aboutToolStripMenuItem1.Name = "aboutToolStripMenuItem1";
            this.aboutToolStripMenuItem1.Size = new System.Drawing.Size(62, 28);
            this.aboutToolStripMenuItem1.Text = "About";
            this.aboutToolStripMenuItem1.Click += new System.EventHandler(this.aboutToolStripMenuItem1_Click);
            // 
            // outputToolStripComboBox
            // 
            this.outputToolStripComboBox.Items.AddRange(new object[] {
            "Show All Output",
            "Minimum Output",
            "Hide All Output"});
            this.outputToolStripComboBox.Name = "outputToolStripComboBox";
            this.outputToolStripComboBox.Size = new System.Drawing.Size(160, 28);
            this.outputToolStripComboBox.ToolTipText = "This may help efficiency and let the program go a little faster.\r\nThis is also he" +
    "lpful for decluttering the output tab.\r\n";
            this.outputToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.outputToolStripComboBox_SelectedIndexChanged);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openFileToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openFileToolStripMenuItem
            // 
            this.openFileToolStripMenuItem.Name = "openFileToolStripMenuItem";
            this.openFileToolStripMenuItem.Size = new System.Drawing.Size(147, 26);
            this.openFileToolStripMenuItem.Text = "Open File";
            this.openFileToolStripMenuItem.Click += new System.EventHandler(this.openFileToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(62, 24);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // Main_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1151, 654);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "Main_Form";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Daily Support Checks";
            this.Activated += new System.EventHandler(this.Main_Form_Activated);
            this.Deactivate += new System.EventHandler(this.Main_Form_Deactivate);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_Form_FormClosed);
            this.Load += new System.EventHandler(this.Main_Form_Load);
            this.Leave += new System.EventHandler(this.Main_Form_Leave);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Main_Form_MouseMove);
            this.tabControl.ResumeLayout(false);
            this.shopifyTab.ResumeLayout(false);
            this.shopifyTab.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderTypeIDNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.daysNumericUpDown)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.nraFirstCheckTabPage.ResumeLayout(false);
            this.nraFirstCheckTabPage.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.NRATabPage.ResumeLayout(false);
            this.NRATabPage.PerformLayout();
            this.nra_ProcessGroupBox.ResumeLayout(false);
            this.nra_ProcessGroupBox.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.pendingGroupBox.ResumeLayout(false);
            this.pendingGroupBox.PerformLayout();
            this.fileGroupBox.ResumeLayout(false);
            this.fileGroupBox.PerformLayout();
            this.outputTab.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Label ordersErrorlabel;
        private System.Windows.Forms.ListBox errorListBox;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage shopifyTab;
        private System.Windows.Forms.ListBox accountListBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage outputTab;
        private System.Windows.Forms.RichTextBox outputTextBox;
        private System.Windows.Forms.TextBox dateModifiedTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox dateCreatedTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label queryStatusLabel;
        private System.Windows.Forms.Label elapsedTimeLabel;
        private System.Windows.Forms.TextBox referenceNumberTextBox;
        private System.Windows.Forms.Label referenceNumberLabel;
        private System.Windows.Forms.ListBox possibleAccountListBox;
        private System.Windows.Forms.Label possibleErrorsLabel;
        private System.Windows.Forms.ListBox possibleErrorListBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label errorsLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button queryMoreInfoButton;
        private System.Windows.Forms.TextBox idTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown daysNumericUpDown;
        private System.Windows.Forms.ListBox accountsQueryListBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox deletedCheckBox;
        private System.Windows.Forms.Button resetOptionsButton;
        private System.Windows.Forms.Button openSaveFileButton;
        private System.Windows.Forms.TabPage NRATabPage;
        private System.Windows.Forms.TextBox accountNumberTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox accountNameTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.OpenFileDialog NRAopenFileDialog;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RichTextBox lastQueryRichTextBox;
        private System.Windows.Forms.Button loadSettingsButton;
        private System.Windows.Forms.ToolTip ShopifyToolTip;
        private System.Windows.Forms.Button startProcessButton;
        private System.Windows.Forms.GroupBox fileGroupBox;
        private System.Windows.Forms.TextBox givenExcelFileTextBox;
        private System.Windows.Forms.Label givenExcelFileLabel;
        private System.Windows.Forms.Button selectExcelFileButton;
        private System.Windows.Forms.GroupBox pendingGroupBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox eventDateTextBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox productDescriptionTextBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox lineItemIDTextBox;
        private System.Windows.Forms.TextBox productCodeTextBox;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox committeeNumberTextBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox dateRequiredTextBox;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label distributorListLabel;
        private System.Windows.Forms.ListBox pending_distributorListBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ListBox pending_orderListBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ListBox acknowledge_distributorsListBox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ListBox acknowledge_ordersListBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown orderTypeIDNumericUpDown;
        private System.Windows.Forms.Button recurringButton;
        private System.Windows.Forms.Label nraStatusLabel;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox orderDateTextBox;
        private System.Windows.Forms.TextBox nraReferenceNumberTextBox;
        private System.Windows.Forms.TextBox skuTextBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox orderStatusTextBox;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.TextBox statusTextBox;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFileToolStripMenuItem;
        private System.Windows.Forms.RadioButton nra_ProcessBothRadioButton;
        private System.Windows.Forms.RadioButton nra_AcknowledgeRadioButton;
        private System.Windows.Forms.RadioButton nra_PendingOnlyRadioButton;
        private System.Windows.Forms.GroupBox nra_ProcessGroupBox;
        private System.Windows.Forms.CheckBox shopify_CheckedCheckBox;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem1;
        private System.Windows.Forms.TabPage nraFirstCheckTabPage;
        private System.Windows.Forms.Label shopify_OrderInfoLabel;
        private System.Windows.Forms.Button nraFirstCheck_StartQueryButton;
        private System.Windows.Forms.ListBox nraFirst_ID_Status5_ListBox;
        private System.Windows.Forms.ListBox nraFirst_AccountsListBox;
        private System.Windows.Forms.DateTimePicker nraFirstDateTimePicker;
        private System.Windows.Forms.RichTextBox nraFirst_QueryRichTextBox;
        private System.Windows.Forms.ComboBox nraFirstCheck_channelComboBox;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox nraFirstCheck_workflowsComboBox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox nraFirstCheck_locationComboBox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button nraFirstCheck_runOrderProcessButton;
        private System.Windows.Forms.ToolStripMenuItem abortOperationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem allTasksToolStripMenuItem;
        private System.Windows.Forms.Label nraFirstCheck_StatusLabel;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button shopify_QueryAllForAccount;
        private System.Windows.Forms.CheckBox shopify_checkErrorsCheckBox;
        private System.Windows.Forms.ToolStripComboBox outputToolStripComboBox;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ListBox nraFirst_ID_Other_ListBox;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
    }
}