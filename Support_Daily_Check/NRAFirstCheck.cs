﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;
using Console = System.Console;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using System.Diagnostics;
using nChannel_Daily_Program;
using nChannel.Framework.v1_0.Collections;
using Newtonsoft.Json;
using nChannel.Framework.v1_0;
using System.Net;
using System.IO;
using System.Windows.Forms;
using nChannel.Framework.v1_0.Custom;

/*
* Created by: Scott Lewis
* Date: July 1, 2016

*/
public class NRAFirstCheck : NRAManager
{
    private static int totalErrors = 0;//total number of errors found after all queries, excludes possible errors
    
    public Dictionary<string, List<string>> firstCheckDictionary = new Dictionary<string, List<string>>();//list of accounts with the list of order id for first check in NRA not in status 5
    public Dictionary<string, List<string>> firstCheckDictionary_Status5 = new Dictionary<string, List<string>>();//list of accounts with the list of order id for first check in NRA that are in status 5

    private string ApiUrl = "https://api.nchannel.com";
    private string accountID = "1272";
    private string queryParams = "?dataformat=json&suppressevents=true&token=sharp";
    public const string status5_Mark = " *Status=5*";
    public NRAFirstCheck(Main_Form form) : base(form)
    {
        mainForm = form;
    }

    /// <summary>
    /// Returns the workflow id for order shipping
    /// </summary>
    /// <returns></returns>
    public string getOrderProcessingWorkflowID(string accountNum)
    {
        string wfID = "";

        Dictionary<string, string> channels = loadChannelChannels(accountNum);
        string channelID = "";
        foreach (string id in channels.Keys)
        {
            if (!channels[id].Contains("Shipping") && channels[id].Contains("Order Process"))
            {
                channelID = id;
            }
        }
        Account_Locations locations = loadLocationsByChannel(channelID, accountNum);
        string locationID = "";
        locations.ForEach(delegate (Account_Location location)
        {
            if (location.Name.Contains("Cloud"))
            {
                locationID = location.ID.ToString();
            }
        });
        Dictionary<string, string> workflows = getWorkflows(channelID, locationID, accountNum);
        foreach (string id in workflows.Keys)
        {
            if (!workflows[id].Contains("Shipping") && workflows[id].Contains("Order Process"))
            {
                wfID = id;
            }
        }
        return wfID;
    }

    /// <summary>
    /// Returns the workflow id for order shipping
    /// </summary>
    /// <returns></returns>
    public string getShippingWorkflowID(string accountNum)
    {
        string wfID = "";

        Dictionary<string, string> channels = loadChannelChannels(accountNum);
        string channelID = "";
        foreach(string id in channels.Keys)
        {
            if(channels[id].Contains("Shipping"))
            {
                channelID = id;
            }
        }
        Account_Locations locations = loadLocationsByChannel(channelID, accountNum);
        string locationID = "";
        locations.ForEach(delegate (Account_Location location)
        {
            if (location.Name.Contains("Cloud"))
            {
                locationID = location.ID.ToString();
            }
        });
        Dictionary<string, string> workflows = getWorkflows(channelID, locationID, accountNum);
        foreach (string id in workflows.Keys)
        {
            if (workflows[id].Contains("Shipping"))
            {
                wfID = id;
            }
        }
        return wfID;
    }

    public int queryOrders(DateTime dateTime)
    {
        //start stopwatch for time elapsed
        Stopwatch sw = new Stopwatch();
        sw.Start();

        Console.WriteLine("\n---------------\nStarting NRA First Check Query ");
        //mainForm.addOutput("Starting NRA First Check Query");
        string connectionStr = "Data Source=db-01;" +
        "Initial Catalog=nChannel;" +
        "User id=Sa-nchannelreader;" +
        "Password=Fluftuo2!;" +
        "Connection Timeout=45;" ;
        SqlConnection sqlConnection1 = null;
        SqlCommand cmd;
        SqlDataReader reader = null;

        //mainForm.addOutput(dateTime.ToShortDateString());

        string query = " select o1.AccountID,o1.id as destinationorderid, e.status, e1.status " +
            "from [Order].[Order] o join [Order].Entry e on e.OrderID=o.ID join [Order].[Order] o1 on o1.OriginatingOrderID=o.ID " +
            "join [order].[Entry] e1 on e1.OrderID=o1.ID and e1.SKU=e.sku left join [Order].EntryDetail ed1 on ed1.OrderEntryID=e1.ID left " +
            "join [Order].EntryDetailShipping eds1 on eds1.OrderEntryDetailID=ed1.ID left join [Order].EntryDetail ed on ed.OrderEntryID=e.ID left " +
            "join [Order].EntryDetailShipping eds on eds.OrderEntryDetailID=ed.ID " +
            "where o.AccountID=767 and o.Deleted=0 and e.Deleted=0 and o1.Deleted=0 and (ed1.Deleted=0 or ed.Deleted is null) and (eds.Deleted=0 or eds.Deleted is null) " +
            "and (eds1.Deleted=0 or eds1.Deleted is null) and ((e.Status != e1.Status) or (e1.status not in (5) and eds1.trackingnumber !='')) and o1.DateModified>'" + dateTime.ToShortDateString() + "' order by o1.accountid,o1.DateModified desc ";
            mainForm.Invoke(new MethodInvoker(delegate
        {
            mainForm.setNRAFirstCheckQuery(query);
        }));
        try
        {
            sqlConnection1 = new SqlConnection(connectionStr);
            cmd = new SqlCommand();
            cmd.CommandTimeout = 45;
            cmd.CommandText = query;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;

            sqlConnection1.Open();

            reader = cmd.ExecuteReader();
        }
        catch (Exception ex)
        {
            mainForm.setNRAStatusText("SQL Connection Failed");
            mainForm.addOutput("SQL Failed at NRA Check. Could not query orders.");
            mainForm.addOutput(ex.Message);
            return ERROR;
        }

        if (reader.HasRows)
        {
            while (reader.Read())
            {
                string accountStr = reader.GetValue(0).ToString();
                string idStr = reader.GetValue(1).ToString();
                int status1 = reader.GetInt32(2);
                int status2 = reader.GetInt32(3);
                Console.WriteLine(status1 + " " + status2);
                if(status1 == 5 || status2 == 5)
                {
                    List<string> idList = new List<string>();
                    if (firstCheckDictionary_Status5.ContainsKey(accountStr))
                    {
                        idList = firstCheckDictionary_Status5[accountStr];
                        idList.Add(idStr);
                        firstCheckDictionary_Status5[accountStr] = idList;
                    }
                    else
                    {//add the new key to the other list so they are blank
                        firstCheckDictionary.Add(accountStr, new List<string>());
                        idList.Add(idStr);
                        firstCheckDictionary_Status5.Add(accountStr, idList);
                    }
                }
                else
                {
                    List<string> idList = new List<string>();
                    if (firstCheckDictionary.ContainsKey(accountStr))
                    {
                        idList = firstCheckDictionary[accountStr];
                        idList.Add(idStr);
                        firstCheckDictionary[accountStr] = idList;
                    }
                    else
                    {//add the new key to the statsu5 list so they are blank
                        firstCheckDictionary_Status5.Add(accountStr, new List<string>());
                        idList.Add(idStr);
                        firstCheckDictionary.Add(accountStr, idList);
                    }
                }
            }
            // Console.WriteLine("Rows Found: " + refNumEdit.Count);
        }
        else
        {
            Console.WriteLine("No rows found.");
            mainForm.addOutput("No rows found.");
        }

        sqlConnection1.Close();
        sw.Stop();
        Console.WriteLine("Connection Closed. Time Elapsed: " + sw.Elapsed);
        mainForm.addOutput("Connection Closed. Time Elapsed: " + sw.Elapsed);
        return SUCCESS;
    }
        
    public string CallUrl(string url, string method, object objToSend = null)
    {
        Console.WriteLine(url);
        WebRequest req = WebRequest.Create(url);
        req.Method = method.ToString();
        if (objToSend != null)
        {
            Stream reqStream = req.GetRequestStream();
            string postData = JsonConvert.SerializeObject(objToSend);
            byte[] postArray = Encoding.ASCII.GetBytes(postData);
            reqStream.Write(postArray, 0, postArray.Length);
            reqStream.Close();
        }
        StreamReader sr = new StreamReader(req.GetResponse().GetResponseStream());
        return sr.ReadToEnd();
    }

    public void executeWorkflow(string wfID, string orderID)
    {
        string url = string.Concat(new string[]
        {
                this.ApiUrl,
                "/Workflows/",
                this.accountID.ToString(),
                "/",
                wfID,
                "/Execute",
                this.queryParams });
        try
        {

            Work_WorkAttributes attributes = new Work_WorkAttributes();
                attributes.Add(new Work_WorkAttribute
                {
                    Name = "nChannel.OrderID",
                    Value = orderID
                });
            WebRequest req = WebRequest.Create(url);
            req.Method = "POST";
            Stream reqStream = req.GetRequestStream();
            string postData = JsonConvert.SerializeObject(attributes);
            byte[] postArray = Encoding.ASCII.GetBytes(postData);
            reqStream.Write(postArray, 0, postArray.Length);
            reqStream.Close();
            StreamReader sr = new StreamReader(req.GetResponse().GetResponseStream());
            string result = sr.ReadToEnd();
            //  MessageResult message = JsonConvert.DeserializeObject<MessageResult>(result);
            //MessageBox.Show(message.Message, message.Status.ToString());
        }
        catch (Exception ex)
        {
            Console.WriteLine(url);
        }
    }

    public void executeWorkflow(string wfID, List<string> orderIDList)
    {
        string url = string.Concat(new string[]
        {
                this.ApiUrl,
                "/Workflows/",
                this.accountID.ToString(),
                "/",
                wfID,
                "/Execute",
                this.queryParams });
        try
        {
            
        Work_WorkAttributes attributes = new Work_WorkAttributes();
        foreach (string id in orderIDList)
        {
            attributes.Add(new Work_WorkAttribute
            {
                Name = "nChannel.OrderID",
                Value = id
            });
        }
        WebRequest req = WebRequest.Create(url);
        req.Method = "POST";
        Stream reqStream = req.GetRequestStream();
        string postData = JsonConvert.SerializeObject(attributes);
        byte[] postArray = Encoding.ASCII.GetBytes(postData);
        reqStream.Write(postArray, 0, postArray.Length);
        reqStream.Close();
        StreamReader sr = new StreamReader(req.GetResponse().GetResponseStream());
        string result = sr.ReadToEnd();
      //  MessageResult message = JsonConvert.DeserializeObject<MessageResult>(result);
        //MessageBox.Show(message.Message, message.Status.ToString());
        }
        catch (Exception ex)
        {
            Console.WriteLine(url);
        }
    }

    public void loadChannelChannels()
    {
        string strChannels = this.CallUrl(string.Concat(new object[]
        {
                this.ApiUrl,
                "/Accounts/",
                this.accountID,
                "/ChannelChannelsForAccount/",
                this.queryParams
        }), "GET", null) ?? "";
        if (strChannels != "")
        {
            Dictionary<string, string> items = new Dictionary<string, string>();
            items.Add("-1", "Select Channel..");
            Channel_Channels channels = JsonConvert.DeserializeObject<Channel_Channels>(strChannels);
            channels.ForEach(delegate (Channel_Channel x)
            {
                items.Add(x.ID.ToString(), x.Name);
            });
            mainForm.GetNRAFirstCheck_ChannelComboBox().DataSource = new BindingSource(items, null);
        }
    }

    /// <summary>
    /// Gets a dictionary of channelID, channelName of all channels for the account
    /// </summary>
    /// <param name="accountNum"></param>
    /// <returns></returns>
    public Dictionary<string, string> loadChannelChannels(string accountNum)
    {
        string strChannels = this.CallUrl(string.Concat(new object[]
        {
                this.ApiUrl,
                "/Accounts/",
                accountNum,
                "/ChannelChannelsForAccount/",
                this.queryParams
        }), "GET", null) ?? "";
        if (strChannels != "")
        {
            Dictionary<string, string> items = new Dictionary<string, string>();
            items.Add("-1", "Select Channel..");
            Channel_Channels channels = JsonConvert.DeserializeObject<Channel_Channels>(strChannels);
            channels.ForEach(delegate (Channel_Channel x)
            {
                items.Add(x.ID.ToString(), x.Name);
            });
            return items;
        }

        return new Dictionary<string, string>();
    }

    public Account_Locations loadLocationsByChannel(string channelID)
    {
        string str = this.CallUrl(string.Concat(new object[]
        {
                this.ApiUrl,
                "/Accounts/",
                this.accountID,
                "/LocationsByChannelChannel/",
                channelID,
                this.queryParams
        }), "GET", null) ?? "";
        Account_Locations result;
        if (str != "")
        {
            Dictionary<string, string> items = new Dictionary<string, string>();
            items.Add("-1", "Select Locations..");
            Account_Locations entities = JsonConvert.DeserializeObject<Account_Locations>(str);
            entities.ForEach(delegate (Account_Location x)
            {
                items.Add(x.ID.ToString(), x.Name);
            });
            result = entities;
        }
        else
        {
            result = new Account_Locations();
        }
        return result;
    }

    public Account_Locations loadLocationsByChannel(string channelID, string accountNum)
    {
        string str = this.CallUrl(string.Concat(new object[]
        {
                this.ApiUrl,
                "/Accounts/",
                accountNum,
                "/LocationsByChannelChannel/",
                channelID,
                this.queryParams
        }), "GET", null) ?? "";
        Account_Locations result;
        if (str != "")
        {
            Dictionary<string, string> items = new Dictionary<string, string>();
            items.Add("-1", "Select Locations..");
            Account_Locations entities = JsonConvert.DeserializeObject<Account_Locations>(str);
            entities.ForEach(delegate (Account_Location x)
            {
                items.Add(x.ID.ToString(), x.Name);
            });
            result = entities;
        }
        else
        {
            result = new Account_Locations();
        }
        return result;
    }

    public Dictionary<string, string> getWorkflows(string channelID, string locationID, string accountNum)
    {
        Account_ChannelLocations entities = this.loadChannelLocation(channelID, locationID, accountNum);
        Dictionary<string, string> items = new Dictionary<string, string>();
        items.Add("-1", "Select WorkFlow..");
        entities.ForEach(delegate (Account_ChannelLocation x)
        {
            x.Account_ChannelTransactions.ForEach(delegate (Account_ChannelTransaction ct)
            {
                ct.Work_WorkFlows.ForEach(delegate (Work_WorkFlow wf)
                {
                    items.Add(wf.ID.ToString(), wf.Name);
                });
            });
        });

        return items;
    }

    public void populate_cbWorkFlow(string channelID, string locationID)
    {
        Account_ChannelLocations entities = this.loadChannelLocation(channelID, locationID);
        Dictionary<string, string> items = new Dictionary<string, string>();
        items.Add("-1", "Select WorkFlow..");
        entities.ForEach(delegate (Account_ChannelLocation x)
        {
            x.Account_ChannelTransactions.ForEach(delegate (Account_ChannelTransaction ct)
            {
                ct.Work_WorkFlows.ForEach(delegate (Work_WorkFlow wf)
                {
                    items.Add(wf.ID.ToString(), wf.Name);
                });
            });
        });
        
    }

    public Account_ChannelLocations loadChannelLocation(string channelID, string locationID)
    {
        string tmpQueryParams = "?dataformat=json&suppressevents=false&token=sharp";
        string str = this.CallUrl(string.Concat(new object[]
        {
                this.ApiUrl,
                "/Accounts/",
                this.accountID,
                "/ChannelLocationsByChannelIDAndLocationID/",
                channelID,
                "/",
                locationID,
                tmpQueryParams
        }), "GET", null) ?? "";
        Account_ChannelLocations result;
        if (str != "")
        {
            Account_ChannelLocations entities = JsonConvert.DeserializeObject<Account_ChannelLocations>(str);
            result = entities;
        }
        else
        {
            result = new Account_ChannelLocations();
        }
        return result;
    }

    public Account_ChannelLocations loadChannelLocation(string channelID, string locationID, string accountNUm)
    {
        string tmpQueryParams = "?dataformat=json&suppressevents=false&token=sharp";
        string str = this.CallUrl(string.Concat(new object[]
        {
                this.ApiUrl,
                "/Accounts/",
                accountNUm,
                "/ChannelLocationsByChannelIDAndLocationID/",
                channelID,
                "/",
                locationID,
                tmpQueryParams
        }), "GET", null) ?? "";
        Account_ChannelLocations result;
        if (str != "")
        {
            Account_ChannelLocations entities = JsonConvert.DeserializeObject<Account_ChannelLocations>(str);
            result = entities;
        }
        else
        {
            result = new Account_ChannelLocations();
        }
        return result;
    }

}


