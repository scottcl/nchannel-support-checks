﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;
using Console = System.Console;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using System.Diagnostics;
using nChannel_Daily_Program;
using System.Windows.Forms;

/*
* Original for zipit internship
* Created by: Scott Lewis
* Date: Feb 2, 2016
* 
* nChannel Category Map
* Date: 6/23/2016
* 
* Excel Format:
* 1 Name1
* 2 Name2
* 3 Name3
* 1 Name21
* 2 Name22
* 
* Conversion:
* Name1
* Name1|Name2
* Name1|Name2|Name3
* Name21
* Name21|Name22
*/
public class ShopifyManager
{
    private ArrayList list;//the list to save all the cells
    private char delimiter = '|';//the character used to separate each category/column
    private string saveFileName = "CPCategories_Modified.xls";//the name of the new excel saved file with the correct flatten information
    private string saveFileLocation = "C:\\Users\\Scott.Lewis\\Desktop\\";//the location to save the new correct file. Don't forget the ending "\\"
    private string openFileName = "CPCategories_10.xlsx";//the name of the excel file you want to be edited and read
    private string openFileLocation = "C:\\Users\\Scott.Lewis\\Desktop\\";//the location of the excel file you want to be read. Don't forget the ending "\\"

    private static Regex digitsOnly = new Regex(@"[^\d]");//used to get rid of any non-numeric in a string
    private static Regex charsOnly = new Regex("[0-9]{2,}");//used to get rid of any numbers in a string

    public const string statusMark = " -S ";
    private static int totalErrors = 0;//total number of errors found after all queries, excludes possible errors
    public static List<int> accountNum_Errors = new List<int>();//list of all account numbers with an error, excludes possible errors
    public static Dictionary<int,List<string>> errors_Account_List = new Dictionary<int, List<string>>();//dictionary of account numbers, and list of errors with each account number
    public static Dictionary<int, List<string>> possible_Errors_Account_List = new Dictionary<int, List<string>>();//dictionary of account numbers, and list of possible errors with each account number
   

    public static string str668 = "select ID,referencenumber from [Order].[Order] where Accountid=668 and deleted=0 and DateCreated > GETDATE()-7 order by ReferenceNumber";
    public static string str1012 = "select ID,referencenumber from [Order].[Order] where Accountid=1012 and deleted=0 and referencenumber like 'S%' and DateCreated > GETDATE()-5 order by ReferenceNumber";
    public static string str1128 = "select ID,referencenumber from [Order].[Order] where Accountid=1128 and deleted=0   and DateCreated > GETDATE()-7 order by ReferenceNumber";
    public static string str479 = "select ID,referencenumber from [Order].[Order] where Accountid=479 and deleted=0 and ordertypeid=1  and DateCreated > GETDATE()-7 order by ReferenceNumber";
    public static string str1169 = "select ID,referencenumber from [Order].[Order] where Accountid=1169 and deleted=0   and DateCreated > GETDATE()-7 order by ReferenceNumber";
    public static string str1017 = "select ID,referencenumber from [Order].[Order] where Accountid=1017 and deleted=0 and DateCreated > GETDATE()-6 order by ReferenceNumber";
    public static string str1055 = "select ID,referencenumber from [Order].[Order] where Accountid=1055 and deleted=0 and DateCreated > GETDATE()-7 order by ReferenceNumber";
    public static string str987 = "select ID,referencenumber from [Order].[Order] where Accountid=987 and deleted=0 and DateCreated > GETDATE()-7 order by ReferenceNumber";
    public static string str983 = "select ID,referencenumber from [Order].[Order] where Accountid=983 and deleted=0 and DateCreated > GETDATE()-7 order by ReferenceNumber";
    public static string str992 = "select ID,referencenumber from [Order].[Order] where Accountid=992 and deleted=0 and DateCreated > GETDATE()-7 order by ReferenceNumber";
    public static string str905 = "select ID,referencenumber from [Order].[Order] where Accountid=905 and deleted=0 and DateCreated > GETDATE()-7 order by ReferenceNumber";
    public static string str1193 = "select ID,referencenumber from [Order].[Order] where Accountid=1193 and deleted=0 and OrderTypeID=1 and DateCreated > GETDATE()-6 order by ReferenceNumber";

    private Main_Form mainForm;

    public ShopifyManager(Main_Form form)
    {
        mainForm = form;
    }
    /*  static void Main(string[] args)
    {
        Console.WriteLine("Starting work\n");
         Manager test = new Manager();
       // Main_Form mainForm = new Main_Form();

        string firstQuery = "select Accountid, referencenumber,id,status,datecreated,DateModified from [Order].[Order] where deleted=0 and OrderTypeID=1 and ((status in (1,2) and AccountID not in (479,558,588,592,587,593,597,635,698,734,751,748,753,755,797,800,801,802,805,806,807,808,809,812,815,817,819,822,825,827,828,829,872,898,907,908,910,933,944,950,791,1094,954,990,1217,1231,1047,1055,1261,1264,1265,1224,1302)and AccountID not in (select supplieraccountid from Account.Account_RelatedAccount where AccountID=767)) or (Status=1 and AccountID in (791,950,751,1055)) ) and DateModified > '6/23/2016' AND DateModified <= DATEADD(day, 1, '6/24/2016') AND DATEPART(mi,DateModified) >= 0 AND DATEPART(mi,DateModified) <= 40              -- gets the hour of the day from the datetimeorder by accountid,referencenumber";

        test.newShopifyQuery(str668);
        test.newShopifyQuery(str1012);
        test.newShopifyQuery(str1128);
        test.newShopifyQuery(str479);
        test.newShopifyQuery(str1169);
        test.newShopifyQuery(str1017);
        test.newShopifyQuery(str1055);
        test.newShopifyQuery(str987);
        test.newShopifyQuery(str479);
        test.newShopifyQuery(str983);
        test.newShopifyQuery(str992);
        test.newShopifyQuery(str905);
        test.newShopifyQuery(str1193);

      //  test.newFirstQuery(firstQuery);
       Console.WriteLine("Total Errors Found: "+totalErrors+". In Accounts: ");
        foreach(int x in accountNum_Errors)
        {
            Console.WriteLine(x);
        }
        Console.Write("\nPress enter to terminate...");
        Console.ReadLine();
    }*/
    
    public int getTotalErrors()
    {
        return totalErrors;
    }

    /// <summary>
    /// Executs query to get more order information
    /// </summary>
    /// <param name="accountNum">The account number to query</param>
    /// <param name="errorList">The reference number to look for</param>
    /// <param name="deleted">Whether to check if for only deleted orders or not</param>
    /// <param name="orderTypeID">The order type to query for</param>
    /// <returns>List of string. The first string displays the reference number</returns>
    public List<string> checkAllErrors(string accountNum, List<string> errorList, bool deleted = false, int orderTypeID = 1)
    {
        string referenceNumbersStr = "";
        //create string for ('###','####')
        foreach(string str in errorList)
        {
            referenceNumbersStr += "'"+str+"',";
        }
        referenceNumbersStr = referenceNumbersStr.Trim(',');

        //start stopwatch for time elapsed
        Stopwatch sw = new Stopwatch();
        sw.Start();
        List<string> resultList = new List<string>();

        int deletedNum = 0;
        if (deleted)
        {
            deletedNum = 1;
        }
        
        string query = "select oo.ReferenceNumber from [Order].[Order] oo join Account.Account aa ON aa.ID = oo.AccountID  where referenceNumber in (" + referenceNumbersStr + ") and Accountid = " + accountNum + " and oo.deleted = " + deletedNum + " and oo.OrderTypeID = " + orderTypeID + " order by ReferenceNumber";
        mainForm.Invoke(new MethodInvoker(delegate
        {
            mainForm.setLastQueryText(query);
            mainForm.addOutput("Shopify Query Selected Erorr query: ");
            mainForm.addOutput(query, false);
        }));
        // Console.WriteLine("Starting Extra Query for Account " + accountNum);
        // mainForm.addOutput("Starting Extra Query for Account " + accountNum);
        string connectionStr = "Data Source=db-01;" +
        "Initial Catalog=nChannel;" +
        "User id=Sa-nchannelreader;" +
        "Password=Fluftuo2!;";
        SqlConnection sqlConnection1 = new SqlConnection(connectionStr);
        SqlCommand cmd = new SqlCommand();
        SqlDataReader reader;

        try
        {
            cmd.CommandText = query;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;
            sqlConnection1.Open();
            reader = cmd.ExecuteReader();
        }
        catch (Exception ex)
        {
            mainForm.Invoke(new MethodInvoker(delegate
            {
                MessageBox.Show("Error occured when connecting to database!\nQuery Terminated.", "SQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mainForm.addOutput("Error occured when connecting to database!\nQuery Terminated.");
                mainForm.addOutput(ex.Message);
            }));
            resultList.Insert(0, "Failed Query.");
            return resultList;
        }
        List<String> column1 = new List<String>();
        List<String> refNum = new List<String>();//list of reference numbers as originally queried
        List<String> refNumEdit_Name = new List<String>();//list of reference numbers with only the abbreviations (no numbers)
        List<String> refNumEdit = new List<String>();//list of reference numbers without any non-numeric characters
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                resultList.Add(reader.GetValue(0).ToString());
              //  resultList.Add(reader.GetValue(1).ToString());
            }
        }
        if (resultList.Count == 0)
        {
            mainForm.Invoke(new MethodInvoker(delegate
            {
                Console.WriteLine("No errors found!");
                mainForm.addOutput("No errors found!");
            }));
            resultList.Insert(0, "Success Query. No results found.");
            return resultList;
        }
        else
        {
            mainForm.Invoke(new MethodInvoker(delegate
            {
                Console.WriteLine("Success Query. Found " + resultList.Count + " results");
                mainForm.addOutput("Success Query. Found " + resultList.Count + " results");
            }));
            resultList.Insert(0, "Success Query. Found " + resultList.Count + " results");
        }

        sqlConnection1.Close();
        sw.Stop();
        Console.WriteLine("Connection Closed. Time Elapsed: " + sw.Elapsed + "\n------------");
        mainForm.Invoke(new MethodInvoker(delegate
        {
            mainForm.addOutput("Connection Closed. Time Elapsed: " + sw.Elapsed + "\n------------");
            mainForm.setElapsedTime(sw.ElapsedMilliseconds);
        }));
        return resultList;
    }


    /// <summary>
    /// Executs query to get more order information
    /// </summary>
    /// <param name="accountNum">The account number to query</param>
    /// <param name="error">The reference number to look for</param>
    /// <param name="deleted">Whether to check if for only deleted orders or not</param>
    /// <param name="orderTypeID">The order type to query for</param>
    /// <param name="markStatus">If true will add the statusMark variable plus the status number to the error string (Ex: #### -S:1)</param>
    /// <returns>List of string. The first string displays the status. Second: id, Third: referenceNumber, Fourth: datemodified, Fifth: datecreated, Sixth: account name, Seventh: status</returns>
    public List<string> queryMoreInfo(string accountNum, string error, bool deleted = false, int orderTypeID = 1, bool markStatus = false)
    {
        //start stopwatch for time elapsed
        Stopwatch sw = new Stopwatch();
        sw.Start();
        List<string> resultList = new List<string>();

        int deletedNum = 0;
        if (deleted)
        {
            deletedNum = 1;
        }
        long errorNum = Convert.ToInt64(digitsOnly.Replace(error, ""));
        string query = "select oo.id, oo.referenceNumber, oo.datemodified, oo.datecreated, aa.Company, oo.Status from [Order].[Order] oo join Account.Account aa ON aa.ID = oo.AccountID  where referenceNumber like '%" + error+"%' and Accountid = " + accountNum + " and oo.deleted = "+ deletedNum + " and oo.OrderTypeID = "+orderTypeID+" order by ReferenceNumber";
        mainForm.Invoke(new MethodInvoker(delegate
        {
            mainForm.setLastQueryText(query);
            mainForm.addOutput("Shopify Query Selected Erorr query: ");
            mainForm.addOutput(query, false);
        }));
       // Console.WriteLine("Starting Extra Query for Account " + accountNum);
       // mainForm.addOutput("Starting Extra Query for Account " + accountNum);
        string connectionStr = "Data Source=db-01;" +
        "Initial Catalog=nChannel;" +
        "User id=Sa-nchannelreader;" +
        "Password=Fluftuo2!;";
        SqlConnection sqlConnection1 = new SqlConnection(connectionStr);
        SqlCommand cmd = new SqlCommand();
        SqlDataReader reader;
        
        try
        {
            cmd.CommandText = query;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;
            sqlConnection1.Open();
            reader = cmd.ExecuteReader();
        }
        catch (Exception ex)
        {
            mainForm.Invoke(new MethodInvoker(delegate
            {
                MessageBox.Show("Error occured when connecting to database!\nQuery Terminated.", "SQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mainForm.addOutput("Error occured when connecting to database!\nQuery Terminated.");
                mainForm.addOutput(ex.Message);
            }));
            resultList.Insert(0,"Failed Query.");
            return resultList ;
        }
        List<String> column1 = new List<String>();
        List<String> refNum = new List<String>();//list of reference numbers as originally queried
        List<String> refNumEdit_Name = new List<String>();//list of reference numbers with only the abbreviations (no numbers)
        List<String> refNumEdit = new List<String>();//list of reference numbers without any non-numeric characters
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                resultList.Add(reader.GetValue(0).ToString());
                resultList.Add(reader.GetValue(1).ToString());
                resultList.Add(reader.GetValue(2).ToString());
                resultList.Add(reader.GetValue(3).ToString());
                resultList.Add(reader.GetValue(4).ToString());
                resultList.Add(reader.GetValue(5).ToString());
            }
        }
        else
        {
          //  sqlConnection1.Close();
            //Subtract 1 from referenceNumber to try and find something close to it, if not return
            resultList = new List<string>();
            //replaces the first number(error) with the last order that probably exists
            long lastNum = errorNum;
            errorNum--;
            error = error.Replace(lastNum + "", errorNum + "");
            query = "select oo.id, oo.referenceNumber, oo.datemodified, oo.datecreated, aa.Company, oo.Status from [Order].[Order] oo join Account.Account aa ON aa.ID = oo.AccountID  where referenceNumber like '%" + error + "' and Accountid = " + accountNum + " and oo.deleted = "+ deletedNum + " and oo.OrderTypeID = "+orderTypeID+" order by ReferenceNumber";
            mainForm.Invoke(new MethodInvoker(delegate
            {
                mainForm.addOutput("No rows found.");
                mainForm.addOutput("Subtracting 1 from reference number and trying again for Account " + accountNum);
                mainForm.setLastQueryText(query);
                mainForm.addOutput("Shopify Selected Error query: ");
                mainForm.addOutput(query, false);
                Console.WriteLine("\n---------------\nNo rows found.\nSubtracting 1 from reference number and trying again for Account " + accountNum);                
            }));
            sqlConnection1 = new SqlConnection(connectionStr);
            cmd = new SqlCommand();

            cmd.CommandText = query;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = sqlConnection1;
            try
            {
                sqlConnection1.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error occured when connecting to database!\nQuery Terminated.", "SQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mainForm.addOutput("Error occured when connecting to database!\nQuery Terminated.");
                mainForm.addOutput(ex.Message);
                resultList.Insert(0, "Failed Query.");
                return resultList;
            }
            reader = cmd.ExecuteReader();
            column1 = new List<String>();
            refNum = new List<String>();//list of reference numbers as originally queried
            refNumEdit_Name = new List<String>();//list of reference numbers with only the abbreviations (no numbers)
            refNumEdit = new List<String>();//list of reference numbers without any non-numeric characters
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    resultList.Add(reader.GetValue(0).ToString());
                    resultList.Add(reader.GetValue(1).ToString());
                    resultList.Add(reader.GetValue(2).ToString());
                    resultList.Add(reader.GetValue(3).ToString());
                    resultList.Add(reader.GetValue(4).ToString());
                    resultList.Add(reader.GetValue(5).ToString());
                }
            }
            else
            {
                //sqlConnection1.Close();
                //Subtract 1 from referenceNumber to try and find something close to it, if not return
                resultList = new List<string>();
                errorNum--;
                query = "select oo.id, oo.referenceNumber, oo.datemodified, oo.datecreated, aa.Company, oo.Status from [Order].[Order] oo join Account.Account aa ON aa.ID = oo.AccountID  where Accountid = " + accountNum + " and oo.deleted = " + deletedNum + " and oo.OrderTypeID = " + orderTypeID + " order by ReferenceNumber";
                mainForm.Invoke(new MethodInvoker(delegate
                {
                    mainForm.setLastQueryText(query);
                    mainForm.addOutput("No rows found.");
                    mainForm.addOutput("Removing refererence number to find any order with the account number: " + accountNum);
                    mainForm.addOutput("Shopify Selected Error query: ");
                    mainForm.addOutput(query, false);
                }));
               
                try
                {
                    sqlConnection1 = new SqlConnection(connectionStr);
                    cmd = new SqlCommand();
                    cmd.CommandText = query;
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = sqlConnection1;
                    sqlConnection1.Open();
                    reader = cmd.ExecuteReader();
                }
                catch (Exception ex)
                {
                    mainForm.Invoke(new MethodInvoker(delegate
                    {
                        MessageBox.Show("Error occured when connecting to database!\nQuery Terminated.", "SQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        mainForm.addOutput("Error occured when connecting to database!\nQuery Terminated.");
                        mainForm.addOutput(ex.Message);
                    }));
                    resultList.Insert(0, "Failed Query.");
                    return resultList;
                }

                column1 = new List<String>();
                refNum = new List<String>();//list of reference numbers as originally queried
                refNumEdit_Name = new List<String>();//list of reference numbers with only the abbreviations (no numbers)
                refNumEdit = new List<String>();//list of reference numbers without any non-numeric characters
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        resultList.Add(reader.GetValue(0).ToString());
                        resultList.Add(reader.GetValue(1).ToString());
                        resultList.Add(reader.GetValue(2).ToString());
                        resultList.Add(reader.GetValue(3).ToString());
                        resultList.Add(reader.GetValue(4).ToString());
                        resultList.Add(reader.GetValue(5).ToString());
                    }
                }
                else
                {
                    //Try to find the last order from the company
                    Console.WriteLine("No rows found.");
                    mainForm.addOutput("No rows found.");

                }
            }
            }
        
        if (resultList.Count == 0)
        {
            mainForm.Invoke(new MethodInvoker(delegate
            {
                Console.WriteLine("No errors found!");
                mainForm.addOutput("No errors found!");
            }));
            resultList.Insert(0, "Success Query. No results found.");
            return resultList;
        }
        else
        {
            mainForm.Invoke(new MethodInvoker(delegate
            {
                Console.WriteLine("Success Query. Found " + resultList.Count + " results");
                mainForm.addOutput("Success Query. Found " + resultList.Count + " results");
            }));
            resultList.Insert(0, "Success Query. Found " + resultList.Count + " results");
        }

        sqlConnection1.Close();
        sw.Stop();
        Console.WriteLine("Connection Closed. Time Elapsed: " + sw.Elapsed + "\n------------");
        mainForm.Invoke(new MethodInvoker(delegate
        {
            mainForm.addOutput("Connection Closed. Time Elapsed: " + sw.Elapsed + "\n------------");
            mainForm.setElapsedTime(sw.ElapsedMilliseconds);
        }));
        return resultList;
    }
    
    /// <summary>
    /// Executs query and checks if orders are sequential, reports back any gaps
    /// </summary>
    /// <param name="query">The sql query. You may omit accountID and dateModified from the where clause, but must supply parameters. Leave blank for default.</param>
    /// <param name="accountNum">The nChannel account ID.</param>
    /// <param name="numDaysToCheck">The number of days to check with order datemodified. Ex: 1 will query anything from execution to 24 hour earlier</param>
    /// <param name="deleted">False will check if order has not been deleted. True will check all orders.</param>
    /// <param name="orderTypeID">Order Type ID to look for. 1 is default</param>
    /// <returns>Success: 1, Failure: 2</returns>
    public int newShopifyQuery(string query, int accountNum = 0, int numDaysToCheck = 0, bool deleted = false, int orderTypeID = 1)
    {
       //start stopwatch for time elapsed
        Stopwatch sw = new Stopwatch();
        sw.Start();
        mainForm.Invoke(new MethodInvoker(delegate
        {
            mainForm.addOutput("------------------------");
            mainForm.addOutput("Starting Query for Account " + accountNum);
        }));
        if (String.IsNullOrEmpty(query) )
        {
            int deleteNum = 0;
            if (deleted)
            {
                deleteNum = 1;
            }
            query = "select ID,referencenumber from [Order].[Order] where Accountid=" + accountNum + " and deleted=" + deleteNum + " and ordertypeid="+orderTypeID+" and DateCreated > GETDATE()-" + numDaysToCheck + " order by ReferenceNumber";
            mainForm.Invoke(new MethodInvoker(delegate
            {
                mainForm.setLastQueryText(query);
                mainForm.addOutput("New shopify query: ");
                mainForm.addOutput(query, false);
            }));
        }
        else
        {
            mainForm.Invoke(new MethodInvoker(delegate
            {
                mainForm.setLastQueryText(query);
            mainForm.addOutput("New shopify query: ");
            mainForm.addOutput(query, false);
            }));
        }

        //Console.WriteLine("\n---------------\nStarting Query for Account "+accountNum);
        string connectionStr = "Data Source=db-01;" +
        "Initial Catalog=nChannel;" +
        "User id=Sa-nchannelreader;" +
        "Password=Fluftuo2!;" +
        "Connection Timeout=45;";
        SqlConnection sqlConnection1 = new SqlConnection(connectionStr);
        SqlCommand cmd = new SqlCommand();
        SqlDataReader reader;

        cmd.CommandText = query;
        cmd.CommandType = CommandType.Text;
        cmd.Connection = sqlConnection1;
        cmd.CommandTimeout = 45;
        try
        {
            sqlConnection1.Open();
        }
        catch (Exception ex)
        {
            mainForm.Invoke(new MethodInvoker(delegate
            {
                MessageBox.Show("Error occured when connecting to database!\nQuery Terminated.", "SQL Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mainForm.addOutput("Error occured when connecting to database!\nQuery Terminated.");
                mainForm.addOutput(ex.Message);
            }));
            return 2;           
        }

        reader = cmd.ExecuteReader();
        List<String> column1 = new List<String>();
        List<String> refNum = new List<String>();//list of reference numbers as originally queried
        List<String> refNumEdit_Name = new List<String>();//list of reference numbers with only the abbreviations (no numbers)
        List<String> refNumEdit = new List<String>();//list of reference numbers without any non-numeric characters
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                // Console.WriteLine("{0}\t{1}", reader.GetValue(0), reader.GetValue(1));
                column1.Add(reader.GetValue(0).ToString());
                refNum.Add(reader.GetValue(1).ToString());
                refNumEdit_Name.Add(charsOnly.Replace(reader.GetValue(1).ToString(),""));
                refNumEdit.Add(digitsOnly.Replace(reader.GetValue(1).ToString(), ""));
            }
           // Console.WriteLine("Rows Found: " + refNumEdit.Count);
        }
        else
        {
            Console.WriteLine("No rows found.");
            return 1;
        }
        List<string> errors = new List<string>();
        List<string> possibleErrors = new List<string>();
        //long number = (long)Convert.ToInt32(column2[0]);
        for (int i = 0; i < refNumEdit.Count - 1; i++)
        {
            long num1 = Convert.ToInt64(refNumEdit[i]);
            long num2 = Convert.ToInt64(refNumEdit[i + 1]);
            long nextNum = num1 + 1;

            if (num1 + 1 != num2)
            {
                if (num2 < num1 + 100)
                {
                    // Console.WriteLine(num1 + " + " + " 1 = " + (num1 + 1) + " : num2 = " + num2);
                    //errors.Add(i.ToString());
                    if (!refNumEdit_Name[i].Equals(refNumEdit_Name[i + 1])){//store names do not match
                        possibleErrors.Add((num1 + 1).ToString());
                    }
                    else
                    {
                        int currentIndex = i;
                        long endNumber = Convert.ToInt64(refNumEdit[i + 1]);
                        long currentNumber = nextNum;
                        do
                        {
                            errors.Add(refNumEdit_Name[i]+currentNumber.ToString());
                            totalErrors++;
                            if (accountNum_Errors.IndexOf(accountNum) == -1)
                                accountNum_Errors.Add(accountNum);
                            currentNumber++;
                        } while (endNumber > currentNumber);
                       
                        
                    }
                }               
                else
                {
                    possibleErrors.Add(nextNum.ToString());
                }
            }
        }
        if(errors.Count == 0 && possibleErrors.Count == 0)
        {
            mainForm.Invoke(new MethodInvoker(delegate
            {
                Console.WriteLine("No errors found!");
                mainForm.addOutput("No errors found!");
                sqlConnection1.Close();
                sw.Stop();
                Console.WriteLine("Connection Closed. Time Elapsed: " + sw.Elapsed + "\n------------");
                mainForm.addOutput("Connection Closed. Time Elapsed: " + sw.Elapsed );
                mainForm.setElapsedTime(sw.ElapsedMilliseconds);
            }));
                return 1;
        }
        else
        {
            if(errors.Count != 0)
            {
                errors_Account_List.Add(accountNum, errors);
            }
            if(possibleErrors.Count != 0)
            {
                possible_Errors_Account_List.Add(accountNum, possibleErrors);
            }
            mainForm.Invoke(new MethodInvoker(delegate
            {
                Console.WriteLine("Errors Found (" + errors.Count + ") :");
                mainForm.addOutput("Errors Found (" + errors.Count + ") :");
                foreach (string x in errors)
                {
                 //   Console.WriteLine(x);
                    mainForm.addOutput("" + x);
                }
                Console.WriteLine("Possible Errors Found (" + possibleErrors.Count + ") :");
                mainForm.addOutput("Possible Errors Found (" + possibleErrors.Count + ") :");
                foreach (string x in possibleErrors)
                {
                  //  Console.WriteLine(x);
                    mainForm.addOutput("" + x);
                }
            }));
        }

        sqlConnection1.Close();
        sw.Stop();
        Console.WriteLine("Connection Closed. Time Elapsed: "+ sw.Elapsed+"\n------------");
        mainForm.addOutput("Connection Closed. Time Elapsed: " + sw.Elapsed);
        mainForm.setElapsedTime(sw.ElapsedMilliseconds);
        return 1;
    }
   
    
    private void query()
    {
        string connectionStr = "Data Source=db-01;" +
        "Initial Catalog=nChannel;" +
        "User id=Sa-nchannelreader;" +
        "Password=Fluftuo2!;";
        SqlConnection sqlConnection1 = new SqlConnection(connectionStr);
        SqlCommand cmd = new SqlCommand();
        SqlDataReader reader;

        cmd.CommandText = "select ID,referencenumber from [Order].[Order]"+
            "where Accountid = 668 and deleted = 0"+
            "and DateCreated > GETDATE() - 7"+
            "order by ReferenceNumber";
        cmd.CommandType = CommandType.Text;
        cmd.Connection = sqlConnection1;

        sqlConnection1.Open();

        reader = cmd.ExecuteReader();
        List<String> column1 = new List<String>();
        List<String> column2 = new List<String>();
        if (reader.HasRows)
        {
            while (reader.Read())
            {
               // Console.WriteLine("{0}\t{1}", reader.GetValue(0), reader.GetValue(1));
                column1.Add(reader.GetValue(0).ToString());
                column2.Add(digitsOnly.Replace(reader.GetValue(1).ToString(),""));
            }
            Console.WriteLine("Rows Found: "+column2.Count);
        }
        else
        {
            Console.WriteLine("No rows found.");
            return;
        }
        List <string> errors = new List<string>();
        List<string> possibleErrors = new List<string>();
        //long number = (long)Convert.ToInt32(column2[0]);
        for(int i = 0; i < column2.Count - 1; i++)
        {
            long num1 = Convert.ToInt64(column2[i]);
            long num2 = Convert.ToInt64(column2[i + 1]);
            if (num1 + 1 != num2)
            {
                if (num2 < num1 + 50)
                {
                    Console.WriteLine(num1 + " + " + " 1 = " + (num1 + 1) + " : num2 = " + num2);
                    //errors.Add(i.ToString());
                    errors.Add((num1 + 1).ToString());
                }else
                {
                    possibleErrors.Add((num1 + 1).ToString());
                }
            }
        }
        Console.WriteLine("Errors Found:");
        foreach(string x in errors)
        {
            Console.WriteLine(x);
        }
        Console.WriteLine("\n\nPossible Errors Found:");
        foreach (string x in possibleErrors)
        {
            Console.WriteLine(x);
        }
        sqlConnection1.Close();
    }
   
    private void start()
    {
        //Potential bug with language, did not need to use
        //SetNewCurrentCulture();

        //Create excel objects. If issue exists on creation, try for-each loop as work may get busy and fail. Refer to save()
        Excel.Application xlApp;
        Excel.Workbook xlWorkBook;
        Excel.Worksheet xlWorkSheet;
        Microsoft.Office.Interop.Excel.Range range;
        xlApp = new Microsoft.Office.Interop.Excel.Application();

        string str = "", level1Str = "", level2Str = "", level3Str = "", level4Str = "", level5Str = "";
        int rCnt = 0;
        int cCnt = 0;

        Console.WriteLine("Opening workbook and first worksheet");
        xlWorkBook = xlApp.Workbooks.Open(openFileLocation + openFileName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
        xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        Console.WriteLine("Worksheet and workbook opened.\nReading cells...");
        range = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.UsedRange;

        //used to create a list of strings that will be added to the new excel worksheet
        list = new ArrayList();
        string row = "";

        //in this case we need to check the columns before proceeding, so for each columns then rows
        for (rCnt = 1; rCnt < range.Rows.Count; rCnt++)
        {
            for (cCnt = 1; cCnt < range.Columns.Count; cCnt++)
            {
                //gets the current cell, must get Value and convert it to string
                Microsoft.Office.Interop.Excel.Range range2 = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[rCnt, cCnt];
                if(range2.Value == null)//If true, cell must have been empty. Should only do this if there is a column gap
                {
                    continue;
                }
                string cellValue = range2.Value.ToString();
                str = cellValue;
                //Check which level the column represents.
                if (str.Equals("1"))
                {
                    //Level 1 format, just add the next column that exists
                    string cellValue2 = "";
                    for(int i = 1; i < 8; i++)
                    {
                        range2 = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[rCnt, i];
                        if (range2.Value == null)
                        {
                            continue;
                        }
                         cellValue2 = range2.Value.ToString();
                      //  Console.WriteLine("Level 1: " + level1Str);
                    }
                    level1Str = cellValue2;
                    row = level1Str;
                   // Console.WriteLine("Level 1 FINALLLLL: "+level1Str);
                }
                else if (str.Equals("2"))
                {
                    //Level 2 format, add the last level 1 string plus the next column that exists
                    string cellValue2 = "";
                    for (int i = 1; i < 8; i++)
                    {
                        range2 = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[rCnt, i];
                        if (range2.Value == null)
                        {
                            continue;
                        }
                        cellValue2 = range2.Value.ToString();
                       // Console.WriteLine("Level 2: " + level1Str);
                    }
                    level2Str = level1Str + delimiter + cellValue2;
                    row = level2Str;
                   // Console.WriteLine("Level 2: " + level2Str);
                }
                else if (str.Equals("3"))
                {
                    //Level 3 format, add the last level 2 string plus the next column that exists
                    string cellValue2 = "";
                    for (int i = 1; i < 8; i++)
                    {
                        range2 = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[rCnt, i];
                        if (range2.Value == null)
                        {
                            continue;
                        }
                        cellValue2 = range2.Value.ToString();
                      //  Console.WriteLine("Level 3: " + level1Str);
                    }
                    level3Str = level1Str + delimiter + level2Str + delimiter + cellValue2;
                    level3Str = level2Str + delimiter + cellValue2;
                    row = level3Str;
                  //  Console.WriteLine("Level 3: " + level3Str);
                }
                else if (str.Equals("4"))
                {
                    //Level 4 format, add the last level 3 string plus the next column that exists
                    string cellValue2 = "";
                    for (int i = 1; i < 8; i++)
                    {
                        range2 = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[rCnt, i];
                        if (range2.Value == null)
                        {
                            continue;
                        }
                        cellValue2 = range2.Value.ToString();
                    //    Console.WriteLine("Level 4: " + level1Str);
                    }
                    level4Str = level1Str + delimiter + level2Str + delimiter + level4Str + delimiter + cellValue2;
                    level4Str = level3Str + delimiter + cellValue2;
                    row = level4Str;
                  //  Console.WriteLine("Level 4: " + level4Str);
                }
                else if (str.Equals("5"))
                {
                    //Level 5 format, add the last level 4 string plus the next column that exists
                    string cellValue2 = "";
                    for (int i = 1; i < 8; i++)
                    {
                        range2 = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Cells[rCnt, i];
                        if (range2.Value == null)
                        {
                            continue;
                        }
                        cellValue2 = range2.Value.ToString();
                    //    Console.WriteLine("Level 5: " + level1Str);
                    }
                    level5Str = level1Str + delimiter + level2Str + delimiter + level4Str + delimiter + level5Str + delimiter + cellValue2;
                    level5Str = level4Str + delimiter + cellValue2;
                    row = level5Str;
                  //  Console.WriteLine("Level 5: " + level5Str);
                }                
                //Console.WriteLine(str);
            }
            //gets rid of ending slashes
            row = row.TrimEnd(delimiter);
            list.Add(row);
            row = "";
        }

        Console.WriteLine("Finished processing cells.");
        /*  foreach (string x in list) //Used to print all list results that will be saved to new workbook
          {
              Console.WriteLine(x);
          }
          */

        //close any excel objects for clean finish
        xlApp.Quit();
        releaseObject(xlWorkSheet);
        releaseObject(xlWorkBook);
        releaseObject(xlApp);

        save();
    }

    private void save()
    {
        Console.WriteLine("\nCreating new excel sheet...");

        Excel.Application xlApp = null;
       // Microsoft.Office.Interop.Excel.Range range;

        //Noticed problems when creating Excel.Application if busy it will fail. Try up to 3 times
        int tries = 0;
        do
        {
            try
            {
                xlApp = new Microsoft.Office.Interop.Excel.Application();
                break;
            }
            catch (Exception ex)
            {
                tries++;
                mainForm.addOutput("Error opening excel file. Incrementing tries to " + tries + " of 3");
                mainForm.addOutput(ex.Message);
            }
        } while (tries < 3);

        if (xlApp == null)
        {
            Console.WriteLine("Excel is not properly installed or could not open file! (" + tries + " tries)");
            mainForm.addOutput("Excel is not properly installed or could not open file! (" + tries + " tries)");
            return;
        }

        Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
        //used to save excel as certain parameters are uneccessary, this is a filler
        object misValue = System.Reflection.Missing.Value;

        xlWorkBook = xlApp.Workbooks.Add(misValue);
        xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        Console.WriteLine("New excel worksheet created.  (" + tries + " tries)\nAdding cells...");
        for (int i = 1; i < list.Count; i++)
        {
            xlWorkSheet.Cells[i, 1] = list[i];
        }
        //xlWorkSheet.Cells[1, 1] = "Sheet 1 content";
        Console.WriteLine("Saving new excel workbook.");
        xlWorkBook.SaveAs(saveFileLocation + saveFileName, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
        xlWorkBook.Close(true, misValue, misValue);
        xlApp.Quit();

        //close out any excel objects for clean finish
        releaseObject(xlWorkSheet);
        releaseObject(xlWorkBook);
        releaseObject(xlApp);
        Console.Write("New excel sheet at " + saveFileLocation + saveFileName + "\n\nProgram finished.\n");
    }

    private void releaseObject(object obj)
    {
        try
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }
        catch (Exception ex)
        {
            obj = null;
            Console.WriteLine("Unable to release the Object " + ex.ToString());
        }
        finally
        {
            GC.Collect();
        }
    }

    /* Below methods are unneccessary. Used as a fix for computers with inconsistent cultures. Sets to US culture then resets */
    /*
    //declare a variable to hold the CurrentCulture
    System.Globalization.CultureInfo oldCI;
    //get the old CurrenCulture and set the new, en-US
    void SetNewCurrentCulture()
    {
        oldCI = System.Threading.Thread.CurrentThread.CurrentCulture;
        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
    }
    //reset Current Culture back to the originale
    void ResetCurrentCulture()
    {
        System.Threading.Thread.CurrentThread.CurrentCulture = oldCI;
    }
    */
}


